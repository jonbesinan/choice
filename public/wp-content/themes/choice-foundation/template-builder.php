<?php
/*
Template Name: Template Builder
*/
get_header(); ?>

<div class="main">
    <div class="sticky-el sticky-header">
        <?php 
        $field_heading = [
            'heading'     => 'section_1_title',
        ]; 
        
        get_template_part('partials/block', 'header');
        jpr_get_template_part_with_vars('partials/block', 'heading', $field_heading);
        ?>
    </div>

    <div class="sticky-el sticky-footer">
        <?php
        $field_footer = [
            'class'     => '',
        ]; 
        jpr_get_template_part_with_vars('partials/footer', 'top', $field_footer); ?> 
    </div>

    <div id="fullpage">
        <?php 
        get_template_part('partials/section', '1');
        get_template_part('partials/section', '2');
        get_template_part('partials/section', '3');
        get_template_part('partials/section', '4');
        get_template_part('partials/section', '5');
        get_template_part('partials/section', 'feed');
        ?>
    </div>
</div>

<?php
get_footer();