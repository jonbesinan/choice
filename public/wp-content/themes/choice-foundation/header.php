<!DOCTYPE html>
<html lang="en">
<head>
    <?php get_template_part('app/partials/head'); ?>
</head>
<body <?php body_class('bg-blue loading') ?>>

<?php get_template_part('partials/loader'); ?>

<!-- Header is moved to section-5 -->