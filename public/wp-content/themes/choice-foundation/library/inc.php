<?php

// Common library files
require 'library/curl.php';
require 'library/time.php';
require 'library/social.php';
require 'library/simplexlsx.php';


// Hooks
require 'hooks/disable-emojies.php';


# AJAX functions