<?php

function clean_html($string)
{
    $string = urldecode($string);
    $bad = array("content-type", "bcc:", "to:", "cc:", "href");
    return str_replace($bad, "", $string);
}

function clean_string($string)
{
    $string = clean_html($string);
    $string = str_replace("+"," ", $string);
    return $string;
}