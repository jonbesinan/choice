<?php
class Curl{
	function __construct($url){}

	public static function getData($url){
		$c = curl_init($url);
		curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
		//don't verify SSL (required for some servers)
		curl_setopt($c, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($c, CURLOPT_SSL_VERIFYHOST, false);			
		//get data from facebook and decode JSON
		$page = json_decode(curl_exec($c));
		//close the connection
		curl_close($c);

		return $page;
	} 
}