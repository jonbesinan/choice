<?php
/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 10/02/2017
 * Time: 1:55 PM
 */


// REMOVE WP EMOJI
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');

remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );