<header class="wrap <?php echo isset($class) ? $class : '' ?>">
    <div class="spacing-right-40">
        <div class="row no-gutters spacing-top-20 spacing-bottom-20">
            <div class="col-md-6">
                <a href="/" class="brand"><?php the_field('header_text', 'option'); ?></a>
            </div>
            <div class="col-md-6 menu-wrap">
                <a href="#" class="menu-toggle collapsed">
                    <span class="brand"><?php the_field('header_text', 'option'); ?></span>
                    <span>Menu</span>
                </a>
                <div class="header-menu">
                    <?php wp_nav_menu(array('menu'=>'header-nav'));
                    
                    //this will display for mobile
                    $field_footer = [
                        'class'     => '',
                    ]; 
                    jpr_get_template_part_with_vars('partials/footer', 'top', $field_footer); ?>
                </div>
            </div>
        </div>  
    </div>
</header>