<div class="col-md-6 offset-md-6 col-right block-video">
    <div class="bg-video">
        <?php $video = get_field('section_1_video'); ?>
        
        <div id="loop">
            <div class="bg-img" style="background-image: url('<?php the_field('section_1_video_thumb') ?>');"></div>
        </div>

        <div class="mobile-view">
            <?php
            $field_header = [
                'class'     => '',
            ];

            jpr_get_template_part_with_vars('partials/block', 'header', $field_header);
            ?>

            <h1><?php the_field('section_1_title') ?></h1>
        </div>

       <div id="vid-container"></div>

        <div id="vid-mobile">
            <?php the_field('section_1_video_mobile'); ?>
        </div>

        <div class="vid-controls">
            <div>
                <button class="ctrl-play">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 86 86" class="play">
                        <defs><style>.fill{fill:#fff;}.no-fill{fill:none;}</style></defs>
                        <path class="fill" d="M72.05,41.32c-0.31-0.53-0.64-0.86-1.21-1.24S29.7,13.49,29,13.12s-1.04-0.62-1.85-0.63
                            s-1.24,0.2-1.73,0.49s-1.07,0.99-1.24,1.74s-0.17,2.16-0.17,3V66.1c0,0.39,0,1.1,0,2.15c0,0.85-0.01,2.26,0.17,3
                            s0.75,1.45,1.24,1.74s0.92,0.51,1.73,0.49s1.15-0.25,1.85-0.63S70.37,46.2,70.84,45.89c0.48-0.31,0.9-0.69,1.21-1.24
                            s0.45-1.1,0.45-1.67C72.5,42.43,72.36,41.85,72.05,41.32z"/>
                        <rect class="no-fill" id="Bounding-Box" width="86" height="86"/>
                    </svg>

                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 86 86" class="pause">
                        <defs><style>.fill{fill:#fff;}.no-fill{fill:none;}</style></defs>
                        <path class="fill" d="M53,15h6c1.66,0,3,1.34,3,3v50c0,1.66-1.34,3-3,3h-6c-1.66,0-3-1.34-3-3V18C50,16.34,51.34,15,53,15z
                            M24,18v50c0,1.66,1.34,3,3,3h6c1.66,0,3-1.34,3-3V18c0-1.66-1.34-3-3-3h-6C25.34,15,24,16.34,24,18z"/>
                        <rect class="no-fill" id="Bounding-Box" width="86" height="86"/>
                    </svg>
                </button>

                <progress id="progress-bar" min='0' max='100' value='0'>0% played</progress>
            </div>
            <div class="pull-right">
                <span class="current">00:00</span> / <span class="duration">00:00</span>
                
                <button class="ctrl-vol">
                    <svg xmlns="http://www.w3.org/2000/svg" width="20px" height="16px" viewBox="0 0 20 16" class="off">
                        <defs><style>.fill{fill:#fff;}.no-fill{fill:none;}</style></defs>
                        <path class="fill" d="M9,7.48L5.34,3.81l2.34-2.07c0.21-0.23,0.55-0.3,0.84-0.19C8.82,1.66,9.01,1.95,9,2.26V7.48z
                                M9,12.29v1.46c0,0.31-0.19,0.59-0.49,0.71c-0.09,0.03-0.18,0.05-0.28,0.05c-0.21,0-0.41-0.09-0.56-0.25L4,11H1
                                c-0.51,0-1-0.42-1-0.99V5.98C0,5.43,0.48,5,0.99,5h0.74L9,12.29z M12.57,13.91c0.13-0.01,0.26-0.07,0.35-0.17
                                c0.19-0.21,0.17-0.52-0.04-0.71L2.07,2.21C1.86,2.03,1.55,2.04,1.36,2.26C1.18,2.47,1.19,2.78,1.41,2.96l10.81,10.82
                                C12.31,13.88,12.44,13.92,12.57,13.91z"/>
                        <rect class="no-fill" id="Bounding-Box" width="20" height="16"/>
                    </svg>

                    <svg xmlns="http://www.w3.org/2000/svg" width="20px" height="16px" viewBox="0 0 20 16" class="on">
                        <defs><style>.fill{fill:#fff;}.no-fill{fill:none;}</style></defs>
                        <path class="fill" d="M16.07,15.9c-0.14,0-0.29-0.05-0.4-0.16c-0.23-0.22-0.24-0.58-0.02-0.81
                                c1.79-1.88,2.78-4.34,2.78-6.93c0-2.62-1-5.09-2.82-6.97c-0.22-0.23-0.22-0.6,0-0.82c0.22-0.22,0.58-0.23,0.8,0
                                c0.01,0.01,0.04,0.04,0.05,0.06c2,2.07,3.12,4.83,3.12,7.74c0,2.89-1.1,5.63-3.1,7.73C16.37,15.84,16.22,15.9,16.07,15.9z
                                M12.91,13.03c1.36-1.29,2.17-3.17,2.17-5.03c0-1.85-0.81-3.73-2.17-5.03c-0.23-0.22-0.59-0.21-0.81,0.02
                                c-0.22,0.23-0.21,0.59,0.02,0.81c1.14,1.09,1.82,2.66,1.82,4.2c0,1.55-0.68,3.12-1.81,4.2c-0.23,0.22-0.24,0.58-0.02,0.81
                                c0.11,0.12,0.26,0.18,0.42,0.18C12.65,13.19,12.79,13.14,12.91,13.03z M7.68,1.74L4,5.01H0.99C0.48,5.01,0,5.44,0,5.99v3.98
                                c0,0.57,0.48,0.99,1,0.99h3l3.67,3.3c0.16,0.16,0.36,0.25,0.56,0.25c0.1,0,0.19-0.02,0.28-0.05C8.81,14.34,9,14.06,9,13.75V2.26
                                c0.01-0.31-0.18-0.6-0.48-0.71C8.23,1.44,7.9,1.52,7.68,1.74z"/>
                        <rect class="no-fill" id="Bounding-Box" width="20" height="16"/>
                    </svg>
                </button>


                <button class="ctrl-screen">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16px" height="16px" viewBox="0 0 16 16">
                        <defs><style>.fill{fill:#fff;}.no-fill{fill:none;}</style></defs>
                        <path class="fill" d="M6.87,9.97l-4.89,4.9C2.17,14.96,2.38,15,2.61,15h3.91c0.28,0,0.5,0.22,0.5,0.5S6.79,16,6.51,16
                                H2.61C1.17,16,0,14.82,0,13.37V9.51c0-0.28,0.22-0.5,0.5-0.5S1,9.23,1,9.51v3.86c0,0.23,0.05,0.45,0.13,0.65l4.89-4.89
                                c0.23-0.23,0.61-0.23,0.85,0C7.1,9.36,7.1,9.74,6.87,9.97z M13.4,0H9.5C9.23,0,9,0.22,9,0.5S9.23,1,9.5,1h3.89
                                c0.22,0,0.43,0.05,0.63,0.13L9.09,6.06c-0.23,0.23-0.23,0.61,0,0.85c0.12,0.12,0.27,0.18,0.42,0.18s0.31-0.06,0.42-0.18l4.93-4.93
                                C14.95,2.18,15,2.4,15,2.63V6.5C15,6.78,15.23,7,15.5,7S16,6.78,16,6.5V2.63C16,1.18,14.83,0,13.4,0z"/>
                        <rect class="no-fill" id="Bounding-Box" width="16" height="16"/>
                    </svg>
                </button>
            </div>
        </div>
            
        <span class="btn-play">
            <svg height="70" width="70">
                <circle cx="35" cy="35" r="33" stroke="white" stroke-width="1" fill="none" />
            </svg>
        </span>
        <span class="btn-back">Back</span>
        <span class="info"><?php the_field('section_1_info'); ?></span>
    </div>
</div>