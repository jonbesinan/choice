<section id="section-1" class="section" data-title="<?php the_field('section_1_title') ?>">
    <div class="row no-gutters transform-transition">
        <?php 
        // $field_image = [
        //     'image'     => 'section_1_image',
        //     'heading'   => 'section_1_title',
        //     'hasFooter' => true,
        //     'hasArrow'  => true
        // ];
    
        // jpr_get_template_part_with_vars('partials/block', 'image', $field_image);

        get_template_part('partials/block', 'video');
        ?>
    </div>
</section>