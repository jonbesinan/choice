<div class="row no-gutters block-fund">
    <div class="col-md-6 offset-md-6 now col-right">
        <div class="wrap"><h1 class="spacing-top-20">NOW:</h1></div>

        <?php get_template_part('app/partials/svg', 'now') ?>


        <div class="wrap">
            <h1><?php the_field('section_3_current_fund') ?></h1>
            <div class="spacing-top-20 spacing-bottom-20 info">
                <small> 
                    <svg class="sprite information">
                        <use xlink:href="<?php echo SVG_PATH ?>#sprite-information"></use>
                    </svg>
                    <?php the_field('section_3_footer_note') ?>
                </small>
            </div>
        </div>
    </div>
</div>

<div class="row no-gutters block-fund">
    <div class="col-md-6 offset-md-6 target col-right">
        <div class="wrap"><h1 class="spacing-top-20"><?php the_field('section_3_target_year') ?>:</h1></div>

        <?php get_template_part('app/partials/svg', '2021') ?>

        <div class="wrap">
            <h1><?php the_field('section_3_target_fund') ?></h1>
            <div class="spacing-top-20 spacing-bottom-20 info">
                <small> 
                    <svg class="sprite information">
                        <use xlink:href="<?php echo SVG_PATH ?>#sprite-information"></use>
                    </svg>
                    <?php the_field('section_3_footer_note') ?>
                </small>
            </div>
        </div>
    </div>
</div>

<!-- <div class="row no-gutters block-fund">
    <div class="col-md-6 offset-md-6 statistics col-right">
        <div class="wrap"><h1 class="spacing-top-20">1 in 5</h1></div>

        <?php get_template_part('app/partials/svg', '1-in-5') ?>

        <div class="wrap">
            <p><?php the_field('section_3_1in5_heading') ?></p>
            <div class="spacing-top-10 spacing-bottom-20 info">
                <small class="no-icon"> 
                    <?php the_field('section_3_1in5_description') ?>
                </small>
            </div>
        </div>
    </div>
</div> -->