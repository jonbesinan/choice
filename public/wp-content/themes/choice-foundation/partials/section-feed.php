<section id="section-feed" class="section">
    <div class="bg-img" style="background-image: url(<?php the_field('section_6_banner') ?>)"></div>
    
    <div class="parent-wrap">
        <div class="block-feed">
            <div class="wrap">
                <div class="row spacing-top-20">
                    <div class="col-sm-6">
                        <h1>WHAT’S ON</h1>
                    </div>
                    <div class="col-sm-6">
                        <small> 
                            <svg class="sprite information">
                                <use xlink:href="<?php echo SVG_PATH ?>#sprite-information"></use>
                            </svg>
                            <?php the_field('section_6_note') ?>
                        </small>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div id="curator-feed"></div>
                    </div>
                </div>
            </div>
        </div>

        <?php get_template_part('partials/footer', 'contact'); ?>
    </div>
</section>