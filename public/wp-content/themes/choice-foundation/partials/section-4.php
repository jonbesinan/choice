<section id="section-4" class="section fp-auto-height fp-auto-height-responsive" data-title="<?php the_field('section_4_title') ?>">
    <div class="row no-gutters">
        <?php
        $field_image = [
            'image'     => 'section_4_image',
            'heading'   => 'section_4_title',
            'hasArrow'  => false,
            'hasFooter' => false,
        ]; 

        jpr_get_template_part_with_vars('partials/block', 'image', $field_image);
        ?>
    </div>

    <?php get_template_part('partials/block', 'get-involved'); ?>
</section>