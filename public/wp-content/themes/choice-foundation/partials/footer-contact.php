<footer class="footer-contact">
    <div class="row no-gutters spacing-top-20">
        <div class="col-md-6">
            <div class="wrap spacing-right-40">
                <h1 class="spacing-bottom-10">
                    <?php the_field('footer_contact_heading', 'options') ?>
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 47.58 47.58" class="sprite arrow"><defs><style>.cls-1{fill:none;}</style></defs><title>arrow-animate</title><g id="containter"><rect class="cls-1" width="47.58" height="47.58"/></g><g id="sideways"><path id="sideways-2" data-name="sideways" class="cls-2" d="M441,269l-12.64-12.64,2.08-2,16.18,16.18-16.18,16.17-2.13-2L441,272H409.11v-3Z" transform="translate(-409.11 -246.72)"/></g><g id="downline"><rect id="downline-2" data-name="downline" class="cls-2" y="0.02" width="3.02" height="25.22"/></g><g id="sideways-under"><rect id="downline-3" data-name="downline" class="cls-2" y="22.28" width="25.22" height="3.02"/></g></svg>
                </h1>
            </div>
        </div>
        <div class="col-md-6 col-right">
            <div class="spacing-left-40">
                <div class="wrap">
                    <div class="row">
                        <div class="col-xl-4 order-2 order-sm-1">
                            <h4>Social</h4>
                            <p>Coming soon</p>   
                            <?php //wp_nav_menu(array('menu'=>'social-nav')); ?>
                        </div>  
                        <div class="col-xl-8 order-1 order-sm-2">
                            <?php 
                            the_field('footer_contact_content', 'options');
                            get_template_part('app/partials/subscribe'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row no-gutters spacing-top-48">
        <div class="col-md-12">
            <div class="spacing-top-20 spacing-bottom-20 info">
                <small> 
                    <svg class="sprite information">
                        <use xlink:href="<?php echo SVG_PATH ?>#sprite-information"></use>
                    </svg>
                    <?php the_field('footer_contact_bottom_text', 'options') ?>
                </small>
            </div>
        </div>
    </div>
</footer>