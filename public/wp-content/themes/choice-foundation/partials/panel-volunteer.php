<div id="panel-volunteer" class="panel-in spacing-left-40">
    <div class="wrap">
        <div class="spacing-top-20 spacing-bottom-24 form-header">
            <a href="#" class="btn-panel-back">Back 
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 47.58 47.58" class="sprite arrow"><defs><style>.cls-1{fill:none;}</style></defs><title>arrow-animate</title><g id="containter"><rect class="cls-1" width="47.58" height="47.58"></rect></g><g id="sideways"><path id="sideways-2" data-name="sideways" class="cls-2" d="M441,269l-12.64-12.64,2.08-2,16.18,16.18-16.18,16.17-2.13-2L441,272H409.11v-3Z" transform="translate(-409.11 -246.72)"></path></g><g id="sideways-under"><rect id="downline-3" data-name="downline" class="cls-2" y="22.28" width="25.22" height="3.02"></rect></g></svg>
            </a>
            <p>Volunteer</p>
        </div>

        <?php the_field('section_5_volunteer_content') ?>
        <!-- <form method="POST" action="POST">
            <input type="text" placeholder="First Name*" required>
            <input type="text" placeholder="Last Name*" required>
            <input type="email" placeholder="Email address*" required>
            <input type="text" placeholder="Phone number" required>
            <div class="select-wrap">
                <select>
                    <option value="0">Location*</option>
                    <option class="New South Wales">New South Wales</option>
                    <option class="Queensland">Queensland</option>
                    <option class="South Australia">South Australia</option>
                    <option class="Tasmania">Tasmania</option>
                    <option class="Victoria">Victoria</option>
                    <option value="Western Australia">Western Australia</option>
                    <option value="International">International</option>
                </select>
            </div>
            <input type="text" placeholder="Occupation/Skills*" required>
            <input type="text" placeholder="How do you want to volunteer?" required>
            <button type="submit" class="btn btn-default">Submit</button>
        </form> -->

        <div class="spacing-top-20 spacing-bottom-20 info">
            <small> 
                <svg class="sprite information">
                    <use xlink:href="<?php echo SVG_PATH ?>#sprite-information"></use>
                </svg>
                <?php the_field('section_5_volunteer_note') ?>
            </small>
        
            <a href="#" class="btn-panel-back btn-panel-close">Close</a>
        </div>
    </div>
</div>