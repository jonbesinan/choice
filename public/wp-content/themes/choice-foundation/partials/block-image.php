<div class="col-md-6 offset-md-6 col-right block-image">
    <div class="bg-img" style="background-image: url(<?php the_field($image) ?>);"></div>

    <div class="mobile-view">
        <?php
        $field_header = [
            'class'     => '',
        ];

        jpr_get_template_part_with_vars('partials/block', 'header', $field_header);
        ?>

        <h1><?php the_field($heading) ?></h1>
    </div>

    <?php if($hasFooter): 
        $field_footer = [
            'class'     => '',
        ]; 

        jpr_get_template_part_with_vars('partials/footer', 'top', $field_footer);
    endif; ?>

    <?php if($hasArrow): ?>
        <a href="#" class="btn-next">
            <svg class="sprite left-arrow">
                <use xlink:href="<?php echo SVG_PATH ?>#sprite-left-arrow"></use>
            </svg>
        </a>
    <?php endif; ?>
</div>