<div id="panel-donate" class="panel-in spacing-left-40">
    <div class="wrap">
        <div class="spacing-top-20 spacing-bottom-24 form-header">
            <a href="#" class="btn-panel-back">Back 
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 47.58 47.58" class="sprite arrow"><defs><style>.cls-1{fill:none;}</style></defs><title>arrow-animate</title><g id="containter"><rect class="cls-1" width="47.58" height="47.58"></rect></g><g id="sideways"><path id="sideways-2" data-name="sideways" class="cls-2" d="M441,269l-12.64-12.64,2.08-2,16.18,16.18-16.18,16.17-2.13-2L441,272H409.11v-3Z" transform="translate(-409.11 -246.72)"></path></g><g id="sideways-under"><rect id="downline-3" data-name="downline" class="cls-2" y="22.28" width="25.22" height="3.02"></rect></g></svg>
            </a>
            <p>Donate</p>
        </div>

        <ul class="accordion">
            <?php while ( have_rows('section_5_donate_collapse') ) : the_row(); ?>
                <li>
                    <div class="spacing-top-24 spacing-bottom-24">
                        <a class="btn-collapse active" href="#">
                            <?php the_sub_field('title') ?>
                            <svg class="sprite dropdown">
                                <use xlink:href="<?php echo SVG_PATH ?>#sprite-dropdown"></use>
                            </svg>
                        </a>
                    </div>
                    
                    <div class="body">
                        <?php if( have_rows('content') ):
                            while ( have_rows('content') ) : the_row(); ?>
                        
                            <div class="row">
                                <div class="col-md-4 col-xl-4 sub-heading">
                                    <p><?php the_sub_field('title') ?></p>
                                </div>
                                
                                <div class="col-md-8 col-xl-8">
                                    <div class="spacing-right-10">
                                        <?php if(get_sub_field('type') == 'Content'): 
                                            the_sub_field('description');
                                        else: ?>
                                            <a href="<?php the_sub_field('link') ?>" class="btn btn-default">Make a donation
                                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 47.58 47.58" class="sprite arrow"><defs><style>.cls-1{fill:none;}</style></defs><title>arrow-animate</title><g id="containter"><rect class="cls-1" width="47.58" height="47.58"></rect></g><g id="sideways"><path id="sideways-2" data-name="sideways" class="cls-2" d="M441,269l-12.64-12.64,2.08-2,16.18,16.18-16.18,16.17-2.13-2L441,272H409.11v-3Z" transform="translate(-409.11 -246.72)"></path></g><g id="sideways-under"><rect id="downline-3" data-name="downline" class="cls-2" y="22.28" width="25.22" height="3.02"></rect></g></svg>
                                            </a>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>

                            <?php endwhile;
                        endif; ?>
                    </div>
                </li>
            <?php endwhile; ?>
        </ul>

            <!-- <button type="submit" class="btn btn-default">Make a donation
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 47.58 47.58" class="sprite arrow"><defs><style>.cls-1{fill:none;}</style></defs><title>arrow-animate</title><g id="containter"><rect class="cls-1" width="47.58" height="47.58"></rect></g><g id="sideways"><path id="sideways-2" data-name="sideways" class="cls-2" d="M441,269l-12.64-12.64,2.08-2,16.18,16.18-16.18,16.17-2.13-2L441,272H409.11v-3Z" transform="translate(-409.11 -246.72)"></path></g><g id="sideways-under"><rect id="downline-3" data-name="downline" class="cls-2" y="22.28" width="25.22" height="3.02"></rect></g></svg>
            </button> -->

        <div class="spacing-top-20 spacing-bottom-20 info">
            <small> 
                <svg class="sprite information">
                    <use xlink:href="<?php echo SVG_PATH ?>#sprite-information"></use>
                </svg>
                <?php the_field('section_5_apply_note') ?>
            </small>
        
            <a href="#" class="btn-panel-back btn-panel-close">Close</a>
        </div>
    </div>
</div>