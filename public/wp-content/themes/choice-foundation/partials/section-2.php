<section id="section-2" class="section" data-title="<?php the_field('section_2_title') ?>">
    <div class="row no-gutters">
        <?php 
        $field_whoWeAre = [
            'intro'     => 'section_2_intro',
            'members'   => 'section_2_members'
        ];  
        
        jpr_get_template_part_with_vars('partials/block', 'who-we-are', $field_whoWeAre);
        ?>
    </div>
</section>