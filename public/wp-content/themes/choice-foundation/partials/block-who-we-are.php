<div class="col-md-6 offset-md-6 bg-white col-right block-who-we-are">
    <div class="spacing-left-40 bg-white">
        <div class="wrap spacing-bottom-52">
            <hr>
            <div id="who-we-are" class="row spacing-top-20 spacing-bottom-17 top-heading">
                <div class="col-xl-4 col-md-2 col-2"><h4>1.</h4></div>
                <div class="col-xl-8 col-md-10 col-10"><h4>Who we are</h4></div>
            </div>
            
            <div class="description size-24">
                <?php the_field($intro) ?>
            </div>

            <?php if( have_rows($members) ):
                while ( have_rows($members) ) : the_row(); 
                    $details = get_sub_field('list');
                    $index = get_row_index(); ?>
                    
                    <div id="who-gallery-<?php echo $index ?>" class="gallery-wrapper <?php echo !empty($details[0]['video']) ? 'disable-slider' : ''; ?>">
                        <div class="row">
                            <div class="<?php echo get_sub_field('class') ? get_sub_field('class') : 'col-md-12' ?>">
                                <div class="gallery owl-carousel owl-theme">
                
                                    <?php while ( have_rows('list') ) : the_row();
                                        if(get_sub_field('type') == 'Image' && get_sub_field('image')['url']): ?>
                                        
                                        <div class="item">
                                            <img src="<?php echo get_sub_field('image')['url'] ?>" />
                                        </div>

                                        <?php elseif(get_sub_field('type') == 'Video' && get_sub_field('video')): 
                                            $iframe = get_sub_field('video');

                                            // use preg_match to find iframe src
                                            preg_match('/src="(.+?)"/', $iframe, $matches);
                                            $src = $matches[1]; ?>
                
                                        <div class="item-video" data-merge="3"><a class="owl-video" href="<?php echo $src ?>"></a></div>

                                    <?php 
                                        endif;
                                    endwhile;  ?>
                                </div>

                                <div class="float-left spacing-top-10">
                                    <a href="#" class="gallery-prev" slider="<?php echo $index ?>">
                                        <svg class="sprite left-arrow">
                                            <use xlink:href="<?php echo SVG_PATH ?>#sprite-left-arrow"></use>
                                        </svg>
                                    </a>
                                    <a href="#" class="gallery-next" slider="<?php echo $index ?>">
                                        <svg class="sprite right-arrow">
                                            <use xlink:href="<?php echo SVG_PATH ?>#sprite-right-arrow"></use>
                                        </svg>
                                    </a>
                                </div>
                                <div class="float-right spacing-top-10 spacing-bottom-48">
                                    <span class="current">1</span> / <span class="total"><?php echo count($details) ?></span>
                                </div>
                            </div>
                        </div>
                        

                        <div class="row spacing-bottom-48">
                            <div class="col-xl-4 col-md-5 offset-2 offset-md-2 offset-xl-0">
                                <h4 class="positions"><?php the_sub_field('position') ?></h4>

                                <ul class="nav nav-tabs spacing-bottom-48" role="tablist">
                                    <?php  foreach($details as $k => $detail): ?>
                                            
                                    <li><a data-toggle="tab" href="#<?php echo strtolower(str_replace(' ', '-', $detail['name'])) ?>" slider="<?php echo $index ?>"><?php echo $detail['name'] ?></a></li>
                            
                                    <?php endforeach;  ?>
                                </ul>
                            </div>
                            <div class="col-xl-8 offset-2 offset-md-2 offset-xl-0 tab-content">
                                <?php  foreach($details as $k => $detail): ?>
                                            
                                <div id="<?php echo strtolower(str_replace(' ', '-', $detail['name'])) ?>" class="<?php echo $k == 0 ? 'active show' : ''; ?> tab-pane spacing-right-20 fade">
                                    <h4><?php echo $detail['name'] ?></h4>
                                    <p class="position"><?php echo $detail['position'] ?></p>
                                    <?php echo $detail['biography'] ?>
                                </div>
                        
                                <?php endforeach;  ?>
                            </div>
                        </div>
                    </div>

                <?php endwhile;
            endif; ?>
            
        </div>
    </div>
</div>