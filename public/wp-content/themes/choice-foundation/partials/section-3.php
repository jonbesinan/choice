<section id="section-3" class="section" data-title="<?php the_field('section_3_title') ?>">
    <div class="row no-gutters">
        <?php
        $field_image = [
            'image'     => 'section_3_image',
            'heading'   => 'section_3_title',
            'hasArrow'  => false,
            'hasFooter' => false,
        ]; 

        jpr_get_template_part_with_vars('partials/block', 'image', $field_image);
        ?>
    </div>

    <div class="row no-gutters row-down">
        <?php
        $field_whatWeDo = [
            'intro'     => 'section_3_intro',
            'content'   => 'section_3_content'
        ];

        jpr_get_template_part_with_vars('partials/block', 'what-we-do', $field_whatWeDo);
        ?>
    </div>

    <?php get_template_part('partials/block', 'fund'); ?>
</section>