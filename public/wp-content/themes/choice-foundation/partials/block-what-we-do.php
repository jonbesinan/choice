<div class="col-md-6 offset-md-6 bg-white col-right block-what-we-do">
    <div class="spacing-left-40">
        <div class="wrap spacing-top-100 spacing-bottom-100">
            <hr>
            
            <div id="what-we-do" class="row spacing-top-24 spacing-bottom-24 top-heading">
                <div class="col-xl-4 col-md-2 col-2"><h4>2.</h4></div>
                <div class="col-xl-8 col-md-10 col-10"><h4>What we do</h4></div>
            </div>
            
            <div class="description size-24">
                <?php the_field($intro) ?>
            </div>

            <?php 
            $field_slider = [
                'images'     => 'section_3_gallery',
            ];
    
            jpr_get_template_part_with_vars('app/partials/slider', '', $field_slider);
            ?>
            
            <div class="row content">
                <div class="col-xl-8 offset-xl-4 col-md-10 offset-2 col-10 offset-md-2">
                    <div class="spacing-right-20">
                        <?php the_field($content) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>