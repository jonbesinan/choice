<div class="wrap block-heading">
    <div class="spacing-right-25">
        <h1><?php the_field($heading) ?></h1>
    </div>
</div>