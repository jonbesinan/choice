<footer class="footer-bottom">
    <div class="wrap bg-dots spacing-top-20">
        <div class="pattern"></div>
    </div>
    <div class="wrap spacing-top-24 spacing-bottom-24">
        <svg class="sprite secondary-logo">
            <use xlink:href="<?php echo SVG_PATH ?>#sprite-secondary-logo"></use>
        </svg>

        <div class="content">
            <small><?php the_field('footer_2_content', 'options') ?></small>
        </div>

        <small class="copyright spacing-top-24 spacing-bottom-24"><?php the_field('footer_2_copyright', 'options') ?></small>
    </div>
</footer>