<section id="section-5" class="section fp-auto-height fp-auto-height-responsive" data-title="<?php the_field('section_5_title') ?>">
    <div class="parent-wrap">
        <div class="row no-gutters">
            <?php get_template_part('partials/block', 'invest'); ?>
        </div>
    </div>
</section>