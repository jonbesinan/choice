<div class="row no-gutters block-get-involved row-down">
    <div class="col-md-6 offset-md-6 bg-white col-right">
        <div class="spacing-left-40">
            <div class="wrap spacing-top-100">
                <hr>
                <div id="get-involved" class="row spacing-top-24 spacing-bottom-24 top-heading">
                    <div class="col-xl-4 col-md-2 col-2"><h4>3.</h4></div>
                    <div class="col-xl-8 col-md-10 col-10"><h4>How to apply</h4></div>
                </div>

                <?php if(get_field('section_4_intro')): ?>
                <div class="description size-24 spacing-bottom-48">
                    <?php the_field('section_4_intro') ?>
                </div>
                <?php endif;

                if(have_rows('section_4_collapse_upper')): ?>
                <ul class="accordion">
                    <?php while ( have_rows('section_4_collapse_upper') ) : the_row(); ?>
                        <li>
                            <div class="spacing-top-24 spacing-bottom-24">
                                <a class="btn-collapse active" href="#">
                                    <?php the_sub_field('title') ?>
                                    <svg class="sprite dropdown">
                                        <use xlink:href="<?php echo SVG_PATH ?>#sprite-dropdown"></use>
                                    </svg>
                                </a>
                            </div>
                            
                            <div class="body">
                                <?php if( have_rows('answers') ):
                                    while ( have_rows('answers') ) : the_row(); ?>
                                
                                    <div class="row">
                                        <div class="col-md-4 col-xl-4 sub-heading">
                                            <p><?php the_sub_field('title') ?></p>
                                        </div>
                                        
                                        <div class="col-md-8 col-xl-8">
                                            <div class="spacing-right-10">
                                                <?php the_sub_field('description') ?>
                                            </div>
                                        </div>
                                    </div>

                                    <?php endwhile;
                                endif; ?>
                            </div>
                        </li>
                    <?php endwhile; ?> 
                </ul>
                <?php endif;
                
                if(get_field('section_4_table_heading')): ?>
                <div class="title spacing-top-24 spacing-bottom-24">
                    <h4><?php the_field('section_4_table_heading') ?></h4>
                </div>
                <?php endif;
                
                if(have_rows('section_4_table')): ?>
                    <div class="table-list spacing-bottom-18">
                        <div class="row">
                            <div class="col-md-4 spacing-bottom-10">Timing</div>
                            <div class="col-md-8 spacing-bottom-10">Activity</div>
                        </div>
                        <?php while ( have_rows('section_4_table') ) : the_row(); ?>
                            <div class="row">
                                <div class="col-md-4 timing spacing-bottom-18 spacing-top-18">
                                    <p><?php the_sub_field('timing') ?></p>
                                </div>
                                <div class="col-md-8 activity spacing-bottom-18 spacing-top-18">
                                    <?php the_sub_field('activity') ?>
                                </div>
                            </div>
                        <?php endwhile; ?> 
                    </div>
                <?php endif; ?>

                <?php if(get_field('section_4_note')): ?>
                    <div class="spacing-bottom-24 info">
                        <small> 
                            <svg class="sprite information">
                                <use xlink:href="<?php echo SVG_PATH ?>#sprite-information"></use>
                            </svg>
                            <?php the_field('section_4_note') ?>
                        </small>
                    </div>
                <?php endif; ?> 
                
                <?php if(have_rows('section_4_collapse_lower')): ?>
                <ul class="accordion">
                    <?php while ( have_rows('section_4_collapse_lower') ) : the_row(); ?>
                        <li>
                            <div class="spacing-top-24 spacing-bottom-24">
                                <a class="btn-collapse active" href="#">
                                    <?php the_sub_field('title') ?>
                                    <svg class="sprite dropdown">
                                        <use xlink:href="<?php echo SVG_PATH ?>#sprite-dropdown"></use>
                                    </svg>
                                </a>
                            </div>
                            
                            <div class="body">
                                <?php if( have_rows('answers') ):
                                    while ( have_rows('answers') ) : the_row(); ?>
                                
                                    <div class="row">
                                        <div class="col-md-4 col-xl-4 sub-heading">
                                            <p><?php the_sub_field('title') ?></p>
                                        </div>
                                        
                                        <div class="col-md-8 col-xl-8">
                                            <div class="spacing-right-10">
                                                <?php the_sub_field('description') ?>
                                            </div>
                                        </div>
                                    </div>

                                    <?php endwhile;
                                endif; ?>
                            </div>
                        </li>
                    <?php endwhile; ?> 
                </ul>
                <?php endif; ?>

                <?php 
                $field_slider = [
                    'images'     => 'section_4_gallery',
                ];

                jpr_get_template_part_with_vars('app/partials/slider', '', $field_slider);
                ?>
            </div>
        </div>
    </div>
</div>