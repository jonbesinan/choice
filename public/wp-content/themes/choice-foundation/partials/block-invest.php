<div class="col-md-6 offset-md-6 col-right block-invest">

    <div id="invest" class="owl-carousel owl-theme">
        <div class="item front">
            <div class="btn-panel apply blocks" data-panel="panel-apply">
                <div class="wrap">
                    <h1 class="spacing-top-20 spacing-bottom-10">
                        <?php the_field('section_5_apply_heading') ?>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 47.58 47.58" class="sprite arrow"><defs><style>.cls-1{fill:none;}</style></defs><title>arrow-animate</title><g id="containter"><rect class="cls-1" width="47.58" height="47.58"/></g><g id="sideways"><path id="sideways-2" data-name="sideways" class="cls-2" d="M441,269l-12.64-12.64,2.08-2,16.18,16.18-16.18,16.17-2.13-2L441,272H409.11v-3Z" transform="translate(-409.11 -246.72)"/></g><g id="downline"><rect id="downline-2" data-name="downline" class="cls-2" y="0.02" width="3.02" height="25.22"/></g><g id="sideways-under"><rect id="downline-3" data-name="downline" class="cls-2" y="22.28" width="25.22" height="3.02"/></g></svg>
                    </h1>
                </div>
                <div class="wrap">
                    <div class="spacing-top-20 spacing-bottom-20 info">
                        <span class="btn-more">Find out more</span>
                    </div>
                </div>
            </div>

            <a href="<?php the_field('section_5_donate_link') ?>" target="_blank">
            <div class="donate blocks"> 
                <div class="wrap">
                    <h1 class="spacing-top-20 spacing-bottom-10">
                        <?php the_field('section_5_donate_heading') ?>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 47.58 47.58" class="sprite arrow"><defs><style>.cls-1{fill:none;}</style></defs><title>arrow-animate</title><g id="containter"><rect class="cls-1" width="47.58" height="47.58"/></g><g id="sideways"><path id="sideways-2" data-name="sideways" class="cls-2" d="M441,269l-12.64-12.64,2.08-2,16.18,16.18-16.18,16.17-2.13-2L441,272H409.11v-3Z" transform="translate(-409.11 -246.72)"/></g><g id="downline"><rect id="downline-2" data-name="downline" class="cls-2" y="0.02" width="3.02" height="25.22"/></g><g id="sideways-under"><rect id="downline-3" data-name="downline" class="cls-2" y="22.28" width="25.22" height="3.02"/></g></svg>
                    </h1>
                </div>   
                <div class="wrap">
                    
                    <div class="spacing-top-20 spacing-bottom-20 info">
                        <small> 
                            <svg class="sprite information">
                                <use xlink:href="<?php echo SVG_PATH ?>#sprite-information"></use>
                            </svg>
                            <?php the_field('section_5_donate_note') ?>
                        </small>
                        <span class="btn-more">Find out more</span>
                    </div>
                </div>
            </div>
            </a> 

            <div class="btn-panel volunteer blocks" data-panel="panel-volunteer">
                <div class="wrap">
                    <h1 class="spacing-top-20 spacing-bottom-10">
                        <?php the_field('section_5_volunteer_heading') ?>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 47.58 47.58" class="sprite arrow"><defs><style>.cls-1{fill:none;}</style></defs><title>arrow-animate</title><g id="containter"><rect class="cls-1" width="47.58" height="47.58"/></g><g id="sideways"><path id="sideways-2" data-name="sideways" class="cls-2" d="M441,269l-12.64-12.64,2.08-2,16.18,16.18-16.18,16.17-2.13-2L441,272H409.11v-3Z" transform="translate(-409.11 -246.72)"/></g><g id="downline"><rect id="downline-2" data-name="downline" class="cls-2" y="0.02" width="3.02" height="25.22"/></g><g id="sideways-under"><rect id="downline-3" data-name="downline" class="cls-2" y="22.28" width="25.22" height="3.02"/></g></svg>
                    </h1>
                </div>   
                <div class="wrap">
                    <div class="spacing-top-20 spacing-bottom-20 info">
                        <span class="btn-more">Find out more</span>
                    </div>
                </div> 
            </div>
        </div>
        <div class="item">
            <?php  
            get_template_part('partials/panel', 'apply');
            //get_template_part('partials/panel', 'donate'); 
            get_template_part('partials/panel', 'volunteer'); ?>
        </div>
        <div class="item"></div>
    </div>
</div>