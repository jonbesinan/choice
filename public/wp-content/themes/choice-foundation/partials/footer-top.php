
<footer class="footer-1 <?php echo $class ?>">
    <div class="spacing-right-40">
        <div class="wrap">
            <div class="row no-gutters align-items-end spacing-bottom-20">
                <div class="col-6">
                    <svg class="sprite logo-2">
                        <use xlink:href="<?php echo SVG_PATH ?>#sprite-logo-2"></use>
                    </svg>
                </div>
                <div class="col-6"><small><?php the_field('footer_text', 'option'); ?></small></div>
            </div>
        </div>
    </div>
</footer>