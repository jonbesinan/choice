<?php
//Hooks
require 'hooks/wp_enqueue.php';
require 'hooks/nav_menu_link_attributes.php';
require 'hooks/wp_title.php';
require 'hooks/wp_cron.php';
require 'hooks/customize-register.php';
require 'hooks/preview_changes.php';
require 'hooks/style_format.php';

// AJAX functions
//require 'ajax/signup.php';