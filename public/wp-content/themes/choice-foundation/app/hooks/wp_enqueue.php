<?php 
function wp_mrblack_scripts() {
    wp_enqueue_style( 'bootstrap', '//maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css' );
    wp_enqueue_style( 'font-awesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css' );
    wp_enqueue_style( 'main', get_template_directory_uri() . '/assets/css/app.css' );
     
    wp_enqueue_script( 'vendor', get_template_directory_uri() . '/assets/js/vendor.js', array(), '1.0.0', true );
    wp_enqueue_script( 'main', get_template_directory_uri() . '/assets/js/app.js', array(), '1.0.0', true );
}
add_action( 'wp_enqueue_scripts', 'wp_mrblack_scripts' );