<?php 
/*Customizer Code HERE*/
// add_action('customize_register', 'theme_footer_customizer');

// function theme_footer_customizer($wp_customize)
// {
// 	//adding section in wordpress customizer   
// 	$wp_customize->add_section('footer_settings_section', array(
// 	  	'title'		=> 'General settings',
// 	  	'panel'		=> 'footer_panel',
// 	));

// 	$wp_customize->add_section('footer_address1_section', array(
// 	  	'title'		=> 'Address 1',
// 	  	'panel'		=> 'footer_panel',
// 	));

// 	$wp_customize->add_section('footer_address2_section', array(
// 	  	'title'		=> 'Address 2',
// 	  	'panel'		=> 'footer_panel',
// 	));

// 	$wp_customize->add_panel( 'footer_panel', array(
// 		'title' => 'Footer',
// 	));
	
// 	//adding setting for footer text area
// 	$wp_customize->add_setting('footer_brand', array(
// 	 	'default'        => '',
// 	));

// 	$wp_customize->add_setting('copyright_setting', array(
// 	 	'default'        => '',
// 	));

// 	$wp_customize->add_setting('address1_title_setting', array(
// 	 	'default'        => '',
// 	));

// 	$wp_customize->add_setting('address1_setting', array(
// 	 	'default'        => '',
// 	));

// 	$wp_customize->add_setting('address1_map_setting', array(
// 	 	'default'        => '',
// 	));

// 	$wp_customize->add_setting('address2_title_setting', array(
// 	 	'default'        => '',
// 	));

// 	$wp_customize->add_setting('address2_setting', array(
// 	 	'default'        => '',
// 	));

// 	$wp_customize->add_setting('address2_map_setting', array(
// 	 	'default'        => '',
// 	));

// 	$wp_customize->add_control('footer_brand', array(
// 	 	'label'   	=> 'Brand name',
// 	  	'section' 	=> 'footer_settings_section',
// 	 	'type'    	=> 'text',
// 	));

// 	$wp_customize->add_control('copyright_setting', array(
// 	 	'label'   => 'Copyright Text',
// 	  	'section' => 'footer_settings_section',
// 	 	'type'    => 'text',
// 	));

// 	$wp_customize->add_control('address1_title_setting', array(
// 	 	'label'   => 'Title',
// 	  	'section' => 'footer_address1_section',
// 	 	'type'    => 'text',
// 	));
	
// 	$wp_customize->add_control('address1_setting', array(
// 	 	'label'   => 'Address',
// 	  	'section' => 'footer_address1_section',
// 	 	'type'    => 'textarea',
// 	));

// 	$wp_customize->add_control('address1_map_setting', array(
// 	 	'label'   => 'Map link',
// 	  	'section' => 'footer_address1_section',
// 	 	'type'    => 'text',
// 	));

// 	$wp_customize->add_control('address2_title_setting', array(
// 	 	'label'   => 'Title',
// 	  	'section' => 'footer_address2_section',
// 	 	'type'    => 'text',
// 	));
	
// 	$wp_customize->add_control('address2_setting', array(
// 	 	'label'   => 'Address',
// 	  	'section' => 'footer_address2_section',
// 	 	'type'    => 'textarea',
// 	));

// 	$wp_customize->add_control('address2_map_setting', array(
// 	 	'label'   => 'Map link',
// 	  	'section' => 'footer_address2_section',
// 	 	'type'    => 'text',
// 	));
// }