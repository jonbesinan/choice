<?php 
function wpdocs_theme_name_wp_title( $title, $sep ) {
    if ( is_feed() ) {
        return $title;
    }
     
    global $page, $paged;
 
    // Add the blog name
    //$title .= get_bloginfo( 'name', 'display' );
 
    // Add the blog description for the home/front page.

    $site_description = get_bloginfo( 'description', 'display' );


    if ( $site_description && ( is_home() || is_front_page() ) ) {
        $title .= " $sep $site_description";
    }
 
    // Add a page number if necessary:
    if ( ( $paged >= 2 || $page >= 2 ) && ! is_404() ) {
        $title .= " $sep " . sprintf( __( 'Page %s', '_s' ), max( $paged, $page ) );
    }

    $temp = explode('-', $title);

    $parent_id = wp_get_post_parent_id( get_the_ID() );
    
    if(!empty($parent_id))
    {
        $title = str_replace( get_the_title(), get_the_title($parent_id), $title);
    }
    
    return $title;
}
add_filter( 'wp_title', 'wpdocs_theme_name_wp_title', 10, 2 );