<?php if( have_rows($images) ): ?>
    <div class="gallery-wrapper">
        <div class="row">
            <div class="col-md-12">
                <div class="gallery owl-carousel owl-theme">
                    <?php while ( have_rows($images) ) : the_row(); ?>
                        <div class="item">
                            <img src="<?php the_sub_field('image') ?>" />
                        </div>
                    <?php endwhile; ?>
                </div>
            
                <div class="float-left spacing-top-10">
                    <a href="#" class="gallery-prev">
                        <svg class="sprite left-arrow">
                            <use xlink:href="<?php echo SVG_PATH ?>#sprite-left-arrow"></use>
                        </svg>
                    </a>
                    <a href="#" class="gallery-next">
                        <svg class="sprite right-arrow">
                            <use xlink:href="<?php echo SVG_PATH ?>#sprite-right-arrow"></use>
                        </svg>
                    </a>
                </div>

                <div class="float-right spacing-top-10 spacing-bottom-48">
                    <span class="current">1</span> / <span class="total"><?php echo count(get_field($images)) ?></span>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>