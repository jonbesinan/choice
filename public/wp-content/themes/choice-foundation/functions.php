<?php
define('SVG_PATH', get_template_directory_uri().'/assets/svg/spritemap.svg');

// Common Sandbox library code
require_once 'library/inc.php';

// App specific code can go here or below
require_once 'app/inc.php';

add_theme_support( 'menus' );
add_theme_support( 'post-thumbnails' );
add_image_size( 'smallest', 100, 9999, false ); // increased from 20px because aspect ratio wasn't maintained

if( function_exists('acf_add_options_page') ) {
	acf_add_options_page();
}