/******/ (function(modules) { // webpackBootstrap
/******/ 	// install a JSONP callback for chunk loading
/******/ 	function webpackJsonpCallback(data) {
/******/ 		var chunkIds = data[0];
/******/ 		var moreModules = data[1];
/******/ 		var executeModules = data[2];
/******/
/******/ 		// add "moreModules" to the modules object,
/******/ 		// then flag all "chunkIds" as loaded and fire callback
/******/ 		var moduleId, chunkId, i = 0, resolves = [];
/******/ 		for(;i < chunkIds.length; i++) {
/******/ 			chunkId = chunkIds[i];
/******/ 			if(installedChunks[chunkId]) {
/******/ 				resolves.push(installedChunks[chunkId][0]);
/******/ 			}
/******/ 			installedChunks[chunkId] = 0;
/******/ 		}
/******/ 		for(moduleId in moreModules) {
/******/ 			if(Object.prototype.hasOwnProperty.call(moreModules, moduleId)) {
/******/ 				modules[moduleId] = moreModules[moduleId];
/******/ 			}
/******/ 		}
/******/ 		if(parentJsonpFunction) parentJsonpFunction(data);
/******/
/******/ 		while(resolves.length) {
/******/ 			resolves.shift()();
/******/ 		}
/******/
/******/ 		// add entry modules from loaded chunk to deferred list
/******/ 		deferredModules.push.apply(deferredModules, executeModules || []);
/******/
/******/ 		// run deferred modules when all chunks ready
/******/ 		return checkDeferredModules();
/******/ 	};
/******/ 	function checkDeferredModules() {
/******/ 		var result;
/******/ 		for(var i = 0; i < deferredModules.length; i++) {
/******/ 			var deferredModule = deferredModules[i];
/******/ 			var fulfilled = true;
/******/ 			for(var j = 1; j < deferredModule.length; j++) {
/******/ 				var depId = deferredModule[j];
/******/ 				if(installedChunks[depId] !== 0) fulfilled = false;
/******/ 			}
/******/ 			if(fulfilled) {
/******/ 				deferredModules.splice(i--, 1);
/******/ 				result = __webpack_require__(__webpack_require__.s = deferredModule[0]);
/******/ 			}
/******/ 		}
/******/ 		return result;
/******/ 	}
/******/
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// object to store loaded and loading chunks
/******/ 	// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 	// Promise = chunk loading, 0 = chunk loaded
/******/ 	var installedChunks = {
/******/ 		"app": 0
/******/ 	};
/******/
/******/ 	var deferredModules = [];
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	var jsonpArray = window["webpackJsonp"] = window["webpackJsonp"] || [];
/******/ 	var oldJsonpFunction = jsonpArray.push.bind(jsonpArray);
/******/ 	jsonpArray.push = webpackJsonpCallback;
/******/ 	jsonpArray = jsonpArray.slice();
/******/ 	for(var i = 0; i < jsonpArray.length; i++) webpackJsonpCallback(jsonpArray[i]);
/******/ 	var parentJsonpFunction = oldJsonpFunction;
/******/
/******/
/******/ 	// add entry module to deferred list
/******/ 	deferredModules.push([0,"vendor"]);
/******/ 	// run deferred modules when ready
/******/ 	return checkDeferredModules();
/******/ })
/************************************************************************/
/******/ ({

/***/ "./js/app/app.js":
/*!***********************!*\
  !*** ./js/app/app.js ***!
  \***********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(global, __webpack_provided_window_dot_Tether, $) {/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var tether__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tether */ "./node_modules/tether/dist/js/tether.js");
/* harmony import */ var tether__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(tether__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var bootstrap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! bootstrap */ "./node_modules/bootstrap/dist/js/bootstrap.js");
/* harmony import */ var bootstrap__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(bootstrap__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var owl_carousel__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! owl.carousel */ "./node_modules/owl.carousel/dist/owl.carousel.js");
/* harmony import */ var owl_carousel__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(owl_carousel__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var jquery_actual__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! jquery.actual */ "./node_modules/jquery.actual/jquery.actual.js");
/* harmony import */ var jquery_actual__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(jquery_actual__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var svg4everybody__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! svg4everybody */ "./node_modules/svg4everybody/dist/svg4everybody.js");
/* harmony import */ var svg4everybody__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(svg4everybody__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var lottie_web__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! lottie-web */ "./node_modules/lottie-web/build/player/lottie.js");
/* harmony import */ var lottie_web__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(lottie_web__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var jquery_bez__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! jquery-bez */ "./node_modules/jquery-bez/lib/jquery.bez.js");
/* harmony import */ var jquery_bez__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(jquery_bez__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var jquery_easing__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! jquery.easing */ "./node_modules/jquery.easing/jquery.easing.js");
/* harmony import */ var jquery_easing__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(jquery_easing__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var waypoints_lib_jquery_waypoints__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! waypoints/lib/jquery.waypoints */ "./node_modules/waypoints/lib/jquery.waypoints.js");
/* harmony import */ var waypoints_lib_jquery_waypoints__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(waypoints_lib_jquery_waypoints__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _vimeo_player__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @vimeo/player */ "./node_modules/@vimeo/player/dist/player.es.js");
/* harmony import */ var headroom_js_dist_headroom_js__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! headroom.js/dist/headroom.js */ "./node_modules/headroom.js/dist/headroom.js");
/* harmony import */ var headroom_js_dist_headroom_js__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(headroom_js_dist_headroom_js__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var headroom_js_dist_jQuery_headroom_js__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! headroom.js/dist/jQuery.headroom.js */ "./node_modules/headroom.js/dist/jQuery.headroom.js");
/* harmony import */ var headroom_js_dist_jQuery_headroom_js__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(headroom_js_dist_jQuery_headroom_js__WEBPACK_IMPORTED_MODULE_12__);
/* harmony import */ var _vendor_scrolloverflow_min__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../vendor/scrolloverflow.min */ "./js/vendor/scrolloverflow.min.js");
/* harmony import */ var _vendor_scrolloverflow_min__WEBPACK_IMPORTED_MODULE_13___default = /*#__PURE__*/__webpack_require__.n(_vendor_scrolloverflow_min__WEBPACK_IMPORTED_MODULE_13__);
/* harmony import */ var _vendor_jquery_fullPage_min__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../vendor/jquery.fullPage.min */ "./js/vendor/jquery.fullPage.min.js");
/* harmony import */ var _vendor_jquery_fullPage_min__WEBPACK_IMPORTED_MODULE_14___default = /*#__PURE__*/__webpack_require__.n(_vendor_jquery_fullPage_min__WEBPACK_IMPORTED_MODULE_14__);
/* harmony import */ var _partials_loader__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./partials/loader */ "./js/app/partials/loader.js");
/* harmony import */ var _partials_general__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./partials/general */ "./js/app/partials/general.js");
/* harmony import */ var _partials_menu__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./partials/menu */ "./js/app/partials/menu.js");
/* harmony import */ var _partials_scroll__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./partials/scroll */ "./js/app/partials/scroll.js");
/* harmony import */ var _partials_graph__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./partials/graph */ "./js/app/partials/graph.js");
/* harmony import */ var _partials_collapse__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./partials/collapse */ "./js/app/partials/collapse.js");
/* harmony import */ var _partials_video__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./partials/video */ "./js/app/partials/video.js");
/* harmony import */ var _partials_slider__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./partials/slider */ "./js/app/partials/slider.js");
/* harmony import */ var _widget_list__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./widget/list */ "./js/app/widget/list.js");
/* harmony import */ var _widget_curator_core_events__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ./widget/curator/core/events */ "./js/app/widget/curator/core/events.js");

global.$ = jquery__WEBPACK_IMPORTED_MODULE_0___default.a;
global.jQuery = jquery__WEBPACK_IMPORTED_MODULE_0___default.a;


__webpack_provided_window_dot_Tether = tether__WEBPACK_IMPORTED_MODULE_1___default.a;












window.Headroom = headroom_js_dist_headroom_js__WEBPACK_IMPORTED_MODULE_11___default.a;

















var list = new _widget_list__WEBPACK_IMPORTED_MODULE_23__["default"] ({
    debug:true, // While you're testing
    container:'#curator-feed',
    feedId:'8558f0f9-043f-4bd9-bad1-037cf10a'
});

list.on(_widget_curator_core_events__WEBPACK_IMPORTED_MODULE_24__["default"].POSTS_RENDERED, function (){
    var owl = $('#owl-feed').owlCarousel({
        autoplay: 3000,
        loop: true,
        responsive:{
            0 : {
                items: 1
            },
            767 : {
                items: 2
            },
            992 : {
                items: 4
            }
        }
    });

    $(".crt-list-post-text-wrap").text(function(index, currentText) {
        return currentText.substr(0, 175);
    });
});
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../node_modules/webpack/buildin/global.js */ "./node_modules/webpack/buildin/global.js"), __webpack_require__(/*! tether */ "./node_modules/tether/dist/js/tether.js"), __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./js/app/partials/collapse.js":
/*!*************************************!*\
  !*** ./js/app/partials/collapse.js ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_0__);


var collapse = {
    init: function init () {
        var el = jquery__WEBPACK_IMPORTED_MODULE_0___default()('.btn-collapse'),
        self = this;

        id = setTimeout( function () {
            self.setHeight();
        }, 600);


        var id;
        jquery__WEBPACK_IMPORTED_MODULE_0___default()(window).resize(function () {
            jquery__WEBPACK_IMPORTED_MODULE_0___default()('ul.accordion .body').removeAttr('style');
            jquery__WEBPACK_IMPORTED_MODULE_0___default()('ul.accordion li').removeClass('active');

            clearTimeout(id);
            id = setTimeout( function () {
                self.setHeight();
            }, 600);
        });

        jquery__WEBPACK_IMPORTED_MODULE_0___default()(document.body).on('click', '.btn-collapse', function(e){
            e.preventDefault();

            var $this = jquery__WEBPACK_IMPORTED_MODULE_0___default()(this),
                $container = $this.parents('li');
                
            //$container.toggleClass('active').find('.body').toggleClass('active');
            $container.toggleClass('active').find('.body').slideToggle(500, jquery__WEBPACK_IMPORTED_MODULE_0___default.a.bez([0.1, 0.01, 0, 1]));

            if(jquery__WEBPACK_IMPORTED_MODULE_0___default()(window).width() > 768){
                setTimeout(function(){ 
                    jquery__WEBPACK_IMPORTED_MODULE_0___default.a.fn.fullpage.reBuild();
                }, 500);
            }

            return false;
        });
    },
    setHeight: function setHeight () {
        // $('ul.accordion .body').map(function() {
        //     let self = $(this),
        //     h = self.actual( 'height' );
        //     self.height( h );
        // });

        // let pageHeight = $(window).height();
        // $('ul.accordion .body').css('height', function() {
        //     let h = $(this).innerHeight();
        //     console.log($(this).outerHeight(true));
        //     return h;
        // });
    }
};

// call init here
collapse.init();

/* harmony default export */ __webpack_exports__["default"] = (collapse);

/***/ }),

/***/ "./js/app/partials/general.js":
/*!************************************!*\
  !*** ./js/app/partials/general.js ***!
  \************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var svg4everybody__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! svg4everybody */ "./node_modules/svg4everybody/dist/svg4everybody.js");
/* harmony import */ var svg4everybody__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(svg4everybody__WEBPACK_IMPORTED_MODULE_0__);


var general = {
    init: function init () {
        general.svg();
    },
    svg: function svg () {
        svg4everybody__WEBPACK_IMPORTED_MODULE_0___default()();
    }
};

// call init here
general.init();

/* harmony default export */ __webpack_exports__["default"] = (general);

/***/ }),

/***/ "./js/app/partials/graph.js":
/*!**********************************!*\
  !*** ./js/app/partials/graph.js ***!
  \**********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_0__);


var graph = {
    init: function init () {
        graph.current();
    },
    current: function current () {
        var el = jquery__WEBPACK_IMPORTED_MODULE_0___default()('#current-graph'),
            now = el.data('current'),
            target = el.data('target');

        var total = (now / target) * 100;
        el.find('.progress').height(total + '%');
    }
};

// call init here
graph.init();

/* harmony default export */ __webpack_exports__["default"] = (graph);

/***/ }),

/***/ "./js/app/partials/loader.js":
/*!***********************************!*\
  !*** ./js/app/partials/loader.js ***!
  \***********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var lottie_web__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! lottie-web */ "./node_modules/lottie-web/build/player/lottie.js");
/* harmony import */ var lottie_web__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(lottie_web__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _partials_video__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../partials/video */ "./js/app/partials/video.js");
/* harmony import */ var _widget_curator_core_logger__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../widget/curator/core/logger */ "./js/app/widget/curator/core/logger.js");





var loader = {
    init: function init () {
        var json = jquery__WEBPACK_IMPORTED_MODULE_0___default()('#bm').data('json'),
            elLoader = document.getElementById('bm');
        
        lottie_web__WEBPACK_IMPORTED_MODULE_1___default.a.loadAnimation({
            container: elLoader,
            renderer: 'svg',
            loop: true,
            autoplay: true,
            path: json
        });

        loader.run();
    },
    run: function run () {
        var el = jquery__WEBPACK_IMPORTED_MODULE_0___default()('#loader'),
        elLoader = document.getElementById('bm');
        
        window.onload = function () {
            _partials_video__WEBPACK_IMPORTED_MODULE_2__["default"].loadLoop();

            setTimeout(function () {
                _widget_curator_core_logger__WEBPACK_IMPORTED_MODULE_3__["default"].log("LOADER->run");
                elLoader.style.display = 'none';
                jquery__WEBPACK_IMPORTED_MODULE_0___default()('section, .footer-1, header, .sticky').not('#section-feed').addClass('loaded');
                jquery__WEBPACK_IMPORTED_MODULE_0___default()('.block-heading').addClass('loaded');
                jquery__WEBPACK_IMPORTED_MODULE_0___default()('body').removeClass('loading');
                el.addClass('active');    
            }, 2500);

            setTimeout(function () {
                jquery__WEBPACK_IMPORTED_MODULE_0___default()('.no-gutters').removeClass('transform-transition');
            }, 2700);

            setTimeout(function () {
                jquery__WEBPACK_IMPORTED_MODULE_0___default()('.block-heading').addClass('animate-top');
            }, 3500);
        };
    }
};

// call init here
loader.init();

/* harmony default export */ __webpack_exports__["default"] = (loader);

/***/ }),

/***/ "./js/app/partials/menu.js":
/*!*********************************!*\
  !*** ./js/app/partials/menu.js ***!
  \*********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(global, $) {/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var headroom_js_dist_headroom_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! headroom.js/dist/headroom.js */ "./node_modules/headroom.js/dist/headroom.js");
/* harmony import */ var headroom_js_dist_headroom_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(headroom_js_dist_headroom_js__WEBPACK_IMPORTED_MODULE_1__);

global.$ = jquery__WEBPACK_IMPORTED_MODULE_0___default.a;
global.jQuery = jquery__WEBPACK_IMPORTED_MODULE_0___default.a;


window.Headroom = headroom_js_dist_headroom_js__WEBPACK_IMPORTED_MODULE_1___default.a;

var menu = {
    moved: false,
    movedTo: '',
    init: function init () {
        menu.header();
        menu.section();
    },
    header: function header () {
        var this$1 = this;

        var menu = $('.header-menu'),
            menuH = $('#menu-header-nav').actual('height'),
            menuLink = $('.header-menu a'),
            headerH = $('header').height();

        $('.menu-toggle').click(function (e) {
            var self = $(e.currentTarget);
            console.log('added delay toggle');
            
            self.parent().find('.header-menu').toggleClass('active');
            self.parents('.sticky-el').find('.block-heading').toggleClass('active');
            self.parents('section').find('h1').toggleClass('active');
            return false;
        });

        var flag = true;
        $('.sticky-header .menu-toggle').click(function (e) {
            var self = $(e.currentTarget);

            if(flag){
                self.removeClass('collapsed');
                flag = false;

                $('html').addClass('mobile-menu');
            }else{
                self.addClass('collapsed');
                flag = true;

                setTimeout(function(){ 
                    $('.sticky-header').removeClass('headroom--unpinned').addClass('headroom--pinned');
                }, 500);
                $('html').removeClass('mobile-menu');
                $('body').removeClass('loading');
            }

            return false;
        });



        $(document.body).on('click','.header-menu a', function (e) {
            var url = $(e.currentTarget).attr('href'), 
            id = url.replace('#section-',''),
            q = this$1.urlParameter(url, 'block');
              
            this$1.goTo(id.split('?')[0], q);

            if($(window).width() < 769) {
                $('.menu-toggle').addClass('collapsed');
                $('.header-menu').removeClass('active');
                flag = true;
            }
            
            return false;
        });

        $(document.body).on('click','.btn-move-who', function (e) {
            var id = $(e.currentTarget).attr('href').replace('#section-','');
            this$1.goTo(id, 'who-gallery-1');
            
            return false;
        });

        $('.sticky-header').headroom({
            onUnpin : function() {
                $('.sticky-header').find('.header-menu').removeClass('active');
            },
            onTop : function() {
                $('.sticky-header').find('.header-menu').removeClass('active');
            }
        });
    },
    section: function section () {
        $('.btn-next, .block-image .bg-img').click(function (e) {
            var $this = $(e.currentTarget);

            if($(window).width() > 768){
                if($this.parents('.fp-section').attr('id') == 'section-1'){
                    $.fn.fullpage.moveSectionDown();
                }else{
                    var iscroll = $('.fp-section.active').find('.fp-scrollable').data('iscrollInstance');
                    if(iscroll && typeof iscroll !== undefined){
                        iscroll.scrollToElement('.fp-section.active .row-down');
                    };
                }
            }else{
                if($this.parents('.section').attr('id') == 'section-1'){
                    $('html, body').animate({ 
                        scrollTop: $this.parents('.section').next().offset().top 
                    }, 800, $.bez("easeOutCubic")); 
                }else{
                    $('html, body').animate({ 
                        scrollTop: $this.parents('.section').find('.row-down').offset().top 
                    }, 800, $.bez("easeOutCubic"));
                }
            }

            return false;
        });
    },
    urlParameter: function urlParameter(url, name) {
        var newUrl = (RegExp(name + '=' + '(.+?)(&|$)').exec(url)||[,null])[1];
        return newUrl != null ? newUrl : '';
    },
    goTo: function goTo (id, _move) {
        if ( _move === void 0 ) _move = '';

        //console.log(id);
        menu.moved = true;
        menu.movedTo = _move;

        if($(window).width() > 768){
            $.fn.fullpage.moveTo(id);
        }
        else{
            $('html, body').animate({
                scrollTop: $('#section-'+id).offset().top
            }, 800, $.bez("easeOutCubic"));
        }
    }
};

// call init here
menu.init();

/* harmony default export */ __webpack_exports__["default"] = (menu);
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../node_modules/webpack/buildin/global.js */ "./node_modules/webpack/buildin/global.js"), __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./js/app/partials/scroll.js":
/*!***********************************!*\
  !*** ./js/app/partials/scroll.js ***!
  \***********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _partials_menu__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../partials/menu */ "./js/app/partials/menu.js");



var fullpageActive = false;

var scroll = {
    init: function init () {
        var this$1 = this;

        this.header();

        var id;
        jquery__WEBPACK_IMPORTED_MODULE_0___default()(window).resize(function () {
            clearTimeout(id);
            id = setTimeout( function () {
                if(jquery__WEBPACK_IMPORTED_MODULE_0___default()(window).width() > 768) {
                    if (!fullpageActive) {
                        this$1.initFullpage();
                    } else {
                        jquery__WEBPACK_IMPORTED_MODULE_0___default()('#section-feed').removeClass('loaded');
                        jquery__WEBPACK_IMPORTED_MODULE_0___default()('.footer-bottom').insertBefore('#section-feed .fp-scroller').addClass('loaded');

                        setTimeout(function () {
                            console.log('rebuild fullpage 2');
                            jquery__WEBPACK_IMPORTED_MODULE_0___default.a.fn.fullpage.reBuild();
                        }, 1000);

                        // setTimeout(function () {
                        //     $('#section-5').addClass('loaded');
                        // }, 1500);
                        
                    }
                }
                else {
                    if (fullpageActive) {
                        console.log('trigger resize');
                        scroll.destroyFullpage();
                    }
                }
            }, 100);
        });

        if (jquery__WEBPACK_IMPORTED_MODULE_0___default()(window).width() > 768) {
            this.initFullpage();
        }
    },

    initFullpage: function initFullpage () {
        fullpageActive = true;

        jquery__WEBPACK_IMPORTED_MODULE_0___default()('#fullpage').fullpage({
            licenseKey: 'OPEN-SOURCE-GPLV3-LICENSE',
            scrollOverflowOptions: {
                disablePointer: true
            },
            responsiveWidth: 768,
            scrollOverflow: true,
            loopBottom: false,
	        loopTop: false,
            css3: true,
            easing: 'easeInOutCubic', //this use if css3 is false
            easingcss3: 'cubic-bezier(0.215, 0.610, 0.355, 1.000)',
            afterRender: function () {
                //$('video[loop]').get(0).play();
            },
            afterLoad: function(){
                var title = jquery__WEBPACK_IMPORTED_MODULE_0___default()(this).data('title'),
                id = this[0].id.replace('section-','');
                jquery__WEBPACK_IMPORTED_MODULE_0___default()('.block-heading').find('h1').text(title);

                if(this[0].id == 'section-1' && jquery__WEBPACK_IMPORTED_MODULE_0___default()(window).width() > 768) {
                    jquery__WEBPACK_IMPORTED_MODULE_0___default()('.block-video').removeClass('overflow-hidden');
                }else{
                    jquery__WEBPACK_IMPORTED_MODULE_0___default()('.block-video').addClass('overflow-hidden');
                }

                if(this[0].id == 'section-4' && jquery__WEBPACK_IMPORTED_MODULE_0___default()(window).width() > 768) {
                    jquery__WEBPACK_IMPORTED_MODULE_0___default()('.footer-bottom').removeClass('loaded');
                }

                if(this[0].id == 'section-5' && jquery__WEBPACK_IMPORTED_MODULE_0___default()(window).width() > 768) {
                    jquery__WEBPACK_IMPORTED_MODULE_0___default()('.sticky-el').insertBefore('.block-invest');
                }

                if(this[0].id == 'section-feed' && jquery__WEBPACK_IMPORTED_MODULE_0___default()(window).width() > 768) {
                    //$('#section-feed').addClass('loaded');
                    jquery__WEBPACK_IMPORTED_MODULE_0___default()('.footer-bottom').insertBefore('#section-feed .fp-scroller').addClass('loaded');
                    jquery__WEBPACK_IMPORTED_MODULE_0___default.a.fn.fullpage.reBuild();
                }

                //this fixed for IE scroll issue (it force to move to next section)
                var iscroll = jquery__WEBPACK_IMPORTED_MODULE_0___default()('.fp-section.active').find('.fp-scrollable').data('iscrollInstance');
                if(iscroll && typeof iscroll !== undefined){
                    iscroll.on('scrollEnd', function(){

                        if(this.y == this.maxScrollY){
                            jquery__WEBPACK_IMPORTED_MODULE_0___default.a.fn.fullpage.moveTo(parseInt(id) + 1);
                        }

                        if(this.y == 0){
                            jquery__WEBPACK_IMPORTED_MODULE_0___default.a.fn.fullpage.moveTo(parseInt(id) - 1);
                        }
                    });
                };

                _partials_menu__WEBPACK_IMPORTED_MODULE_1__["default"].moved = false;

                // console.log(prevIsScroll);
                // console.log(this);
            },
            onLeave: function(index, nextIndex, direction){
                var id = this[0].id.replace('section-','');

                if(id == '4' && jquery__WEBPACK_IMPORTED_MODULE_0___default()(window).width() > 768) {
                    jquery__WEBPACK_IMPORTED_MODULE_0___default()('.scrollable-el').scrollTop(2);
                }
                if(id == '5' && jquery__WEBPACK_IMPORTED_MODULE_0___default()(window).width() > 768 && direction == 'up') {
                    jquery__WEBPACK_IMPORTED_MODULE_0___default()('.sticky-el').insertBefore('#fullpage');
                }

                if(id == 'feed' && jquery__WEBPACK_IMPORTED_MODULE_0___default()(window).width() > 768) {
                    jquery__WEBPACK_IMPORTED_MODULE_0___default()('#section-feed').removeClass('loaded');
                    jquery__WEBPACK_IMPORTED_MODULE_0___default()('.footer-bottom').insertAfter('.main').removeClass('loaded');
                }

                //this fixed for card https://trello.com/c/IZntBDgz
                var prevEl = jquery__WEBPACK_IMPORTED_MODULE_0___default()('#section-' + (nextIndex - 1) ),
                prevIsScroll = prevEl.find('.fp-scrollable').data('iscrollInstance');

                if(prevIsScroll && typeof prevIsScroll !== undefined){
                    prevIsScroll.scrollTo(0, prevIsScroll.maxScrollY);
                }

                var currentEl = jquery__WEBPACK_IMPORTED_MODULE_0___default()('#section-' + nextIndex),
                currentIsScroll = currentEl.find('.fp-scrollable').data('iscrollInstance');

                //console.log(menu.movedTo);
                if(currentIsScroll && typeof currentIsScroll !== undefined && _partials_menu__WEBPACK_IMPORTED_MODULE_1__["default"].moved && _partials_menu__WEBPACK_IMPORTED_MODULE_1__["default"].movedTo == ''){
                    currentIsScroll.scrollTo(0, 0);
                }else if(currentIsScroll && typeof currentIsScroll !== undefined && _partials_menu__WEBPACK_IMPORTED_MODULE_1__["default"].moved && _partials_menu__WEBPACK_IMPORTED_MODULE_1__["default"].movedTo != ''){
                    currentIsScroll.scrollToElement('#'+_partials_menu__WEBPACK_IMPORTED_MODULE_1__["default"].movedTo);
                }

                //console.log(nextIndex);
                //console.log(currentEl);
            }
        });
    },

    destroyFullpage: function destroyFullpage () {
        console.log('destroy fullpage');
        jquery__WEBPACK_IMPORTED_MODULE_0___default()('.footer-bottom').insertAfter('.main').removeClass('loaded');
        jquery__WEBPACK_IMPORTED_MODULE_0___default.a.fn.fullpage.destroy('all');
        fullpageActive = false;
    },

    isScrolledIntoView: function isScrolledIntoView(elem) {
        if (jquery__WEBPACK_IMPORTED_MODULE_0___default()(elem).length == 0) {
            return false;
        }
        var docViewTop = jquery__WEBPACK_IMPORTED_MODULE_0___default()(window).scrollTop();
        var docViewBottom = docViewTop + jquery__WEBPACK_IMPORTED_MODULE_0___default()(window).height();

        var elemTop = jquery__WEBPACK_IMPORTED_MODULE_0___default()(elem).offset().top;
        var elemBottom = elemTop + jquery__WEBPACK_IMPORTED_MODULE_0___default()(elem).height();

        return (docViewBottom >= elemTop && docViewTop);
    },
    header: function header() {
        var blockImage = jquery__WEBPACK_IMPORTED_MODULE_0___default()('.block-image, .block-video');

        blockImage.waypoint({
            handler: function(direction) {
                if(direction == 'down'){
                    jquery__WEBPACK_IMPORTED_MODULE_0___default()('.sticky-header').addClass('disable-header');
                }else{
                    jquery__WEBPACK_IMPORTED_MODULE_0___default()('.sticky-header').removeClass('disable-header');
                }
                //console.log(direction);
            }
        });

        jquery__WEBPACK_IMPORTED_MODULE_0___default()('.block-who-we-are, .block-what-we-do, .block-get-involved').waypoint({
            handler: function(direction) {
                if(direction == 'up'){
                    jquery__WEBPACK_IMPORTED_MODULE_0___default()('.sticky-header').addClass('disable-header');
                }else{
                    jquery__WEBPACK_IMPORTED_MODULE_0___default()('.sticky-header').removeClass('disable-header');
                }

            }
        });

        // $(scroll).on(function(){
        //     let $header = $(".sticky-header");
        //     let $self = $(".block-image");
        //     let fixed_position = $header.offset().top,
        //     fixed_height = $header.height(),
        //     toCross_position = $self.offset().top,
        //     toCross_height = $self.height();

        //     if (fixed_position + fixed_height  < toCross_position) {
        //         $header.removeClass('disable-header');
        //     } else if (fixed_position > toCross_position + toCross_height) {
        //         $header.removeClass('disable-header');
        //     } else {
        //         $header.addClass('disable-header');
        //     }
        // });
    }
};

// call init here
scroll.init();

/* harmony default export */ __webpack_exports__["default"] = (scroll);

/***/ }),

/***/ "./js/app/partials/slider.js":
/*!***********************************!*\
  !*** ./js/app/partials/slider.js ***!
  \***********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_0__);

//import List from '../widget/list';
// import Events from '../widget/curator/core/events';

var slider = {
    init: function init () {
        slider.gallery();
        slider.invest();
        //slider.feed();
    },
    gallery: function gallery () {
        var $el = jquery__WEBPACK_IMPORTED_MODULE_0___default()(".gallery");

        $el.each( function (index, element) {
            var $this = jquery__WEBPACK_IMPORTED_MODULE_0___default()(element),
                $container = $this.parents('.gallery-wrapper');
            
            var owl = $this.owlCarousel({
                center: true,
                items: 1,
                margin: 20,
                video: true,
                onInitialized: slider.loaded,
                onChanged: slider.change,
                onDrag: slider.drag
            });

            owl.on('initialize.owl.carousel', function(event){ 
                // Do something
                console.log( 'Owl is loaded!' );
            });
            
            owl.on('changed.owl.carousel', function(event) {
                $el.find('iframe').remove();
                console.log('changed');
            });

            $container.find(".gallery-prev").click(function () {
                owl.trigger('prev.owl.carousel');
                return false;
            });

            $container.find(".gallery-next").click(function () {
                owl.trigger('next.owl.carousel');
                return false;
            });

            $container.find(".nav-tabs").children('li').children('a').click( function (e) {
                var self = jquery__WEBPACK_IMPORTED_MODULE_0___default()(e.currentTarget), 
                    indexSlider = self.attr('slider'),
                    indexItem = self.parent().index();
                
                if(jquery__WEBPACK_IMPORTED_MODULE_0___default()('#who-gallery-'+indexSlider).hasClass('disable-slider') == false){
                    owl.trigger('to.owl.carousel', indexItem);
                }
            });
        });

        var maxHeight=0;
        jquery__WEBPACK_IMPORTED_MODULE_0___default()(".tab-content .tab-pane").each(function (e) {
            var $this = jquery__WEBPACK_IMPORTED_MODULE_0___default()(e.currentTarget), 
            height = $this.height();
            maxHeight = height>maxHeight?height:maxHeight;
            if ( height > maxHeight ) {
                $this.css('min-height', height);
            }
        });

        jquery__WEBPACK_IMPORTED_MODULE_0___default()(".gallery-wrapper").each(function() {
            var $self = jquery__WEBPACK_IMPORTED_MODULE_0___default()(this),
            tabs = $self.find('.tab-pane'),
            maxHeight = 0;
            tabs.each(function() {
                var $child = jquery__WEBPACK_IMPORTED_MODULE_0___default()(this);
                if(maxHeight < $child.height()) {
                    maxHeight = $child.height();
                }
            });
            
            tabs.css('min-height', maxHeight+'px');
        });
    },
    invest: function invest (){
        var $el = jquery__WEBPACK_IMPORTED_MODULE_0___default()("#invest");

        var owl = $el.owlCarousel({
            center: true,
            items: 1, 
            rtl: true,
            touchDrag  : false,
            mouseDrag  : false,
            autoHeight: true,
        });

        jquery__WEBPACK_IMPORTED_MODULE_0___default()('.btn-panel').click(function (e) {
            var $self = jquery__WEBPACK_IMPORTED_MODULE_0___default()(e.currentTarget);
            var panel = $self.data('panel');

            jquery__WEBPACK_IMPORTED_MODULE_0___default()('.panel-in').hide();
            jquery__WEBPACK_IMPORTED_MODULE_0___default()('#'+panel).show();
            owl.trigger('to.owl.carousel', 1);


        });

        jquery__WEBPACK_IMPORTED_MODULE_0___default()('.btn-panel-back').click(function (e) {
            owl.trigger('to.owl.carousel', 0);

            return false;
        });
    },
    feed: function feed () {
        var list = new List ({
            debug:true, // While you're testing
            container:'#curator-feed',
            feedId:'8558f0f9-043f-4bd9-bad1-037cf10a'
        });

        list.on(Events.POSTS_RENDERED, function (){
            var owl = jquery__WEBPACK_IMPORTED_MODULE_0___default()('#owl-feed').owlCarousel({
                autoplay: 3000,
                loop: true,
                responsive:{
                    0 : {
                        items: 1
                    },
                    767 : {
                        items: 2
                    },
                    992 : {
                        items: 4
                    }
                }
            });

            jquery__WEBPACK_IMPORTED_MODULE_0___default()(".crt-list-post-text-wrap").text(function(index, currentText) {
                return currentText.substr(0, 175);
            });
        });
        
    },
    loaded: function loaded () {
        setTimeout(function(){ 
            jquery__WEBPACK_IMPORTED_MODULE_0___default()('.owl-video-play-icon').html('<span></span>');
        }, 1500);
    },
    change: function change (event) {
        var el = event.target,
            index = event.page.index == -1 ? 0 : event.page.index,
            container = jquery__WEBPACK_IMPORTED_MODULE_0___default()(el).parents('.gallery-wrapper');
            
        container.find('.nav-tabs').children('li').eq(index).children('a').tab('show');   
        container.find('.current').text(index + 1);

        jquery__WEBPACK_IMPORTED_MODULE_0___default()('.gallery').removeClass('hold');
    },
    drag: function drag () {
        jquery__WEBPACK_IMPORTED_MODULE_0___default()('.gallery').addClass('hold');
        console.log('dragging');
    }
};

// call init here
slider.init();

/* harmony default export */ __webpack_exports__["default"] = (slider);

/***/ }),

/***/ "./js/app/partials/video.js":
/*!**********************************!*\
  !*** ./js/app/partials/video.js ***!
  \**********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _vimeo_player__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @vimeo/player */ "./node_modules/@vimeo/player/dist/player.es.js");
/* harmony import */ var _widget_curator_core_logger__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../widget/curator/core/logger */ "./js/app/widget/curator/core/logger.js");




var video = {
    vimeo: null,
    init: function init () {
        jquery__WEBPACK_IMPORTED_MODULE_0___default()(document.body).on('click', '.video', function () {
            var self = jquery__WEBPACK_IMPORTED_MODULE_0___default()(this);

            if(self.get(0).paused){
                self.get(0).play();
                self.parent().children(".playpause").fadeOut();
            }else{
                self.get(0).pause();
                self.parent().children(".playpause").fadeIn();
            }
        });

        jquery__WEBPACK_IMPORTED_MODULE_0___default()('#loop').hover(function() {
            jquery__WEBPACK_IMPORTED_MODULE_0___default()(this).parents('section').addClass('hovered');
        },
        function() {
            jquery__WEBPACK_IMPORTED_MODULE_0___default()(this).parents('section').removeClass('hovered');
        });

        var flag = true,
        self = this,
        vidLoaded = false;
        jquery__WEBPACK_IMPORTED_MODULE_0___default()('#loop').click(function (e) {
            var $self = jquery__WEBPACK_IMPORTED_MODULE_0___default()(e.currentTarget);

            $self.parents('.block-video').parent().addClass('active transform-transition');
            jquery__WEBPACK_IMPORTED_MODULE_0___default()('.bg-video .info').hide();
            jquery__WEBPACK_IMPORTED_MODULE_0___default()('.bg-video .mobile-view').addClass('d-none');
            jquery__WEBPACK_IMPORTED_MODULE_0___default()('.main').addClass('clear-left');
            jquery__WEBPACK_IMPORTED_MODULE_0___default()('body').addClass('loading');
            jquery__WEBPACK_IMPORTED_MODULE_0___default()('#section-1').addClass('playing').removeClass('hovered');
            
            if(flag){
                setTimeout(function () {
                    jquery__WEBPACK_IMPORTED_MODULE_0___default()('#loop').hide();

                    if(jquery__WEBPACK_IMPORTED_MODULE_0___default()(window).width() > 768 && vidLoaded == false) {
                        this.video = document.createElement("video");
                        this.video.controls = false;
                        this.video.id = "vid";
                        this.video.style.cssText = "display: none;";
                        // this.video.autoplay = true;
                        // this.video.loop = true;
                        // this.video.muted = true;

                        this.source1 = document.createElement("source");
                        this.source1.setAttribute("src","https://s3.amazonaws.com/maud-choice/the-choice-foundation-main-video-final-html5.mp4");
                        this.source1.setAttribute("type", "video/mp4");
                        this.video.appendChild(this.source1);

                        this.source2 = document.createElement("source");
                        this.source2.setAttribute("src","https://s3.amazonaws.com/maud-choice/the-choice-foundation-main-video-final-html5.webm");
                        this.source2.setAttribute("type", "video/webm");
                        this.video.appendChild(this.source2);

                        this.source3 = document.createElement("source");
                        this.source3.setAttribute("src","https://s3.amazonaws.com/maud-choice/the-choice-foundation-main-video-final-html5.3gp");
                        this.source3.setAttribute("type", "video/3gp");
                        this.video.appendChild(this.source3);

                        document.getElementById('vid-container').appendChild(this.video);

                        var vid = this.video,
                        progressBar = jquery__WEBPACK_IMPORTED_MODULE_0___default()('#progress-bar');

                        vid.ontimeupdate = function (e) {
                            var percentage = Math.floor((100 /  vid.duration) * vid.currentTime);
                            progressBar.val(percentage);
                            progressBar.html( percentage + '% played' );

                            video.seektimeupdate( vid );
                        };

                        vid.onended = function () {
                            jquery__WEBPACK_IMPORTED_MODULE_0___default()('.ctrl-play').addClass('paused');
                        };

                        jquery__WEBPACK_IMPORTED_MODULE_0___default()(document.body).on('click', '.ctrl-play, #vid', function() {
                            console.log('triggered video');
                            if (vid.paused || vid.ended){
                                vid.play();
                                jquery__WEBPACK_IMPORTED_MODULE_0___default()('.ctrl-play').removeClass('paused');
                            }
                            else {
                                vid.pause();
                                jquery__WEBPACK_IMPORTED_MODULE_0___default()('.ctrl-play').addClass('paused');
                            }
                        });

                        progressBar.on('click', video.seektime); 

                        video.volumeController(vid);
                        video.screenController(vid);

                        vidLoaded = true;
                    }

                    if(jquery__WEBPACK_IMPORTED_MODULE_0___default()(window).width() > 768) {
                        jquery__WEBPACK_IMPORTED_MODULE_0___default()('#vid').fadeIn(function(){
                            jquery__WEBPACK_IMPORTED_MODULE_0___default()(this)[0].play();
                            jquery__WEBPACK_IMPORTED_MODULE_0___default()('.vid-controls').stop(true, true)
                            .animate({
                                height:"toggle",
                                opacity:"toggle"
                            },700); 
                        });
                    }
                    else{
                        var iframe = jquery__WEBPACK_IMPORTED_MODULE_0___default()('#vid-mobile iframe');
                        self.vimeo = new _vimeo_player__WEBPACK_IMPORTED_MODULE_1__["default"](iframe);

                        jquery__WEBPACK_IMPORTED_MODULE_0___default()('#vid-mobile').fadeIn(function () {
                            self.vimeo.play();
                        });
                    }
                   
                }, 500);
                
                flag = false;
            }

            if(jquery__WEBPACK_IMPORTED_MODULE_0___default()(window).width() > 768) {
                jquery__WEBPACK_IMPORTED_MODULE_0___default.a.fn.fullpage.setAutoScrolling(false);
            }
        });

        jquery__WEBPACK_IMPORTED_MODULE_0___default()(document.body).on('click', '.btn-back', function(){
            flag = true;

            setTimeout(function () {
                jquery__WEBPACK_IMPORTED_MODULE_0___default()('.block-video').removeClass('active').find('#loop').removeAttr("controls","controls").attr('muted', 'muted');
                jquery__WEBPACK_IMPORTED_MODULE_0___default()('.main').removeClass('clear-left');
                jquery__WEBPACK_IMPORTED_MODULE_0___default()('body').removeClass('loading');
                jquery__WEBPACK_IMPORTED_MODULE_0___default()('#vid, #vid-mobile').hide();
                jquery__WEBPACK_IMPORTED_MODULE_0___default()('#loop').show();
                jquery__WEBPACK_IMPORTED_MODULE_0___default()('#loop').parents('.block-video').parent().removeClass('active');
                jquery__WEBPACK_IMPORTED_MODULE_0___default()('.bg-video .info').show();
                jquery__WEBPACK_IMPORTED_MODULE_0___default()('.bg-video .mobile-view').removeClass('d-none');
                jquery__WEBPACK_IMPORTED_MODULE_0___default()('.ctrl-play').removeClass('paused');
                jquery__WEBPACK_IMPORTED_MODULE_0___default()('#vid')[0].pause();
                self.vimeo.pause();
                
                jquery__WEBPACK_IMPORTED_MODULE_0___default()('#section-1').removeClass('playing');
            }, 200);

            setTimeout(function () {
                jquery__WEBPACK_IMPORTED_MODULE_0___default()('#loop').parents('.block-video').parent().removeClass('transform-transition');
            }, 1000);
            
            if(jquery__WEBPACK_IMPORTED_MODULE_0___default()(window).width() > 768) {
                jquery__WEBPACK_IMPORTED_MODULE_0___default()('.vid-controls').stop(true, true)
                .animate({
                    height:"toggle",
                    opacity:"toggle"
                },300);

                jquery__WEBPACK_IMPORTED_MODULE_0___default.a.fn.fullpage.setAutoScrolling(true);
            }
        });
    },
    loadLoop: function loadLoop(){
        _widget_curator_core_logger__WEBPACK_IMPORTED_MODULE_2__["default"].log("LOOP->loaded");
        this.loop = document.createElement("video");
        this.loop.controls = false;
        this.loop.id = "loop-video";
        this.loop.autoplay = true;
        this.loop.attributeName = "data-autoplay";
        this.loop.preload = "auto";
        this.loop.loop = true;
        this.loop.muted = true;

        this.loopSource1 = document.createElement("source");
        this.loopSource1.setAttribute("src","https://s3.amazonaws.com/maud-choice/the-choice-foundation-looping-video-blue.mp4");
        this.loopSource1.setAttribute("type", "video/mp4");
        this.loop.appendChild(this.loopSource1);

        this.loopSource2 = document.createElement("source");
        this.loopSource2.setAttribute("src","https://s3.amazonaws.com/maud-choice/the-choice-foundation-looping-video-blue.webm");
        this.loopSource2.setAttribute("type", "video/webm");
        this.loop.appendChild(this.loopSource2);

        this.loopSource3 = document.createElement("source");
        this.loopSource3.setAttribute("src","https://s3.amazonaws.com/maud-choice/the-choice-foundation-looping-video-blue.3gp");
        this.loopSource3.setAttribute("type", "video/3gp");
        this.loop.appendChild(this.loopSource3);

        document.getElementById('loop').appendChild(this.loop);

        this.loop.load();
        this.loop.play();
    },
    volumeController: function volumeController(vid){
        jquery__WEBPACK_IMPORTED_MODULE_0___default()('.ctrl-vol').on('click', function (e) {
            var self = jquery__WEBPACK_IMPORTED_MODULE_0___default()(e.currentTarget);

            if (vid.muted) {
                vid.muted = false;
                self.removeClass('muted');
            }
            else {
                vid.muted = true;
                self.addClass('muted'); 
            }
        });
    },
    screenController: function screenController(vid){
        jquery__WEBPACK_IMPORTED_MODULE_0___default()('.ctrl-screen').on('click', function (e) {
            if (vid.requestFullscreen)
                { if (document.fullScreenElement) {
                    document.cancelFullScreen();
                } else {
                    vid.requestFullscreen();
                } }
                else if (vid.msRequestFullscreen)
                { if (document.msFullscreenElement) {
                    document.msExitFullscreen();
                } else {
                    vid.msRequestFullscreen();
                } }
                else if (vid.mozRequestFullScreen)
                { if (document.mozFullScreenElement) {
                    document.mozCancelFullScreen();
                } else {
                    vid.mozRequestFullScreen();
                } }
                else if (vid.webkitRequestFullscreen)
                { if (document.webkitFullscreenElement) {
                    document.webkitCancelFullScreen();
                } else {
                    vid.webkitRequestFullscreen();
                } }
            else {
                console.log("Fullscreen API is not supported");
            }
        });
    },
    seektime: function seektime(e){
        var vid = jquery__WEBPACK_IMPORTED_MODULE_0___default()('#vid')[0], 
        progressBar = jquery__WEBPACK_IMPORTED_MODULE_0___default()('#progress-bar');

        var percent = e.offsetX / this.offsetWidth;
        vid.currentTime = percent * vid.duration;
        e.target.value = Math.floor(percent / 100);
        e.target.innerHTML = progressBar[0].value + '% played';
    },
    seektimeupdate: function seektimeupdate(vid){
        var nt = vid.currentTime * (100 / vid.duration),
        curmins = Math.floor(vid.currentTime / 60),
        cursecs = Math.floor(vid.currentTime - curmins * 60),
        durmins = Math.floor(vid.duration / 60),
        dursecs = Math.floor(vid.duration - durmins * 60);
        
        if(cursecs < 10){ cursecs = "0"+cursecs; }
        if(dursecs < 10){ dursecs = "0"+dursecs; }
        if(curmins < 10){ curmins = curmins; }
        if(durmins < 10){ durmins = durmins; }
        
        jquery__WEBPACK_IMPORTED_MODULE_0___default()('.vid-controls .current').html(curmins+":"+cursecs);
        jquery__WEBPACK_IMPORTED_MODULE_0___default()('.vid-controls .duration').html(durmins+":"+dursecs);
    }
};

// call init here
video.init();

/* harmony default export */ __webpack_exports__["default"] = (video);

/***/ }),

/***/ "./js/app/widget/curator/config/networks.js":
/*!**************************************************!*\
  !*** ./js/app/widget/curator/config/networks.js ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
var networks = {
    1 : {
        id: 1,
        name:'Twitter',
        slug:'twitter',
        icon:'crt-icon-twitter'
    },
    2 : {
        id: 2,
        name:'Instagram',
        slug:'instagram',
        icon:'crt-icon-instagram'
    },
    3 : {
        id: 3,
        name:'Facebook',
        slug:'facebook',
        icon:'crt-icon-facebook'
    },
    4 : {
        id: 4,
        name:'Pinterest',
        slug:'pinterest',
        icon:'crt-icon-pinterest'
    },
    5 : {
        id: 5,
        name:'Google',
        slug:'google',
        icon:'crt-icon-google'
    },
    6 : {
        id: 6,
        name:'Vine',
        slug:'vine',
        icon:'crt-icon-vine'
    },
    7 : {
        id: 7,
        name:'Flickr',
        slug:'flickr',
        icon:'crt-icon-flickr'
    },
    8 : {
        id: 8,
        name:'Youtube',
        slug:'youtube',
        icon:'crt-icon-youtube'
    },
    9 : {
        id: 9,
        name:'Tumblr',
        slug:'tumblr',
        icon:'crt-icon-tumblr'
    },
    10 : {
        id: 10,
        name:'RSS',
        slug:'rss',
        icon:'crt-icon-rss'
    },
    11 : {
        id: 11,
        name:'LinkedIn',
        slug:'linkedin',
        icon:'crt-icon-linkedin'
    },
};

/* harmony default export */ __webpack_exports__["default"] = (networks);

/***/ }),

/***/ "./js/app/widget/curator/config/widget_base.js":
/*!*****************************************************!*\
  !*** ./js/app/widget/curator/config/widget_base.js ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);

var ConfigWidgetBase = {
    apiEndpoint: 'https://api.curator.io/v1.2',
    feedId:'',
    postsPerPage:12,
    maxPosts:0,
    templatePost:'post-v2',
    templatePopup:'popup',
    templatePopupWrapper:'popup-wrapper',
    templateFilter:'filter',
    lang:'en',
    debug:false,
    postClickAction:'open-popup',             // open-popup | goto-source | nothing
    postClickReadMoreAction:'open-popup',     // open-popup | goto-source | nothing
    filter: {
        showNetworks: false,
        showSources: false,
    }
};

/* harmony default export */ __webpack_exports__["default"] = (ConfigWidgetBase);

/***/ }),

/***/ "./js/app/widget/curator/config/widget_list.js":
/*!*****************************************************!*\
  !*** ./js/app/widget/curator/config/widget_list.js ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _widget_base__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./widget_base */ "./js/app/widget/curator/config/widget_base.js");
/* harmony import */ var _core_lib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../core/lib */ "./js/app/widget/curator/core/lib.js");





var ConfigWidgetList = _core_lib__WEBPACK_IMPORTED_MODULE_1__["default"].extend({}, _widget_base__WEBPACK_IMPORTED_MODULE_0__["default"], {
    templatePost:'list-post',
    templateFeed:'list-feed',
    animate:false,
    list: {
        showLoadMore:true,
    }
});

/* harmony default export */ __webpack_exports__["default"] = (ConfigWidgetList);

/***/ }),

/***/ "./js/app/widget/curator/core/ajax.js":
/*!********************************************!*\
  !*** ./js/app/widget/curator/core/ajax.js ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _libraries_nanoajax__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../libraries/nanoajax */ "./js/app/widget/curator/libraries/nanoajax.js");


var serialize = function serialize( obj ) {
    return '?'+Object.keys(obj).reduce(function(a,k){a.push(k+'='+encodeURIComponent(obj[k]));return a;},[]).join('&');
};

var fixUrl = function (url) {
    var p = window.location.protocol,
        pp = url.indexOf('://');

    // IE9/IE10 cors requires same protocol
    // stripe current protocol and match window.location
    if (pp) {
        url = url.substr(pp + 3);
    }

    // if not https: or http: (eg file:) default to https:
    p = p !== 'https:' && p !== 'http:' ? 'https:' : p;
    url = p + '//' + url;
    return url;
};

var ajax = {
    get: function get (url, params, success, fail) {
        url = fixUrl(url);

        if (params) {
            url = url + serialize (params);
        }

        Object(_libraries_nanoajax__WEBPACK_IMPORTED_MODULE_0__["default"]) ({
            url:url,
            cors:true
        },function(statusCode, responseText) {
            if (statusCode) {
                success(JSON.parse(responseText));
            } else {
                fail (statusCode, responseText);
            }
        });
    },

    post: function post (url, params, success, fail) {
        url = fixUrl(url);

        Object(_libraries_nanoajax__WEBPACK_IMPORTED_MODULE_0__["default"]) ({
            url:url,
            cors:true,
            body:params,
            method:'POST'
        },function(statusCode, responseText) {
            if (statusCode) {
                success(JSON.parse(responseText));
            } else {
                fail (statusCode, responseText);
            }
        });
    }
};

/* harmony default export */ __webpack_exports__["default"] = (ajax);

/***/ }),

/***/ "./js/app/widget/curator/core/bus.js":
/*!*******************************************!*\
  !*** ./js/app/widget/curator/core/bus.js ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
var EventBus = function EventBus() {
    this.listeners = {};
};

EventBus.prototype.on = function on (type, callback, scope) {
        var arguments$1 = arguments;

    var args = [];
    var numOfArgs = arguments.length;
    for (var i = 0; i < numOfArgs; i++) {
        args.push(arguments$1[i]);
    }
    args = args.length > 3 ? args.splice(3, args.length - 1) : [];
    if (typeof this.listeners[type] !== "undefined") {
        this.listeners[type].push({scope: scope, callback: callback, args: args});
    } else {
        this.listeners[type] = [{scope: scope, callback: callback, args: args}];
    }
};

EventBus.prototype.off = function off (type, callback, scope) {
        var this$1 = this;

    if (typeof this.listeners[type] !== "undefined") {
        var numOfCallbacks = this.listeners[type].length;
        var newArray = [];
        for (var i = 0; i < numOfCallbacks; i++) {
            var listener = this$1.listeners[type][i];
            if (listener.scope === scope && listener.callback === callback) {

            } else {
                newArray.push(listener);
            }
        }
        this.listeners[type] = newArray;
    }
};

EventBus.prototype.has = function has (type, callback, scope) {
        var this$1 = this;

    if (typeof this.listeners[type] !== "undefined") {
        var numOfCallbacks = this.listeners[type].length;
        if (callback === undefined && scope === undefined) {
            return numOfCallbacks > 0;
        }
        for (var i = 0; i < numOfCallbacks; i++) {
            var listener = this$1.listeners[type][i];
            if ((scope ? listener.scope === scope : true) && listener.callback === callback) {
                return true;
            }
        }
    }
    return false;
};

EventBus.prototype.trigger = function trigger (type) {
        var arguments$1 = arguments;
        var this$1 = this;

    var numOfListeners = 0;
    var event = {
        type: type,
        // target: target
    };
    var args = [];
    // let numOfArgs = arguments.length;
    for (var i = 1; i < arguments.length; i++) {
        args.push(arguments$1[i]);
    }
    // args = args.length > 2 ? args.splice(2, args.length - 1) : [];
    args = [event].concat(args);
    if (typeof this.listeners[type] !== "undefined") {
        var numOfCallbacks = this.listeners[type].length;
        for (var i$1 = 0; i$1 < numOfCallbacks; i$1++) {
            var listener = this$1.listeners[type][i$1];
            if (listener && listener.callback) {
                var concatArgs = args.concat(listener.args);
                listener.callback.apply(listener.scope, concatArgs);
                numOfListeners += 1;
            }
        }
    }
};

EventBus.prototype.getEvents = function getEvents () {
        var this$1 = this;

    var str = "";
    for (var type in this$1.listeners) {
        var numOfCallbacks = this$1.listeners[type].length;
        for (var i = 0; i < numOfCallbacks; i++) {
            var listener = this$1.listeners[type][i];
            str += listener.scope && listener.scope.className ? listener.scope.className : "anonymous";
            str += " listen for '" + type + "'\n";
        }
    }
    return str;
};

EventBus.prototype.destroy = function destroy () {
    // Might be a bit simplistic!!!
    this.listeners = {};
};

/* harmony default export */ __webpack_exports__["default"] = (EventBus);

/***/ }),

/***/ "./js/app/widget/curator/core/events.js":
/*!**********************************************!*\
  !*** ./js/app/widget/curator/core/events.js ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
var Events = {
    FEED_LOADED             :'feed:loaded',
    FEED_FAILED             :'feed:failed',

    FILTER_CHANGED          :'filter:changed',

    POSTS_LOADED             :'posts:loaded',
    POSTS_FAILED             :'posts:failed',
    POSTS_RENDERED           :'posts:rendered',

    POST_CREATED            :'post:created',
    POST_CLICK              :'post:click',
    POST_CLICK_READ_MORE    :'post:clickReadMore',
    POST_IMAGE_LOADED       :'post:imageLoaded',
    POST_IMAGE_FAILED       :'post:imageFailed',

    CAROUSEL_CHANGED        :'carousel:changed',
    GRID_HEIGHT_CHANGED     :'grid:heightChanged'
};

/* harmony default export */ __webpack_exports__["default"] = (Events);

/***/ }),

/***/ "./js/app/widget/curator/core/feed.js":
/*!********************************************!*\
  !*** ./js/app/widget/curator/core/feed.js ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _logger__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./logger */ "./js/app/widget/curator/core/logger.js");
/* harmony import */ var _events__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./events */ "./js/app/widget/curator/core/events.js");
/* harmony import */ var _bus__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./bus */ "./js/app/widget/curator/core/bus.js");
/* harmony import */ var _ajax__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./ajax */ "./js/app/widget/curator/core/ajax.js");
/* harmony import */ var _lib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./lib */ "./js/app/widget/curator/core/lib.js");








var Feed = (function (EventBus) {
    function Feed(widget) {
        EventBus.call (this);

        _logger__WEBPACK_IMPORTED_MODULE_0__["default"].log ('Feed->init with options');

        this.widget = widget;

        this.posts = [];
        this.currentPage = 0;
        this.postsLoaded = 0;
        this.postCount = 0;
        this.loading = false;
        this.allPostsLoaded = false;
        this.pagination = {
            after:null,
            before:null
        };

        this.options = this.widget.options;

        this.params = this.options.feedParams || {};
        this.params.limit = this.options.postsPerPage;
        this.params.hasPoweredBy = this.widget.hasPoweredBy;
        this.params.version = '1.2';

        this.feedBase = this.options.apiEndpoint+'/feeds';
    }

    if ( EventBus ) Feed.__proto__ = EventBus;
    Feed.prototype = Object.create( EventBus && EventBus.prototype );
    Feed.prototype.constructor = Feed;

    Feed.prototype.loadPosts = function loadPosts (page, paramsIn) {
        page = page || 0;
        _logger__WEBPACK_IMPORTED_MODULE_0__["default"].log ('Feed->loadPosts '+this.loading);
        if (this.loading) {
            return false;
        }
        this.currentPage = page;

        if (+this.currentPage === 0) {
            this.posts = [];
            this.postsLoaded = 0;
        }

        var params = _lib__WEBPACK_IMPORTED_MODULE_4__["default"].extend({},this.params,paramsIn);

        params.limit = this.options.postsPerPage;
        params.offset = page * this.options.postsPerPage;

        this._loadPosts (params);
    };

    Feed.prototype.loadMorePaginated = function loadMorePaginated (paramsIn) {

        var params = _lib__WEBPACK_IMPORTED_MODULE_4__["default"].extend({},this.params,paramsIn);

        if (this.pagination && this.pagination.after) {
            params.after = this.pagination.after;
        }

        // console.log (params);

        this._loadPosts (params);
    };

    Feed.prototype.loadMore = function loadMore (paramsIn) {
        _logger__WEBPACK_IMPORTED_MODULE_0__["default"].log ('Feed->loadMore '+this.loading);
        if (this.loading) {
            return false;
        }

        var params = {
            limit:this.options.postsPerPage
        };
        _lib__WEBPACK_IMPORTED_MODULE_4__["default"].extend(params,this.options.feedParams, paramsIn);

        params.offset = this.posts.length;

        this._loadPosts (params);
    };

    /**
     * First load - get's the most recent posts.
     * @param params - set parameters to send to API
     * @returns {boolean}
     */
    Feed.prototype.load = function load (params) {
        _logger__WEBPACK_IMPORTED_MODULE_0__["default"].log ('Feed->load '+this.loading);

        if (this.loading) {
            return false;
        }
        this.currentPage = 0;

        var loadPostParams = _lib__WEBPACK_IMPORTED_MODULE_4__["default"].extend(this.params, params);

        this._loadPosts (loadPostParams);
    };

    /**
     * Loads posts after the current set
     * @returns {boolean}
     */
    Feed.prototype.loadAfter = function loadAfter () {
        _logger__WEBPACK_IMPORTED_MODULE_0__["default"].log ('Feed->loadAfter '+this.loading);

        if (this.loading) {
            return false;
        }
        this.currentPage = 0;

        var params = _lib__WEBPACK_IMPORTED_MODULE_4__["default"].extend({},this.params);

        // TODO should we check we have after?
        if (this.pagination && this.pagination.after) {
            params.after = this.pagination.after;
            delete params.before;
        }

        this._loadPosts (params);
    };

    Feed.prototype._loadPosts = function _loadPosts (params) {
        var this$1 = this;

        _logger__WEBPACK_IMPORTED_MODULE_0__["default"].log ('Feed->_loadPosts');

        this.loading = true;

        params.rnd = (new Date ()).getTime();

        _ajax__WEBPACK_IMPORTED_MODULE_3__["default"].get(
            this.getUrl('/posts'),
            params,
            function (data) {
                _logger__WEBPACK_IMPORTED_MODULE_0__["default"].log('Feed->_loadPosts success');

                if (data.success) {
                    this$1.postCount = data.postCount;
                    this$1.postsLoaded += data.posts.length;

                    this$1.allPostsLoaded = this$1.postsLoaded >= this$1.postCount;

                    this$1.posts = this$1.posts.concat(data.posts);
                    this$1.networks = data.networks;

                    if (data.pagination) {
                        this$1.pagination = data.pagination;
                    }

                    this$1.widget.trigger(_events__WEBPACK_IMPORTED_MODULE_1__["default"].FEED_LOADED, data);
                    this$1.trigger(_events__WEBPACK_IMPORTED_MODULE_1__["default"].FEED_LOADED, data);

                    this$1.widget.trigger(_events__WEBPACK_IMPORTED_MODULE_1__["default"].POSTS_LOADED, data.posts);
                    this$1.trigger(_events__WEBPACK_IMPORTED_MODULE_1__["default"].POSTS_LOADED, data.posts);
                } else {
                    this$1.trigger(_events__WEBPACK_IMPORTED_MODULE_1__["default"].POSTS_FAILED, data);
                    this$1.widget.trigger(_events__WEBPACK_IMPORTED_MODULE_1__["default"].POSTS_FAILED, data);
                }
                this$1.loading = false;
            },
            function (jqXHR, textStatus, errorThrown) {
                _logger__WEBPACK_IMPORTED_MODULE_0__["default"].log('Feed->_loadPosts fail');
                _logger__WEBPACK_IMPORTED_MODULE_0__["default"].log(textStatus);
                _logger__WEBPACK_IMPORTED_MODULE_0__["default"].log(errorThrown);

                this$1.trigger(_events__WEBPACK_IMPORTED_MODULE_1__["default"].POSTS_FAILED, []);
                this$1.loading = false;
            }
        );
    };

    /**
     * Loads new posts
     * @returns {boolean}
     */
    Feed.prototype.loadNewPosts = function loadNewPosts () {
        var this$1 = this;

        _logger__WEBPACK_IMPORTED_MODULE_0__["default"].log ('Feed->loadNewPosts '+this.loading);

        if (this.loading) {
            return false;
        }
        this.currentPage = 0;

        var params = _lib__WEBPACK_IMPORTED_MODULE_4__["default"].extend({},this.params);

        // TODO should we check we have after?
        if (this.pagination && this.pagination.before) {
            params.before = this.pagination.before;
            delete params.after;
        }

        // console.log(params.before);

        return new Promise (function (resolve, reject) {
            this$1.loading = true;

            params.rnd = (new Date ()).getTime();

            _ajax__WEBPACK_IMPORTED_MODULE_3__["default"].get(
                this$1.getUrl('/posts'),
                params,
                function (data) {
                    _logger__WEBPACK_IMPORTED_MODULE_0__["default"].log('Feed->_loadPosts success');
                    this$1.loading = false;


                    if (data.success) {
                        // this.postCount = data.postCount;
                        // this.postsLoaded += data.posts.length;
                        //
                        // this.allPostsLoaded = this.postsLoaded >= this.postCount;
                        //
                        // this.posts = this.posts.concat(data.posts);
                        // this.networks = data.networks;
                        //
                        if (data.pagination && data.pagination.before) {
                            this$1.pagination.before = data.pagination.before;
                        }
                        //
                        // this.widget.trigger(Events.FEED_LOADED, data);
                        // this.trigger(Events.FEED_LOADED, data);
                        //
                        // this.widget.trigger(Events.POSTS_LOADED, data.posts);
                        // this.trigger(Events.POSTS_LOADED, data.posts);

                        // add to the beginning
                        if (data.posts.length > 0) {
                            this$1.posts = data.posts.concat(this$1.posts);
                        }

                        resolve (data.posts);
                    } else {
                        // this.trigger(Events.POSTS_FAILED, data);
                        // this.widget.trigger(Events.POSTS_FAILED, data);
                        reject();
                    }
                },
                function (jqXHR, textStatus, errorThrown) {
                    _logger__WEBPACK_IMPORTED_MODULE_0__["default"].log('Feed->_loadNewPosts fail');
                    _logger__WEBPACK_IMPORTED_MODULE_0__["default"].log(textStatus);
                    _logger__WEBPACK_IMPORTED_MODULE_0__["default"].log(errorThrown);

                    this$1.trigger(_events__WEBPACK_IMPORTED_MODULE_1__["default"].POSTS_FAILED, []);
                    this$1.loading = false;
                }
            );
        });
    };



    Feed.prototype.loadPost = function loadPost (id, successCallback, failCallback) {
        failCallback = failCallback || function(){};
        _ajax__WEBPACK_IMPORTED_MODULE_3__["default"].get(
            this.getUrl('/post/' + id),
            {},
            function (data) {
                if (data.success) {
                    successCallback (data.post);
                } else {
                    failCallback ();
                }
            },
            function (jqXHR, textStatus, errorThrown) { /* jshint ignore:line */
                // FAIL
            });
    };

    Feed.prototype.inappropriatePost = function inappropriatePost (id, reason, success, failure) {
        var params = {
            reason: reason
        };

        _ajax__WEBPACK_IMPORTED_MODULE_3__["default"].post(
            this.getUrl('/post/' + id + '/inappropriate'),
            params,
            function (data, textStatus, jqXHR) {
                data = _lib__WEBPACK_IMPORTED_MODULE_4__["default"].parseJSON(data);

                if (data.success === true) {
                    success();
                }
                else {
                    failure(jqXHR);
                }
        }   );
    };

    Feed.prototype.lovePost = function lovePost (id, success, failure) {
        var params = {};

        _lib__WEBPACK_IMPORTED_MODULE_4__["default"].post(this.getUrl('/post/' + id + '/love'), params, function (data, textStatus, jqXHR) {
            data = _lib__WEBPACK_IMPORTED_MODULE_4__["default"].parseJSON(data);

            if (data.success === true) {
                success(data.loves);
            }
            else {
                failure(jqXHR);
            }
        });
    };

    Feed.prototype.getUrl = function getUrl (trail) {
        return this.feedBase+'/'+this.options.feedId+trail;
    };

    Feed.prototype.destroy = function destroy () {
        EventBus.prototype.destroy.call(this);
    };

    return Feed;
}(_bus__WEBPACK_IMPORTED_MODULE_2__["default"]));

/* harmony default export */ __webpack_exports__["default"] = (Feed);

/***/ }),

/***/ "./js/app/widget/curator/core/globals.js":
/*!***********************************************!*\
  !*** ./js/app/widget/curator/core/globals.js ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
var Globals = {
    POST_CLICK_ACTION_OPEN_POPUP:   'open-popup',
    POST_CLICK_ACTION_GOTO_SOURCE:  'goto-source',
    POST_CLICK_ACTION_NOTHING:      'nothing',
};

/* harmony default export */ __webpack_exports__["default"] = (Globals);

/***/ }),

/***/ "./js/app/widget/curator/core/lib.js":
/*!*******************************************!*\
  !*** ./js/app/widget/curator/core/lib.js ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(__webpack_provided_window_dot_jQuery) {
// Change to use $local is passed into the factory wrapper - it's either jQuery or Zepto
var z = null;

if (window.$crt) {
    z = window.$crt;
} else if (window.Zepto) {
    z = window.Zepto;
} else if (__webpack_provided_window_dot_jQuery) {
    z = __webpack_provided_window_dot_jQuery;
}


if (!z) {
    window.alert('Curator requires jQuery or Zepto. \n\nPlease include jQuery in your HTML before the Curator widget script tag.\n\nVisit http://jquery.com/download/ to get the latest version');
}

/* harmony default export */ __webpack_exports__["default"] = (z);
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./js/app/widget/curator/core/logger.js":
/*!**********************************************!*\
  !*** ./js/app/widget/curator/core/logger.js ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* globals window */

var Logger = {
    debug: false,

    log: function (s) {

        if (window.console && Logger.debug) {
            //window.console.log(s);
        }
    },

    error: function (s) {
        if (window.console) {
            window.console.error(s);
        }
    }
};

/* harmony default export */ __webpack_exports__["default"] = (Logger);

/***/ }),

/***/ "./js/app/widget/curator/core/templating.js":
/*!**************************************************!*\
  !*** ./js/app/widget/curator/core/templating.js ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _templates_templates__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../templates/templates */ "./js/app/widget/curator/templates/templates.js");
/* harmony import */ var _templating_helpers__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./templating_helpers */ "./js/app/widget/curator/core/templating_helpers.js");
/* harmony import */ var _logger__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./logger */ "./js/app/widget/curator/core/logger.js");
/* harmony import */ var _lib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./lib */ "./js/app/widget/curator/core/lib.js");

// Simple JavaScript Templating
// John Resig - http://ejohn.org/ - MIT Licensed






var _rendererTmplCache = {};

var Templating = {
    renderTemplate: function renderTemplate (templateId, data, options) {

        if (options) {
            data.options = options;
        }

        var source = '';
        var $t = Object(_lib__WEBPACK_IMPORTED_MODULE_3__["default"])('#'+templateId);

        if ($t.length===1)
        {
            source = $t.html();
        } else if (_templates_templates__WEBPACK_IMPORTED_MODULE_0__["default"][templateId] !== undefined)
        {
            source = _templates_templates__WEBPACK_IMPORTED_MODULE_0__["default"][templateId];
        }

        if (source === '')
        {
            throw new Error ('Could not find template '+templateId);
        }

        return Templating.renderDiv(source, data);
    },

    renderDiv: function renderDiv (source, data) {
        var tmpl = Templating.render(source, data);
        if (_lib__WEBPACK_IMPORTED_MODULE_3__["default"].parseHTML) {
            // breaks with jquery < 1.8
            tmpl = _lib__WEBPACK_IMPORTED_MODULE_3__["default"].parseHTML(tmpl);
        }
        return Object(_lib__WEBPACK_IMPORTED_MODULE_3__["default"])(tmpl).filter('div');
    },

    render: function render (str, data) {
        var err = "";
        try {
            var func = _rendererTmplCache[str];
            if (!func) {
                var strComp =
                    str.replace(/[\r\t\n]/g, " ")
                        .replace(/'(?=[^%]*%>)/g, "\t")
                        .split("'").join("\\'")
                        .split("\t").join("'")
                        .replace(/<%=(.+?)%>/g, "',$1,'")
                        .split("<%").join("');")
                        .split("%>").join("p.push('");

                // note - don't change the 'var' in the string to 'let'!!!
                var strFunc =
                    "var p=[],print=function(){p.push.apply(p,arguments);};" +
                    "with(obj){p.push('" + strComp + "');}return p.join('');";

                func = new Function("obj", strFunc);  // jshint ignore:line
                _rendererTmplCache[str] = func;
            }
            _templating_helpers__WEBPACK_IMPORTED_MODULE_1__["default"].data = data;
            return func.call(_templating_helpers__WEBPACK_IMPORTED_MODULE_1__["default"], data);
        } catch (e) {
            _logger__WEBPACK_IMPORTED_MODULE_2__["default"].log ('Template parse error: ' +e.message);
            err = e.message;
        }
        return " # ERROR: " + err + " # ";
    }
};

/* harmony default export */ __webpack_exports__["default"] = (Templating);



/***/ }),

/***/ "./js/app/widget/curator/core/templating_helpers.js":
/*!**********************************************************!*\
  !*** ./js/app/widget/curator/core/templating_helpers.js ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _utils_date__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../utils/date */ "./js/app/widget/curator/utils/date.js");
/* harmony import */ var _utils_string__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../utils/string */ "./js/app/widget/curator/utils/string.js");
/* harmony import */ var _translate__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./translate */ "./js/app/widget/curator/core/translate.js");





var options = {};

var helpers = {
    networkIcon: function networkIcon () {
        return this.data.network_name.toLowerCase();
    },

    networkName: function networkName () {
        return this.data.network_name.toLowerCase();
    },

    userUrl: function userUrl () {
        if (this.data.user_url && this.data.user_url !== '') {
            return this.data.user_url;
        }
        if (this.data.originator_user_url && this.data.originator_user_url !== '') {
            return this.data.originator_user_url;
        }
        if (this.data.userUrl && this.data.userUrl !== '') {
            return this.data.userUrl;
        }

        var netId = this.data.network_id+'';
        if (netId === '1') {
            return 'http://twitter.com/' + this.data.user_screen_name;
        } else if (netId === '2') {
            return 'http://instagram.com/'+this.data.user_screen_name;
        } else if (netId === '3') {
            return 'http://facebook.com/'+this.data.user_screen_name;
        }

        return '#';
    },

    parseText: function parseText (s) {
        if (this.data.is_html) {
            return s;
        } else {
            if (this.data.network_name === 'Twitter') {
                s = _utils_string__WEBPACK_IMPORTED_MODULE_1__["default"].linksToHref(s);
                s = _utils_string__WEBPACK_IMPORTED_MODULE_1__["default"].twitterLinks(s);
            } else if (this.data.network_name === 'Instagram') {
                s = _utils_string__WEBPACK_IMPORTED_MODULE_1__["default"].linksToHref(s);
                s = _utils_string__WEBPACK_IMPORTED_MODULE_1__["default"].instagramLinks(s);
            } else if (this.data.network_name === 'Facebook') {
                s = _utils_string__WEBPACK_IMPORTED_MODULE_1__["default"].linksToHref(s);
                s = _utils_string__WEBPACK_IMPORTED_MODULE_1__["default"].facebookLinks(s);
            } else {
                s = _utils_string__WEBPACK_IMPORTED_MODULE_1__["default"].linksToHref(s);
            }

            return _utils_string__WEBPACK_IMPORTED_MODULE_1__["default"].nl2br(s);
        }
    },

    nl2br: function nl2br (s) {
        return _utils_string__WEBPACK_IMPORTED_MODULE_1__["default"].nl2br(s);
    },

    contentImageClasses: function contentImageClasses () {
        return this.data.image ? 'crt-post-has-image' : 'crt-post-content-image-hidden crt-post-no-image';
    },

    contentTextClasses: function contentTextClasses () {
        return this.data.text ? 'crt-post-has-text' : 'crt-post-content-text-hidden crt-post-no-text';
    },

    fuzzyDate: function fuzzyDate (dateString)
    {
        return _utils_date__WEBPACK_IMPORTED_MODULE_0__["default"].fuzzyDate(dateString);
    },

    prettyDate: function prettyDate (time) {
        return _utils_date__WEBPACK_IMPORTED_MODULE_0__["default"].prettyDate (time);
    },

    _t: function _t (s, n) {
        return _translate__WEBPACK_IMPORTED_MODULE_2__["default"].t (s, n);
    }
};

/* harmony default export */ __webpack_exports__["default"] = (helpers);

/***/ }),

/***/ "./js/app/widget/curator/core/translate.js":
/*!*************************************************!*\
  !*** ./js/app/widget/curator/core/translate.js ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _libraries_translate__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../libraries/translate */ "./js/app/widget/curator/libraries/translate.js");
/* harmony import */ var _translations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./translations */ "./js/app/widget/curator/core/translations.js");



var _cache = {};
var currentLang = 'en';

var mod = {
    setLang: function setLang (lang) {
        currentLang = lang;
    },

    t: function t (key, n, lang) {
        lang = lang || currentLang;

        if (!_cache[lang]) {
            if (_translations__WEBPACK_IMPORTED_MODULE_1__["default"][lang]) {
                _cache[lang] = _libraries_translate__WEBPACK_IMPORTED_MODULE_0__["default"].getTranslationFunction(_translations__WEBPACK_IMPORTED_MODULE_1__["default"][lang]);
            } else {
                window.console.error('Unsupported language `' + lang + '`');
                _cache[lang] = _libraries_translate__WEBPACK_IMPORTED_MODULE_0__["default"].getTranslationFunction(_translations__WEBPACK_IMPORTED_MODULE_1__["default"].en);
            }
        }

        key = key.toLowerCase();
        key = key.replace(' ','-');

        return _cache[lang](key, n);
    }
};

/* harmony default export */ __webpack_exports__["default"] = (mod);

/***/ }),

/***/ "./js/app/widget/curator/core/translations.js":
/*!****************************************************!*\
  !*** ./js/app/widget/curator/core/translations.js ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);


function _k (o, key, val) {
    // console.log(key);
    var kPath = key.split('.');
    for (var i=0;i<kPath.length;i++) {
        var k = kPath[i];
        if (!o[k]) {
            o[k] = {};
        }
        if (i === kPath.length-1) {
            o[k] = val;
        } else {
            o = o[k];
        }
    }
}

var langsData = "\nid,en,de,it,nl,es,fr,po,ru,sl\nload-more,Load more,Mehr anzeigen,Di più,Laad meer,Cargar más,Voir plus,Carregar Mais,Загрузить больше,Prikaži več\nminutes-ago.1,{n} minute ago,Vor einer Minute,Un minuto fa,{n} minuut geleden,Hace un minuto,Il y a {n} minute,Tem um minuto,Одну минуту назад,pred {n} minuto\nminutes-ago.n,{n} minutes ago,Vor {n} Minuten,{n} minuti fa,{n} minuten geleden,Hace {n} minutos,Il y a {n} minutes,Tem {n} minutos,{n} минут назад,pred {n} minutami\nhours-ago.1,{n} hour ago,Vor einer Stunde,Un'ora fa,{n} uur geleden,Hace una hora,Il y a {n} heure,Tem {n} hora,Один час назад,pred {n} uro\nhours-ago.n,{n} hours ago,Vor {n} Stunden,{n} ore fa,{n} uren geleden,Hace {n} horas,Il y a {n} heures,Tem {n} horas,{n} часов назад,pred {n} urami\ndays-ago.1,{n} day ago,Vor einem Tag,Un giorno fa,{n} dag geleden,Hace un día,Il y a {n} jour,Faz um dia,Один день назад,pred {n} dnevom\ndays-ago.n,{n} days ago,Vor {n} Tagen,{n} giorni fa,{n} dagen geleden,Hace {n} días,Il y a {n} jours,Fazem {n} dias,{n} дней назад,pred {n} dnevi\nweeks-ago.1,{n} week ago,Vor einer Woche,Una settimana fa,{n} week geleden,Hace una semana,Il y a {n} semaine,Faz uma semana,Одну неделю назад,pred {n} tednom\nweeks-ago.n,{n} weeks ago,Vor {n} Wochen,{n} settimane fa,{n} weken geleden,Hace {n} semanas,Il y a {n} semaines,Fazem {n} semanas,{n} недель назад,pred {n} tedni\nmonths-ago.1,{n} month ago,Vor einem Monat,Un mese fa,{n} maand geleden,Hace un mes,Il y a {n} mois,Tem um mês,Один месяц назад,pred {n} mesecem\nmonths-ago.n,{n} months ago,Vor {n} Monaten,{n} mesi,{n} maanden geleden,Hace {n} meses,Il y a {n} mois,Tem {n} meses,{n} месяцев назад,pred {n} meseci\nyesterday,Yesterday,Gestern,Leri,Gisteren,Ayer,Hier,Ontem,Вчера,Včeraj\njust-now,Just now,Eben,Appena,Nu,Ahora,Il y a un instant,Agora,Только что,Pravkar\nprevious,Previous,Zurück,Indietro,Vorige,Anterior,Précédent,Anterior,Предыдущий,Prejšnji\nnext,Next,Weiter,Più,Volgende,Siguiente,Suivant,Próximo,Следующий,Naslednji\ncomments,Comments,Kommentare,Commenti,Comments,Comentarios,Commentaires,Comentários,Комментарии,Komentarji\nlikes,Likes,Gefällt mir,Mi piace,Likes,Me gusta,J'aime,Curtir,Лайки,Všečki\nread-more,Read more,Weiterlesen,Di più,Lees meer,Leer más,En savoir plus,Leia mais,Подробнее,Preberi več\nfilter,Filter,Filtern,filtrare,Filtreren,filtrar,filtrer,Filtro,фильтровать,Filter\nall,All,Alle,Tutti,Alle,Todas,Tout,Todos,все,Vsi\n";


var langs = {};
var langDataLines = langsData.split('\n');

// Remove unused lines
for (var i = langDataLines.length-1 ; i>=0 ; i--) {
    if (!langDataLines[i]) {
        langDataLines.splice(i,1);
    }
}
var keys = langDataLines[0].split(',');

for (var i$1=1;i$1<langDataLines.length;i$1++) {
    var langDataCols = langDataLines[i$1].split(',');
    for (var j = 1;j < langDataCols.length;j++) {
        _k (langs, keys[j]+'.'+langDataCols[0], langDataCols[j]);
    }
}

// console.log(langs.sl);

/* harmony default export */ __webpack_exports__["default"] = (langs);


/***/ }),

/***/ "./js/app/widget/curator/libraries/nanoajax.js":
/*!*****************************************************!*\
  !*** ./js/app/widget/curator/libraries/nanoajax.js ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(global) {/**
 * Props to https://github.com/yanatan16/nanoajax
 */

// Best place to find information on XHR features is:
// https://developer.mozilla.org/en-US/docs/Web/API/XMLHttpRequest

var reqfields = [
    'responseType', 'withCredentials', 'timeout', 'onprogress'
];

function nanoajax (params, callback) {
    // Any variable used more than once is var'd here because
    // minification will munge the variables whereas it can't munge
    // the object access.
    var headers = params.headers || {},
        body = params.body,
        method = params.method || (body ? 'POST' : 'GET'),
        called = false;

    var req = getRequest(params.cors);

    function cb(statusCode, responseText) {
        return function () {
            if (!called) {
                callback(req.status === undefined ? statusCode : req.status,
                    req.status === 0 ? "Error" : (req.response || req.responseText || responseText),
                    req);
                called = true;
            }
        };
    }

    req.open(method, params.url, true);

    var success = req.onload = cb(200);
    req.onreadystatechange = function () {
        if (req.readyState === 4) {success();}
    };
    req.onerror = cb(null, 'Error');
    req.ontimeout = cb(null, 'Timeout');
    req.onabort = cb(null, 'Abort');

    if (body) {
        setDefault(headers, 'X-Requested-With', 'XMLHttpRequest');

        if (!global.FormData || !(body instanceof global.FormData)) {
            setDefault(headers, 'Content-Type', 'application/x-www-form-urlencoded');
        }
    }

    for (var i = 0, len = reqfields.length, field = (void 0); i < len; i++) {
        field = reqfields[i];
        if (params[field] !== undefined)
            { req[field] = params[field]; }
    }

    for (var field$1 in headers) {
        req.setRequestHeader(field$1, headers[field$1]);
    }

    req.send(body);

    return req;
}

function getRequest(cors) {
    // XDomainRequest is only way to do CORS in IE 8 and 9
    // But XDomainRequest isn't standards-compatible
    // Notably, it doesn't allow cookies to be sent or set by servers
    // IE 10+ is standards-compatible in its XMLHttpRequest
    // but IE 10 can still have an XDomainRequest object, so we don't want to use it
    if (cors && window.XDomainRequest && !/MSIE 1/.test(window.navigator.userAgent)) {
        return new window.XDomainRequest ();
    }
    if (window.XMLHttpRequest) {
        return new window.XMLHttpRequest ();
    }
}

function setDefault(obj, key, value) {
    obj[key] = obj[key] || value;
}

/* harmony default export */ __webpack_exports__["default"] = (nanoajax);
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../../node_modules/webpack/buildin/global.js */ "./node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./js/app/widget/curator/libraries/translate.js":
/*!******************************************************!*\
  !*** ./js/app/widget/curator/libraries/translate.js ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/**
 * Microlib for translations with support for placeholders and multiple plural forms.
 *
 * https://github.com/musterknabe/translate.js
 *
 * v1.1.0
 *
 * @author Jonas Girnatis <dermusterknabe@gmail.com>
 * @licence May be freely distributed under the MIT license.
 */


var isNumeric = function(obj) { return !isNaN(parseFloat(obj)) && isFinite(obj); };
var isObject = function(obj) { return typeof obj === 'object' && obj !== null; };
var isString = function(obj) { return Object.prototype.toString.call(obj) === '[object String]'; };

var libTranslate = {
    getTranslationFunction: function(messageObject, options) {
        options = isObject(options) ? options : {};

        var debug = options.debug;
        var namespaceSplitter = options.namespaceSplitter || '::';

        function getTranslationValue(translationKey) {
            if(messageObject[translationKey]) {
                return messageObject[translationKey];
            }

            var components = translationKey.split(namespaceSplitter); //@todo make this more robust. maybe support more levels?
            var namespace = components[0];
            var key = components[1];

            if(messageObject[namespace] && messageObject[namespace][key]) {
                return messageObject[namespace][key];
            }

            return null;
        }

        function getPluralValue(translation, count) {
            if (isObject(translation)) {
                var keys = Object.keys(translation);
                var upperCap;

                if(keys.length === 0) {
                    debug && window.console.log('[Translation] No plural forms found.');
                    return null;
                }

                for(var i = 0; i < keys.length; i++) {
                    if(keys[i].indexOf('gt') === 0) {
                        upperCap = parseInt(keys[i].replace('gt', ''), 10);
                    }
                }

                if(translation[count]){
                    translation = translation[count];
                } else if(count > upperCap) { //int > undefined returns false
                    translation = translation['gt' + upperCap];
                } else if(translation.n) {
                    translation = translation.n;
                } else {
                    debug && window.console.log('[Translation] No plural forms found for count:"' + count + '" in', translation);
                    translation = translation[Object.keys(translation).reverse()[0]];
                }
            }

            return translation;
        }

        function replacePlaceholders(translation, replacements) {
            if (isString(translation)) {
                return translation.replace(/\{(\w*)\}/g, function (match, key) {
                    if(!replacements.hasOwnProperty(key)) {
                        debug && window.console.log('Could not find replacement "' + key + '" in provided replacements object:', replacements);

                        return '{' + key + '}';
                    }

                    return replacements.hasOwnProperty(key) ? replacements[key] : key;
                });
            }

            return translation;
        }

        return function (translationKey) {
            var replacements = isObject(arguments[1]) ? arguments[1] : (isObject(arguments[2]) ? arguments[2] : {});
            var count = isNumeric(arguments[1]) ? arguments[1] : (isNumeric(arguments[2]) ? arguments[2] : null);

            var translation = getTranslationValue(translationKey);

            if (count !== null) {
                replacements.n = replacements.n ? replacements.n : count;

                //get appropriate plural translation string
                translation = getPluralValue(translation, count);
            }

            //replace {placeholders}
            translation = replacePlaceholders(translation, replacements);

            if (translation === null) {
                translation = debug ? '@@' + translationKey + '@@' : translationKey;

                if (debug) {
                    window.console.log('Translation for "' + translationKey + '" not found.');
                }
            }

            return translation;
        };
    }
};

/* harmony default export */ __webpack_exports__["default"] = (libTranslate);

/***/ }),

/***/ "./js/app/widget/curator/libraries/twitter-text-regex.js":
/*!***************************************************************!*\
  !*** ./js/app/widget/curator/libraries/twitter-text-regex.js ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
// From https://cdn.rawgit.com/twitter/twitter-text/v1.13.4/js/twitter-text.js
// Cut down to only include RegEx functions

var twttr = {};
twttr.txt = {};
twttr.txt.regexen = {};

var HTML_ENTITIES = {
    '&': '&amp;',
    '>': '&gt;',
    '<': '&lt;',
    '"': '&quot;',
    "'": '&#39;'
};

// HTML escaping
twttr.txt.htmlEscape = function(text) {
    return text && text.replace(/[&"'><]/g, function(character) {
        return HTML_ENTITIES[character];
    });
};

// Builds a RegExp
function regexSupplant(regex, flags) {
    flags = flags || "";
    if (typeof regex !== "string") {
        if (regex.global && flags.indexOf("g") < 0) {
            flags += "g";
        }
        if (regex.ignoreCase && flags.indexOf("i") < 0) {
            flags += "i";
        }
        if (regex.multiline && flags.indexOf("m") < 0) {
            flags += "m";
        }

        regex = regex.source;
    }

    return new RegExp(regex.replace(/#\{(\w+)\}/g, function(match, name) {
        var newRegex = twttr.txt.regexen[name] || "";
        if (typeof newRegex !== "string") {
            newRegex = newRegex.source;
        }
        return newRegex;
    }), flags);
}

twttr.txt.regexSupplant = regexSupplant;

// simple string interpolation
function stringSupplant(str, values) {
    return str.replace(/#\{(\w+)\}/g, function(match, name) {
        return values[name] || "";
    });
}

twttr.txt.stringSupplant = stringSupplant;

function addCharsToCharClass(charClass, start, end) {
    var s = String.fromCharCode(start);
    if (end !== start) {
        s += "-" + String.fromCharCode(end);
    }
    charClass.push(s);
    return charClass;
}

twttr.txt.addCharsToCharClass = addCharsToCharClass;

// Some minimizers convert string escapes into their literal values, which leads to intermittent Unicode normalization bugs and
// increases the gzipped download size. Use RegEx literals as opposed to string literals to prevent that.
var unicodeLettersAndMarks = /A-Za-z\xAA\xB5\xBA\xC0-\xD6\xD8-\xF6\xF8-\u02C1\u02C6-\u02D1\u02E0-\u02E4\u02EC\u02EE\u0370-\u0374\u0376\u0377\u037A-\u037D\u037F\u0386\u0388-\u038A\u038C\u038E-\u03A1\u03A3-\u03F5\u03F7-\u0481\u048A-\u052F\u0531-\u0556\u0559\u0561-\u0587\u05D0-\u05EA\u05F0-\u05F2\u0620-\u064A\u066E\u066F\u0671-\u06D3\u06D5\u06E5\u06E6\u06EE\u06EF\u06FA-\u06FC\u06FF\u0710\u0712-\u072F\u074D-\u07A5\u07B1\u07CA-\u07EA\u07F4\u07F5\u07FA\u0800-\u0815\u081A\u0824\u0828\u0840-\u0858\u08A0-\u08B2\u0904-\u0939\u093D\u0950\u0958-\u0961\u0971-\u0980\u0985-\u098C\u098F\u0990\u0993-\u09A8\u09AA-\u09B0\u09B2\u09B6-\u09B9\u09BD\u09CE\u09DC\u09DD\u09DF-\u09E1\u09F0\u09F1\u0A05-\u0A0A\u0A0F\u0A10\u0A13-\u0A28\u0A2A-\u0A30\u0A32\u0A33\u0A35\u0A36\u0A38\u0A39\u0A59-\u0A5C\u0A5E\u0A72-\u0A74\u0A85-\u0A8D\u0A8F-\u0A91\u0A93-\u0AA8\u0AAA-\u0AB0\u0AB2\u0AB3\u0AB5-\u0AB9\u0ABD\u0AD0\u0AE0\u0AE1\u0B05-\u0B0C\u0B0F\u0B10\u0B13-\u0B28\u0B2A-\u0B30\u0B32\u0B33\u0B35-\u0B39\u0B3D\u0B5C\u0B5D\u0B5F-\u0B61\u0B71\u0B83\u0B85-\u0B8A\u0B8E-\u0B90\u0B92-\u0B95\u0B99\u0B9A\u0B9C\u0B9E\u0B9F\u0BA3\u0BA4\u0BA8-\u0BAA\u0BAE-\u0BB9\u0BD0\u0C05-\u0C0C\u0C0E-\u0C10\u0C12-\u0C28\u0C2A-\u0C39\u0C3D\u0C58\u0C59\u0C60\u0C61\u0C85-\u0C8C\u0C8E-\u0C90\u0C92-\u0CA8\u0CAA-\u0CB3\u0CB5-\u0CB9\u0CBD\u0CDE\u0CE0\u0CE1\u0CF1\u0CF2\u0D05-\u0D0C\u0D0E-\u0D10\u0D12-\u0D3A\u0D3D\u0D4E\u0D60\u0D61\u0D7A-\u0D7F\u0D85-\u0D96\u0D9A-\u0DB1\u0DB3-\u0DBB\u0DBD\u0DC0-\u0DC6\u0E01-\u0E30\u0E32\u0E33\u0E40-\u0E46\u0E81\u0E82\u0E84\u0E87\u0E88\u0E8A\u0E8D\u0E94-\u0E97\u0E99-\u0E9F\u0EA1-\u0EA3\u0EA5\u0EA7\u0EAA\u0EAB\u0EAD-\u0EB0\u0EB2\u0EB3\u0EBD\u0EC0-\u0EC4\u0EC6\u0EDC-\u0EDF\u0F00\u0F40-\u0F47\u0F49-\u0F6C\u0F88-\u0F8C\u1000-\u102A\u103F\u1050-\u1055\u105A-\u105D\u1061\u1065\u1066\u106E-\u1070\u1075-\u1081\u108E\u10A0-\u10C5\u10C7\u10CD\u10D0-\u10FA\u10FC-\u1248\u124A-\u124D\u1250-\u1256\u1258\u125A-\u125D\u1260-\u1288\u128A-\u128D\u1290-\u12B0\u12B2-\u12B5\u12B8-\u12BE\u12C0\u12C2-\u12C5\u12C8-\u12D6\u12D8-\u1310\u1312-\u1315\u1318-\u135A\u1380-\u138F\u13A0-\u13F4\u1401-\u166C\u166F-\u167F\u1681-\u169A\u16A0-\u16EA\u16F1-\u16F8\u1700-\u170C\u170E-\u1711\u1720-\u1731\u1740-\u1751\u1760-\u176C\u176E-\u1770\u1780-\u17B3\u17D7\u17DC\u1820-\u1877\u1880-\u18A8\u18AA\u18B0-\u18F5\u1900-\u191E\u1950-\u196D\u1970-\u1974\u1980-\u19AB\u19C1-\u19C7\u1A00-\u1A16\u1A20-\u1A54\u1AA7\u1B05-\u1B33\u1B45-\u1B4B\u1B83-\u1BA0\u1BAE\u1BAF\u1BBA-\u1BE5\u1C00-\u1C23\u1C4D-\u1C4F\u1C5A-\u1C7D\u1CE9-\u1CEC\u1CEE-\u1CF1\u1CF5\u1CF6\u1D00-\u1DBF\u1E00-\u1F15\u1F18-\u1F1D\u1F20-\u1F45\u1F48-\u1F4D\u1F50-\u1F57\u1F59\u1F5B\u1F5D\u1F5F-\u1F7D\u1F80-\u1FB4\u1FB6-\u1FBC\u1FBE\u1FC2-\u1FC4\u1FC6-\u1FCC\u1FD0-\u1FD3\u1FD6-\u1FDB\u1FE0-\u1FEC\u1FF2-\u1FF4\u1FF6-\u1FFC\u2071\u207F\u2090-\u209C\u2102\u2107\u210A-\u2113\u2115\u2119-\u211D\u2124\u2126\u2128\u212A-\u212D\u212F-\u2139\u213C-\u213F\u2145-\u2149\u214E\u2183\u2184\u2C00-\u2C2E\u2C30-\u2C5E\u2C60-\u2CE4\u2CEB-\u2CEE\u2CF2\u2CF3\u2D00-\u2D25\u2D27\u2D2D\u2D30-\u2D67\u2D6F\u2D80-\u2D96\u2DA0-\u2DA6\u2DA8-\u2DAE\u2DB0-\u2DB6\u2DB8-\u2DBE\u2DC0-\u2DC6\u2DC8-\u2DCE\u2DD0-\u2DD6\u2DD8-\u2DDE\u2E2F\u3005\u3006\u3031-\u3035\u303B\u303C\u3041-\u3096\u309D-\u309F\u30A1-\u30FA\u30FC-\u30FF\u3105-\u312D\u3131-\u318E\u31A0-\u31BA\u31F0-\u31FF\u3400-\u4DB5\u4E00-\u9FCC\uA000-\uA48C\uA4D0-\uA4FD\uA500-\uA60C\uA610-\uA61F\uA62A\uA62B\uA640-\uA66E\uA67F-\uA69D\uA6A0-\uA6E5\uA717-\uA71F\uA722-\uA788\uA78B-\uA78E\uA790-\uA7AD\uA7B0\uA7B1\uA7F7-\uA801\uA803-\uA805\uA807-\uA80A\uA80C-\uA822\uA840-\uA873\uA882-\uA8B3\uA8F2-\uA8F7\uA8FB\uA90A-\uA925\uA930-\uA946\uA960-\uA97C\uA984-\uA9B2\uA9CF\uA9E0-\uA9E4\uA9E6-\uA9EF\uA9FA-\uA9FE\uAA00-\uAA28\uAA40-\uAA42\uAA44-\uAA4B\uAA60-\uAA76\uAA7A\uAA7E-\uAAAF\uAAB1\uAAB5\uAAB6\uAAB9-\uAABD\uAAC0\uAAC2\uAADB-\uAADD\uAAE0-\uAAEA\uAAF2-\uAAF4\uAB01-\uAB06\uAB09-\uAB0E\uAB11-\uAB16\uAB20-\uAB26\uAB28-\uAB2E\uAB30-\uAB5A\uAB5C-\uAB5F\uAB64\uAB65\uABC0-\uABE2\uAC00-\uD7A3\uD7B0-\uD7C6\uD7CB-\uD7FB\uF900-\uFA6D\uFA70-\uFAD9\uFB00-\uFB06\uFB13-\uFB17\uFB1D\uFB1F-\uFB28\uFB2A-\uFB36\uFB38-\uFB3C\uFB3E\uFB40\uFB41\uFB43\uFB44\uFB46-\uFBB1\uFBD3-\uFD3D\uFD50-\uFD8F\uFD92-\uFDC7\uFDF0-\uFDFB\uFE70-\uFE74\uFE76-\uFEFC\uFF21-\uFF3A\uFF41-\uFF5A\uFF66-\uFFBE\uFFC2-\uFFC7\uFFCA-\uFFCF\uFFD2-\uFFD7\uFFDA-\uFFDC\u0300-\u036F\u0483-\u0489\u0591-\u05BD\u05BF\u05C1\u05C2\u05C4\u05C5\u05C7\u0610-\u061A\u064B-\u065F\u0670\u06D6-\u06DC\u06DF-\u06E4\u06E7\u06E8\u06EA-\u06ED\u0711\u0730-\u074A\u07A6-\u07B0\u07EB-\u07F3\u0816-\u0819\u081B-\u0823\u0825-\u0827\u0829-\u082D\u0859-\u085B\u08E4-\u0903\u093A-\u093C\u093E-\u094F\u0951-\u0957\u0962\u0963\u0981-\u0983\u09BC\u09BE-\u09C4\u09C7\u09C8\u09CB-\u09CD\u09D7\u09E2\u09E3\u0A01-\u0A03\u0A3C\u0A3E-\u0A42\u0A47\u0A48\u0A4B-\u0A4D\u0A51\u0A70\u0A71\u0A75\u0A81-\u0A83\u0ABC\u0ABE-\u0AC5\u0AC7-\u0AC9\u0ACB-\u0ACD\u0AE2\u0AE3\u0B01-\u0B03\u0B3C\u0B3E-\u0B44\u0B47\u0B48\u0B4B-\u0B4D\u0B56\u0B57\u0B62\u0B63\u0B82\u0BBE-\u0BC2\u0BC6-\u0BC8\u0BCA-\u0BCD\u0BD7\u0C00-\u0C03\u0C3E-\u0C44\u0C46-\u0C48\u0C4A-\u0C4D\u0C55\u0C56\u0C62\u0C63\u0C81-\u0C83\u0CBC\u0CBE-\u0CC4\u0CC6-\u0CC8\u0CCA-\u0CCD\u0CD5\u0CD6\u0CE2\u0CE3\u0D01-\u0D03\u0D3E-\u0D44\u0D46-\u0D48\u0D4A-\u0D4D\u0D57\u0D62\u0D63\u0D82\u0D83\u0DCA\u0DCF-\u0DD4\u0DD6\u0DD8-\u0DDF\u0DF2\u0DF3\u0E31\u0E34-\u0E3A\u0E47-\u0E4E\u0EB1\u0EB4-\u0EB9\u0EBB\u0EBC\u0EC8-\u0ECD\u0F18\u0F19\u0F35\u0F37\u0F39\u0F3E\u0F3F\u0F71-\u0F84\u0F86\u0F87\u0F8D-\u0F97\u0F99-\u0FBC\u0FC6\u102B-\u103E\u1056-\u1059\u105E-\u1060\u1062-\u1064\u1067-\u106D\u1071-\u1074\u1082-\u108D\u108F\u109A-\u109D\u135D-\u135F\u1712-\u1714\u1732-\u1734\u1752\u1753\u1772\u1773\u17B4-\u17D3\u17DD\u180B-\u180D\u18A9\u1920-\u192B\u1930-\u193B\u19B0-\u19C0\u19C8\u19C9\u1A17-\u1A1B\u1A55-\u1A5E\u1A60-\u1A7C\u1A7F\u1AB0-\u1ABE\u1B00-\u1B04\u1B34-\u1B44\u1B6B-\u1B73\u1B80-\u1B82\u1BA1-\u1BAD\u1BE6-\u1BF3\u1C24-\u1C37\u1CD0-\u1CD2\u1CD4-\u1CE8\u1CED\u1CF2-\u1CF4\u1CF8\u1CF9\u1DC0-\u1DF5\u1DFC-\u1DFF\u20D0-\u20F0\u2CEF-\u2CF1\u2D7F\u2DE0-\u2DFF\u302A-\u302F\u3099\u309A\uA66F-\uA672\uA674-\uA67D\uA69F\uA6F0\uA6F1\uA802\uA806\uA80B\uA823-\uA827\uA880\uA881\uA8B4-\uA8C4\uA8E0-\uA8F1\uA926-\uA92D\uA947-\uA953\uA980-\uA983\uA9B3-\uA9C0\uA9E5\uAA29-\uAA36\uAA43\uAA4C\uAA4D\uAA7B-\uAA7D\uAAB0\uAAB2-\uAAB4\uAAB7\uAAB8\uAABE\uAABF\uAAC1\uAAEB-\uAAEF\uAAF5\uAAF6\uABE3-\uABEA\uABEC\uABED\uFB1E\uFE00-\uFE0F\uFE20-\uFE2D/.source;
var unicodeNumbers = /0-9\u0660-\u0669\u06F0-\u06F9\u07C0-\u07C9\u0966-\u096F\u09E6-\u09EF\u0A66-\u0A6F\u0AE6-\u0AEF\u0B66-\u0B6F\u0BE6-\u0BEF\u0C66-\u0C6F\u0CE6-\u0CEF\u0D66-\u0D6F\u0DE6-\u0DEF\u0E50-\u0E59\u0ED0-\u0ED9\u0F20-\u0F29\u1040-\u1049\u1090-\u1099\u17E0-\u17E9\u1810-\u1819\u1946-\u194F\u19D0-\u19D9\u1A80-\u1A89\u1A90-\u1A99\u1B50-\u1B59\u1BB0-\u1BB9\u1C40-\u1C49\u1C50-\u1C59\uA620-\uA629\uA8D0-\uA8D9\uA900-\uA909\uA9D0-\uA9D9\uA9F0-\uA9F9\uAA50-\uAA59\uABF0-\uABF9\uFF10-\uFF19/.source;
var hashtagSpecialChars = /_\u200c\u200d\ua67e\u05be\u05f3\u05f4\uff5e\u301c\u309b\u309c\u30a0\u30fb\u3003\u0f0b\u0f0c\u00b7/.source;
var hashTagSpecialChars2 = /\.-/.source;
// A hashtag must contain at least one unicode letter or mark, as well as numbers, underscores, and select special characters.
twttr.txt.regexen.hashSigns = /[#＃]/;
twttr.txt.regexen.hashtagAlpha = new RegExp("[" + unicodeLettersAndMarks + "]");
twttr.txt.regexen.hashtagAlphaNumeric = new RegExp("[" + unicodeLettersAndMarks + unicodeNumbers + hashtagSpecialChars + hashTagSpecialChars2 + "]");
twttr.txt.regexen.endHashtagMatch = regexSupplant(/^(?:#{hashSigns}|:\/\/)/);
twttr.txt.regexen.hashtagBoundary = new RegExp("(?:^|$|[^&" + unicodeLettersAndMarks + unicodeNumbers + hashtagSpecialChars + "])");
// twttr.txt.regexen.validHashtag = regexSupplant(/(#{hashtagBoundary})(#{hashSigns})(?!\ufe0f|\u20e3)(#{hashtagAlphaNumeric}*#{hashtagAlpha}#{hashtagAlphaNumeric}*)/gi);
twttr.txt.regexen.validHashtag = regexSupplant(/[#]+(#{hashtagAlphaNumeric}*)/gi);

/* harmony default export */ __webpack_exports__["default"] = (twttr);

/***/ }),

/***/ "./js/app/widget/curator/social/facebook.js":
/*!**************************************************!*\
  !*** ./js/app/widget/curator/social/facebook.js ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _utils_common__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../utils/common */ "./js/app/widget/curator/utils/common.js");
/* harmony import */ var _utils_string__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../utils/string */ "./js/app/widget/curator/utils/string.js");
/* global window */




var SocialFacebook = {
    share: function (post) {
        var obj = post,
            cb = function(){};
        obj.url = _utils_common__WEBPACK_IMPORTED_MODULE_0__["default"].postUrl(post);
        obj.cleanText = _utils_string__WEBPACK_IMPORTED_MODULE_1__["default"].filterHtml(post.text);

        if (obj.url.indexOf('http') !== 0) {
            obj.url = obj.image;
        }
        // Disabling for now - doesn't work - seems to get error "Can't Load URL: The domain of this URL isn't
        // included in the app's domains"
        var useJSSDK = false; // window.FB
        if (useJSSDK) {
            window.FB.ui({
                method: 'feed',
                link: obj.url,
                picture: obj.image,
                name: obj.user_screen_name,
                description: obj.cleanText
            }, cb);
        } else {
            var url = "https://www.facebook.com/sharer/sharer.php?u={{url}}&d={{cleanText}}";
            var url2 = _utils_common__WEBPACK_IMPORTED_MODULE_0__["default"].tinyparser(url, obj);
            _utils_common__WEBPACK_IMPORTED_MODULE_0__["default"].popup(url2, 'twitter', '600', '430', '0');
        }
    }
};

/* harmony default export */ __webpack_exports__["default"] = (SocialFacebook);


/***/ }),

/***/ "./js/app/widget/curator/social/twitter.js":
/*!*************************************************!*\
  !*** ./js/app/widget/curator/social/twitter.js ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _utils_common__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../utils/common */ "./js/app/widget/curator/utils/common.js");
/* harmony import */ var _utils_string__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../utils/string */ "./js/app/widget/curator/utils/string.js");





var SocialTwitter = {
    share: function (post) {
        var obj = post;
        obj.url = _utils_common__WEBPACK_IMPORTED_MODULE_0__["default"].postUrl(post);
        obj.cleanText = _utils_string__WEBPACK_IMPORTED_MODULE_1__["default"].filterHtml(post.text);

        var url = "http://twitter.com/share?url={{url}}&text={{cleanText}}&hashtags={{hashtags}}";
        var url2 = _utils_common__WEBPACK_IMPORTED_MODULE_0__["default"].tinyparser(url, obj);
        _utils_common__WEBPACK_IMPORTED_MODULE_0__["default"].popup(url2, 'twitter', '600', '430', '0');
    }
};

/* harmony default export */ __webpack_exports__["default"] = (SocialTwitter);

/***/ }),

/***/ "./js/app/widget/curator/templates/general/filter.js":
/*!***********************************************************!*\
  !*** ./js/app/widget/curator/templates/general/filter.js ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);

var filterTemplate = "<div class=\"crt-filter\"> \n<div class=\"crt-filter-networks\">\n<ul class=\"crt-networks\"> \n    <li class=\"crt-filter-label\"><label><%=this._t('filter')%>:</label></li>\n    <li class=\"active\"><a href=\"#\" data-network=\"0\"> <%=this._t('all')%></a></li>\n</ul>\n</div> \n<div class=\"crt-filter-sources\">\n<ul class=\"crt-sources\"> \n    <li class=\"crt-filter-label\"><label><%=this._t('filter')%>:</label></li>\n    <li class=\"active\"><a href=\"#\" data-source=\"0\"> <%=this._t('all')%></a></li>\n</ul>\n</div> \n</div>";

/* harmony default export */ __webpack_exports__["default"] = (filterTemplate);

/***/ }),

/***/ "./js/app/widget/curator/templates/general/popup.js":
/*!**********************************************************!*\
  !*** ./js/app/widget/curator/templates/general/popup.js ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);


var v1PopupTemplate = " \n<div class=\"crt-popup\"> \n    <a href=\"#\" class=\"crt-close crt-icon-cancel\"></a> \n    <a href=\"#\" class=\"crt-next crt-icon-right-open\"></a> \n    <a href=\"#\" class=\"crt-previous crt-icon-left-open\"></a> \n    <div class=\"crt-popup-left\">  \n        <div class=\"crt-video\"> \n            <div class=\"crt-video-container\">\n                <video preload=\"none\">\n                <source src=\"<%=video%>\" type=\"video/mp4\">\n                </video>\n                <img src=\"<%=image%>\" alt=\"Image posted by <%=user_screen_name%> to <%=this.networkName()%>\" />\n                <a href=\"javascript:;\" class=\"crt-play\"><i class=\"crt-play-icon\"></i></a> \n            </div> \n        </div> \n        <div class=\"crt-image\"> \n            <img src=\"<%=image%>\" alt=\"Image posted by <%=user_screen_name%> to <%=this.networkName()%>\" /> \n        </div> \n        <div class=\"crt-pagination\"><ul></ul></div>\n    </div> \n    <div class=\"crt-popup-right\"> \n        <div class=\"crt-popup-header\"> \n            <span class=\"crt-social-icon\"><i class=\"crt-icon-<%=this.networkIcon()%>\"></i></span> \n            <img src=\"<%=user_image%>\" alt=\"Profile image for <%=user_full_name%>\"  /> \n            <div class=\"crt-post-name\"><span><%=user_full_name%></span><br/><a href=\"<%=this.userUrl()%>\" target=\"_blank\">@<%=user_screen_name%></a></div> \n        </div> \n        <div class=\"crt-popup-text <%=this.contentTextClasses()%>\"> \n            <div class=\"crt-popup-text-container\"> \n                <p class=\"crt-date\"><%=this.prettyDate(source_created_at)%></p> \n                <a class=\"crt-link\" href=\"<%= this.networkIcon() == \"facebook\" ? url :\"\" %>\" target=\"_blank\"><%= this.networkIcon() == \"facebook\" ? \"Go to post\" :\"\" %></a>\n                <div class=\"crt-popup-text-body\"><%=this.parseText(text)%></div> \n            </div> \n        </div> \n        <div class=\"crt-popup-read-more\">\n            <a href=\"#\" class=\"crt-post-read-more-button\"><%=this._t(\"read-more\")%></a> \n        </div>\n        <div class=\"crt-popup-footer\">\n            <div class=\"crt-popup-stats\"><span><%=likes%></span> <%=this._t(\"likes\", likes)%> <i class=\"sep\"></i> <span><%=comments%></span> <%=this._t(\"comments\", comments)%></div> \n            <div class=\"crt-post-share\"><span class=\"ctr-share-hint\"></span><a href=\"#\" class=\"crt-share-facebook\"><i class=\"crt-icon-facebook\"></i></a>  <a href=\"#\" class=\"crt-share-twitter\"><i class=\"crt-icon-twitter\"></i></a></div>\n        </div> \n    </div> \n</div>";

/* harmony default export */ __webpack_exports__["default"] = (v1PopupTemplate);

/***/ }),

/***/ "./js/app/widget/curator/templates/general/popup_underlay.js":
/*!*******************************************************************!*\
  !*** ./js/app/widget/curator/templates/general/popup_underlay.js ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);

var v1PopupUnderlayTemplate = '';

/* harmony default export */ __webpack_exports__["default"] = (v1PopupUnderlayTemplate);

/***/ }),

/***/ "./js/app/widget/curator/templates/general/popup_wrapper.js":
/*!******************************************************************!*\
  !*** ./js/app/widget/curator/templates/general/popup_wrapper.js ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);



var v1PopupWrapperTemplate = ' \
<div class="crt-popup-wrapper"> \
    <div class="crt-popup-wrapper-c"> \
        <div class="crt-popup-underlay"></div> \
        <div class="crt-popup-container"></div> \
    </div> \
</div>';

/* harmony default export */ __webpack_exports__["default"] = (v1PopupWrapperTemplate);

/***/ }),

/***/ "./js/app/widget/curator/templates/templates.js":
/*!******************************************************!*\
  !*** ./js/app/widget/curator/templates/templates.js ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _general_popup_underlay__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./general/popup_underlay */ "./js/app/widget/curator/templates/general/popup_underlay.js");
/* harmony import */ var _general_popup_wrapper__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./general/popup_wrapper */ "./js/app/widget/curator/templates/general/popup_wrapper.js");
/* harmony import */ var _general_popup__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./general/popup */ "./js/app/widget/curator/templates/general/popup.js");
/* harmony import */ var _general_filter__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./general/filter */ "./js/app/widget/curator/templates/general/filter.js");







var Templates = {
    'filter'                : _general_filter__WEBPACK_IMPORTED_MODULE_3__["default"],
    'popup'                 : _general_popup__WEBPACK_IMPORTED_MODULE_2__["default"],
    'popup-underlay'        : _general_popup_underlay__WEBPACK_IMPORTED_MODULE_0__["default"],
    'popup-wrapper'         : _general_popup_wrapper__WEBPACK_IMPORTED_MODULE_1__["default"],
};

/* harmony default export */ __webpack_exports__["default"] = (Templates);






/***/ }),

/***/ "./js/app/widget/curator/ui/filter.js":
/*!********************************************!*\
  !*** ./js/app/widget/curator/ui/filter.js ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _core_bus__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../core/bus */ "./js/app/widget/curator/core/bus.js");
/* harmony import */ var _core_templating__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../core/templating */ "./js/app/widget/curator/core/templating.js");
/* harmony import */ var _core_logger__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../core/logger */ "./js/app/widget/curator/core/logger.js");
/* harmony import */ var _core_events__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../core/events */ "./js/app/widget/curator/core/events.js");
/* harmony import */ var _config_networks__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../config/networks */ "./js/app/widget/curator/config/networks.js");
/* harmony import */ var _core_lib__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../core/lib */ "./js/app/widget/curator/core/lib.js");








/**
* ==================================================================
* Filter
* ==================================================================
*/

var Filter = (function (EventBus) {
    function Filter (widget) {
        var this$1 = this;

        _core_logger__WEBPACK_IMPORTED_MODULE_2__["default"].log('Filter->construct');

        EventBus.call(this);

        this.widget = widget;
        this.options = widget.options;

        this.$filter = _core_templating__WEBPACK_IMPORTED_MODULE_1__["default"].renderTemplate(this.options.templateFilter, {});
        this.$filterNetworks =  this.$filter.find('.crt-filter-networks');
        this.$filterNetworksUl =  this.$filter.find('.crt-filter-networks ul');
        this.$filterSources =  this.$filter.find('.crt-filter-sources');
        this.$filterSourcesUl =  this.$filter.find('.crt-filter-sources ul');

        this.widget.$container.append(this.$filter);

        this.$filter.on('click','.crt-filter-networks a', function (ev) {
            ev.preventDefault();
            var t = Object(_core_lib__WEBPACK_IMPORTED_MODULE_5__["default"])(ev.target);
            var networkId = t.data('network');

            this$1.$filter.find('.crt-filter-networks li').removeClass('active');
            t.parent().addClass('active');

            this$1.widget.trigger(_core_events__WEBPACK_IMPORTED_MODULE_3__["default"].FILTER_CHANGED, this$1);

            if (networkId) {
                this$1.widget.feed.params.network_id = networkId;
            } else {
                this$1.widget.feed.params.network_id = 0;
            }

            this$1.widget.feed.loadPosts(0);
        });

        this.$filter.on('click','.crt-filter-sources a', function (ev) {
            ev.preventDefault();
            var t = Object(_core_lib__WEBPACK_IMPORTED_MODULE_5__["default"])(ev.target);
            var sourceId = t.data('source');

            this$1.$filter.find('.crt-filter-sources li').removeClass('active');
            t.parent().addClass('active');

            this$1.widget.trigger(_core_events__WEBPACK_IMPORTED_MODULE_3__["default"].FILTER_CHANGED, this$1);

            if (sourceId) {
                this$1.widget.feed.params.source_id = sourceId;
            } else {
                this$1.widget.feed.params.source_id = 0;
            }

            this$1.widget.feed.loadPosts(0);
        });

        this.widget.on(_core_events__WEBPACK_IMPORTED_MODULE_3__["default"].FEED_LOADED, this.onPostsLoaded.bind(this));
    }

    if ( EventBus ) Filter.__proto__ = EventBus;
    Filter.prototype = Object.create( EventBus && EventBus.prototype );
    Filter.prototype.constructor = Filter;

    Filter.prototype.onPostsLoaded = function onPostsLoaded (event, data) {
        var this$1 = this;


        var networks = data.networks;
        var sources = data.sources;

        if (!this.filtersLoaded) {
            if (this.options.filter.showNetworks) {
                for (var i = 0, list = networks; i < list.length; i += 1) {
                    var id = list[i];

                    var network = _config_networks__WEBPACK_IMPORTED_MODULE_4__["default"][id];
                    if (network) {
                        this$1.$filterNetworksUl.append('<li><a href="#" data-network="' + id + '"><i class="' + network.icon + '"></i> ' + network.name + '</a></li>');
                    } else {
                        //console.log(id);
                    }
                }
            } else {
                this.$filterNetworks.hide();
            }

            if (this.options.filter.showSources) {
                for (var i$1 = 0, list$1 = sources; i$1 < list$1.length; i$1 += 1) {
                    var source = list$1[i$1];

                    var network$1 = _config_networks__WEBPACK_IMPORTED_MODULE_4__["default"][source.network_id];
                    if (network$1) {
                        this$1.$filterSourcesUl.append('<li><a href="#" data-source="' + source.id + '"><i class="' + network$1.icon + '"></i> ' + source.name + '</a></li>');
                    } else {
                        // console.log(source.network_id);
                    }
                }
            } else {
                this.$filterSources.hide();
            }

            this.filtersLoaded = true;
        }
    };

    Filter.prototype.destroy = function destroy () {
        this.$filter.remove();
    };

    return Filter;
}(_core_bus__WEBPACK_IMPORTED_MODULE_0__["default"]));

/* harmony default export */ __webpack_exports__["default"] = (Filter);

/***/ }),

/***/ "./js/app/widget/curator/ui/popup.js":
/*!*******************************************!*\
  !*** ./js/app/widget/curator/ui/popup.js ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _core_logger__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../core/logger */ "./js/app/widget/curator/core/logger.js");
/* harmony import */ var _social_facebook__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../social/facebook */ "./js/app/widget/curator/social/facebook.js");
/* harmony import */ var _social_twitter__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../social/twitter */ "./js/app/widget/curator/social/twitter.js");
/* harmony import */ var _utils_string__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../utils/string */ "./js/app/widget/curator/utils/string.js");
/* harmony import */ var _core_templating__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../core/templating */ "./js/app/widget/curator/core/templating.js");
/* harmony import */ var _core_lib__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../core/lib */ "./js/app/widget/curator/core/lib.js");
/* harmony import */ var _utils_common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../utils/common */ "./js/app/widget/curator/utils/common.js");









/**
 * ==================================================================
 * Popup
 * ==================================================================
 */

var Popup = function Popup (popupManager, post, widget) {
    var this$1 = this;

    _core_logger__WEBPACK_IMPORTED_MODULE_0__["default"].log("Popup->init ");
 
    this.popupManager = popupManager;
    this.json = post;
    this.widget = widget;

    var templateId = this.widget.options.templatePopup;
    this.videoPlaying=false;

    this.$popup = _core_templating__WEBPACK_IMPORTED_MODULE_4__["default"].renderTemplate(templateId, this.json);
    this.$left = this.$popup.find('.crt-popup-left');

    if (this.json.image) {
        this.$popup.addClass('has-image');
    }

    if (this.json.video) {
        this.$popup.addClass('has-video');
    }

    if (this.json.video && this.json.video.indexOf('youtu') >= 0 )
    {
        // youtube
        this.$popup.find('video').remove();
        // this.$popup.removeClass('has-image');

        var youTubeId = _utils_string__WEBPACK_IMPORTED_MODULE_3__["default"].youtubeVideoId(this.json.video);

        var src = "<div class=\"crt-responsive-video\"><iframe id=\"ytplayer\" src=\"https://www.youtube.com/embed/" + youTubeId + "?autoplay=0&rel=0&showinfo\" frameborder=\"0\" allowfullscreen></iframe></div>";

        this.$popup.find('.crt-video-container img').remove();
        this.$popup.find('.crt-video-container a').remove();
        this.$popup.find('.crt-video-container').append(src);
    } else if (this.json.video && this.json.video.indexOf('vimeo') >= 0 )
    {
        // youtube
        this.$popup.find('video').remove();
        // this.$popup.removeClass('has-image');

        var vimeoId = _utils_string__WEBPACK_IMPORTED_MODULE_3__["default"].vimeoVideoId(this.json.video);

        if (vimeoId) {
            var src$1 = "<div class=\"crt-responsive-video\"><iframe src=\"https://player.vimeo.com/video/" + vimeoId + "?color=ffffff&title=0&byline=0&portrait=0\" frameborder=\"0\" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>";
            this.$popup.find('.crt-video-container img').remove();
            this.$popup.find('.crt-video-container a').remove();
            this.$popup.find('.crt-video-container').append(src$1);
        }
    }

    if (this.json.images)
    {
        this.$page = this.$popup.find('.crt-pagination ul');
        for (var i = 0;i < this.json.images.length;i++) {
            this$1.$page.append('<li><a href="" data-page="'+i+'"></a></li>');
        }
        this.$page.find('a').click(this.onPageClick.bind(this));
        this.currentImage = 0;
        this.$page.find('li:nth-child('+(this.currentImage+1)+')').addClass('selected');
    }

    this.$popup.on('click',' .crt-close', this.onClose.bind(this));
    this.$popup.on('click',' .crt-previous', this.onPrevious.bind(this));
    this.$popup.on('click',' .crt-next', this.onNext.bind(this));
    this.$popup.on('click',' .crt-play', this.onPlay.bind(this));
    this.$popup.on('click','.crt-share-facebook',this.onShareFacebookClick.bind(this));
    this.$popup.on('click','.crt-share-twitter',this.onShareTwitterClick.bind(this));

    Object(_core_lib__WEBPACK_IMPORTED_MODULE_5__["default"])(window).on('resize.crt-popup',_utils_common__WEBPACK_IMPORTED_MODULE_6__["default"].debounce(this.onResize.bind(this),50));

    this.onResize ();
};

Popup.prototype.onResize = function onResize () {
    _core_logger__WEBPACK_IMPORTED_MODULE_0__["default"].log('Popup->onResize');
    var windowWidth = Object(_core_lib__WEBPACK_IMPORTED_MODULE_5__["default"])(window).width ();
    var padding = 60;
    var paddingMobile = 40;
    var rightPanel = 335;
    var leftPanelMax = 600;

    if (windowWidth > 1055) {
        this.$left.width(leftPanelMax+rightPanel);
    } else if (windowWidth > 910) {
        this.$left.width(windowWidth-(padding*2));
    } else if (windowWidth > leftPanelMax+(paddingMobile*2)) {
        this.$left.width(600);
    } else {
        this.$left.width(windowWidth-(paddingMobile*2));
    }
};

Popup.prototype.onPageClick = function onPageClick (ev) {
    ev.preventDefault();
    var a = Object(_core_lib__WEBPACK_IMPORTED_MODULE_5__["default"])(ev.target);
    var page = a.data('page');

    var image = this.json.images[page];

    this.$popup.find('.crt-image img').attr('src',image.url);
    this.currentImage = page;

    this.$page.find('li').removeClass('selected');
    this.$page.find('li:nth-child('+(this.currentImage+1)+')').addClass('selected');
};

Popup.prototype.onShareFacebookClick = function onShareFacebookClick (ev) {
    ev.preventDefault();
    _social_facebook__WEBPACK_IMPORTED_MODULE_1__["default"].share(this.json);
    this.widget.track('share:facebook');
    return false;
};

Popup.prototype.onShareTwitterClick = function onShareTwitterClick (ev) {
    ev.preventDefault();
    _social_twitter__WEBPACK_IMPORTED_MODULE_2__["default"].share(this.json);
    this.widget.track('share:twitter');
    return false;
};

Popup.prototype.onClose = function onClose (e) {
    e.preventDefault();
    var that = this;
    this.hide(function(){
        that.popupManager.onClose();
    });
};

Popup.prototype.onPrevious = function onPrevious (e) {
    e.preventDefault();

    this.popupManager.onPrevious();
};

Popup.prototype.onNext = function onNext (e) {
    e.preventDefault();

    this.popupManager.onNext();
};

Popup.prototype.onPlay = function onPlay (e) {
    _core_logger__WEBPACK_IMPORTED_MODULE_0__["default"].log('Popup->onPlay');
    e.preventDefault();

    this.videoPlaying = !this.videoPlaying;

    if (this.videoPlaying) {
        this.$popup.find('video')[0].play();
        this.widget.track('video:play');
    } else {
        this.$popup.find('video')[0].pause();
        this.widget.track('video:pause');
    }

    _core_logger__WEBPACK_IMPORTED_MODULE_0__["default"].log(this.videoPlaying);

    this.$popup.toggleClass('video-playing',this.videoPlaying );
};

Popup.prototype.show = function show () {
    //
    // let post = this.json;
    // let mediaUrl = post.image,
    // text = post.text;
    //
    // if (mediaUrl) {
    // let $imageWrapper = that.$el.find('div.main-image-wrapper');
    // this.loadMainImage(mediaUrl, $imageWrapper, ['main-image']);
    // }
    //
    // let $socialIcon = this.$el.find('.social-icon');
    // $socialIcon.attr('class', 'social-icon');
    //
    // //format the date
    // let date = Curator.Utils.dateAsDayMonthYear(post.sourceCreateAt);
    //
    // this.$el.find('input.discovery-id').val(post.id);
    // this.$el.find('div.full-name span').html(post.user_full_name);
    // this.$el.find('div.username span').html('@' + post.user_screen_name);
    // this.$el.find('div.date span').html(date);
    // this.$el.find('div.love-indicator span').html(post.loves);
    // this.$el.find('div.side-text span').html(text);
    //
    // this.wrapper.show();
    this.$popup.fadeIn(function () {
        // that.$popup.find('.crt-popup').animate({width:950}, function () {
        // z('.popup .content').fadeIn('slow');
        // });
    });
};
    
Popup.prototype.hide = function hide (callback) {
    _core_logger__WEBPACK_IMPORTED_MODULE_0__["default"].log('Popup->hide');
    var that = this;
    this.$popup.fadeOut(function(){
        that.destroy();
        callback ();
    });
};
    
Popup.prototype.destroy = function destroy () {
    if (this.$popup && this.$popup.length) {
        this.$popup.remove();

        if (this.$popup.find('video').length) {
            this.$popup.find('video')[0].pause();

        }
    }

    Object(_core_lib__WEBPACK_IMPORTED_MODULE_5__["default"])(window).off('resize.crt-popup');

    delete this.$popup;
};

/* harmony default export */ __webpack_exports__["default"] = (Popup);

/***/ }),

/***/ "./js/app/widget/curator/ui/popup_manager.js":
/*!***************************************************!*\
  !*** ./js/app/widget/curator/ui/popup_manager.js ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _core_logger__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../core/logger */ "./js/app/widget/curator/core/logger.js");
/* harmony import */ var _popup__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./popup */ "./js/app/widget/curator/ui/popup.js");
/* harmony import */ var _core_templating__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../core/templating */ "./js/app/widget/curator/core/templating.js");
/* harmony import */ var _core_lib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../core/lib */ "./js/app/widget/curator/core/lib.js");
/**
* ==================================================================
* Popup Manager
* ==================================================================
*/







var PopupManager = function PopupManager (widget) {
    _core_logger__WEBPACK_IMPORTED_MODULE_0__["default"].log("PopupManager->init ");

    this.widget = widget;
    var templateId = this.widget.options.templatePopupWrapper;

    this.$wrapper = _core_templating__WEBPACK_IMPORTED_MODULE_2__["default"].renderTemplate(templateId, {});
    this.$popupContainer = this.$wrapper.find('.crt-popup-container');
    this.$underlay = this.$wrapper.find('.crt-popup-underlay');

    Object(_core_lib__WEBPACK_IMPORTED_MODULE_3__["default"])('body').append(this.$wrapper);
    this.$underlay.click(this.onUnderlayClick.bind(this));
};

PopupManager.prototype.showPopup = function showPopup (post) {
        var this$1 = this;

    if (this.popup) {
        this.popup.hide(function () {
            this$1.popup.destroy();
            this$1.showPopup2(post);
        });
    } else {
        this.showPopup2(post);
    }

};

PopupManager.prototype.showPopup2 = function showPopup2 (post) {
        var this$1 = this;

    this.popup = new _popup__WEBPACK_IMPORTED_MODULE_1__["default"](this, post, this.widget);
    this.$popupContainer.append(this.popup.$popup);

    this.$wrapper.show();

    if (this.$underlay.css('display') !== 'block') {
        this.$underlay.fadeIn();
    }
    this.popup.show();

    Object(_core_lib__WEBPACK_IMPORTED_MODULE_3__["default"])('body').addClass('crt-popup-visible');

    this.currentPostNum = 0;
    for(var i=0;i < this.posts.length;i++)
    {
        // console.log (post.json.id +":"+this.posts[i].id);
        if (post.id === this$1.posts[i].id) {
            this$1.currentPostNum = i;
            _core_logger__WEBPACK_IMPORTED_MODULE_0__["default"].log('Found post '+i);
            break;
        }
    }

    this.widget.track('popup:show');
};

PopupManager.prototype.setPosts = function setPosts (posts) {
    this.posts = posts;
};

PopupManager.prototype.onClose = function onClose () {
    this.hide();
};

PopupManager.prototype.onPrevious = function onPrevious () {
    this.currentPostNum-=1;
    this.currentPostNum = this.currentPostNum>=0?this.currentPostNum:this.posts.length-1; // loop back to start

    this.showPopup(this.posts[this.currentPostNum]);
};

PopupManager.prototype.onNext = function onNext () {
    this.currentPostNum+=1;
    this.currentPostNum = this.currentPostNum<this.posts.length?this.currentPostNum:0; // loop back to start

    this.showPopup(this.posts[this.currentPostNum]);
};

PopupManager.prototype.onUnderlayClick = function onUnderlayClick (e) {
    _core_logger__WEBPACK_IMPORTED_MODULE_0__["default"].log('PopupManager->onUnderlayClick');
    e.preventDefault();

    if (this.popup) {
        this.popup.hide(function () {
            this.hide();
        }.bind(this));
    }
};

PopupManager.prototype.hide = function hide () {
        var this$1 = this;

    _core_logger__WEBPACK_IMPORTED_MODULE_0__["default"].log('PopupManager->hide');
    this.widget.track('popup:hide');
    Object(_core_lib__WEBPACK_IMPORTED_MODULE_3__["default"])('body').removeClass('crt-popup-visible');
    this.currentPostNum = 0;
    this.popup = null;
    this.$underlay.fadeOut(function () {
        this$1.$underlay.css({'display':'','opacity':''});
        this$1.$wrapper.hide();
    });
};
    
PopupManager.prototype.destroy = function destroy () {

    this.$underlay.remove();

    delete this.$popup;
    delete this.$underlay;
};

/* harmony default export */ __webpack_exports__["default"] = (PopupManager);

/***/ }),

/***/ "./js/app/widget/curator/ui/post.js":
/*!******************************************!*\
  !*** ./js/app/widget/curator/ui/post.js ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _core_bus__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../core/bus */ "./js/app/widget/curator/core/bus.js");
/* harmony import */ var _social_facebook__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../social/facebook */ "./js/app/widget/curator/social/facebook.js");
/* harmony import */ var _social_twitter__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../social/twitter */ "./js/app/widget/curator/social/twitter.js");
/* harmony import */ var _core_logger__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../core/logger */ "./js/app/widget/curator/core/logger.js");
/* harmony import */ var _core_events__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../core/events */ "./js/app/widget/curator/core/events.js");
/* harmony import */ var _core_templating__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../core/templating */ "./js/app/widget/curator/core/templating.js");
/* harmony import */ var _core_lib__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../core/lib */ "./js/app/widget/curator/core/lib.js");
/* harmony import */ var _list_post_template__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../list-post-template */ "./js/app/widget/list-post-template.js");











/**
* ==================================================================
* Post
* ==================================================================
*/


var Post = (function (EventBus) {
    function Post (postJson, options, widget) {
        var this$1 = this;

        EventBus.call(this);

        this.options = options;
        this.widget = widget;

        var templateId = this.widget.options.templatePost;

        this.json = postJson;
        this.$el = _core_templating__WEBPACK_IMPORTED_MODULE_5__["default"].renderDiv(_list_post_template__WEBPACK_IMPORTED_MODULE_7__["default"], postJson);

        this.$postC = this.$el.find('.crt-post-c');
        this.$image = this.$el.find('.crt-post-image');
        this.$imageContainer = this.$el.find('.crt-image-c');

        this.$el.find('.crt-share-facebook').click(this.onShareFacebookClick.bind(this));
        this.$el.find('.crt-share-twitter').click(this.onShareTwitterClick.bind(this));
        // this.$el.find('.crt-hitarea').click(this.onPostClick.bind(this));
        this.$el.find('.crt-post-read-more-button').click(this.onReadMoreClick.bind(this));
        // this.$el.on('click','.crt-post-text-body a',this.onLinkClick.bind(this));

        this.$postC.click(this.onPostClick.bind(this));

        this.$image.css({opacity:0});

        if (this.json.image) {
            this.$image.on('load', this.onImageLoaded.bind(this));
            this.$image.on('error', this.onImageError.bind(this));
        } else {
            // no image ... call this.onImageLoaded
            window.setTimeout(function () {
                this$1.setHeight();
            },100);
        }

        if (this.json.image_width > 0) {
            var p = (this.json.image_height/this.json.image_width)*100;
            this.$imageContainer.addClass('crt-image-responsive')
                .css('padding-bottom',p+'%');
        }

        if (this.json.url.indexOf('http') !== 0) {
            this.$el.find('.crt-post-share').hide ();
        }

        this.$image.data('dims',this.json.image_width+':'+this.json.image_height);

        if (this.json.video) {
            this.$el.addClass('crt-post-has-video');
        }

        if (this.json.images && this.json.images.length > 0) {
            this.$el.addClass('crt-has-image-carousel');
        }
    }

    if ( EventBus ) Post.__proto__ = EventBus;
    Post.prototype = Object.create( EventBus && EventBus.prototype );
    Post.prototype.constructor = Post;

    Post.prototype.onShareFacebookClick = function onShareFacebookClick (ev) {
        ev.preventDefault();
        _social_facebook__WEBPACK_IMPORTED_MODULE_1__["default"].share(this.json);
        this.widget.track('share:facebook');
        return false;
    };

    Post.prototype.onShareTwitterClick = function onShareTwitterClick (ev) {
        ev.preventDefault();
        _social_twitter__WEBPACK_IMPORTED_MODULE_2__["default"].share(this.json);
        this.widget.track('share:twitter');
        return false;
    };

    Post.prototype.onPostClick = function onPostClick (ev) {
        _core_logger__WEBPACK_IMPORTED_MODULE_3__["default"].log('Post->click');

        var target = Object(_core_lib__WEBPACK_IMPORTED_MODULE_6__["default"])(ev.target);

        // console.log(target[0].className.indexOf('read-more'));
        // console.log(target.attr('href'));

        if (target[0] && target[0].className.indexOf('read-more') > 0) {
            // ignore read more clicks
            return;
        }

        if (target.is('a') && target.attr('href') !== '#' && target.attr('href') !== 'javascript:;') {
            this.widget.track('click:link');
        } else {
            ev.preventDefault();
            this.trigger(_core_events__WEBPACK_IMPORTED_MODULE_4__["default"].POST_CLICK, this, ev);
        }

    };

    Post.prototype.onReadMoreClick = function onReadMoreClick (ev) {
        ev.preventDefault();

        this.widget.track('click:read-more');
        this.trigger(_core_events__WEBPACK_IMPORTED_MODULE_4__["default"].POST_CLICK_READ_MORE, this, this.json, ev);
    };

    Post.prototype.onImageLoaded = function onImageLoaded () {
        this.$image.animate({opacity:1});

        this.setHeight();

        this.trigger(_core_events__WEBPACK_IMPORTED_MODULE_4__["default"].POST_IMAGE_LOADED, this);
        this.widget.trigger(_core_events__WEBPACK_IMPORTED_MODULE_4__["default"].POST_IMAGE_LOADED, this);
    };

    Post.prototype.onImageError = function onImageError () {
        // Unable to load image!!!
        this.$image.hide();

        this.setHeight();

        this.trigger(_core_events__WEBPACK_IMPORTED_MODULE_4__["default"].POST_IMAGE_FAILED, this);
        this.widget.trigger(_core_events__WEBPACK_IMPORTED_MODULE_4__["default"].POST_IMAGE_FAILED, this);
    };

    Post.prototype.setHeight = function setHeight () {
        var height = this.$postC.height();
        if (this.options.maxHeight && this.options.maxHeight > 0 && height > this.options.maxHeight) {
            this.$postC
                .css({maxHeight: this.options.maxHeight});
            this.$el.addClass('crt-post-max-height');
        }

        this.layout();
    };

    Post.prototype.getHeight = function getHeight () {
        if (this.$el.hasClass('crt-post-max-height')) {
            return this.$postC.height();
        } else {
            // let $pane = z(this.$panes[i]);
            var contentHeight = this.$el.find('.crt-post-content').height();
            var footerHeight = this.$el.find('.crt-post-footer').height();
            return contentHeight + footerHeight + 2;
        }
    };

    Post.prototype.layout = function layout () {
        // Logger.log("Post->layout");
        this.layoutFooter();
    };

    Post.prototype.layoutFooter = function layoutFooter () {
        // Logger.log("Post->layoutFooter");
        var $userName = this.$el.find('.crt-post-username');
        var $date = this.$el.find('.crt-date');
        var $footer = this.$el.find('.crt-post-footer');
        var $share = this.$el.find('.crt-post-share');
        var $userImage = this.$el.find('.crt-post-userimage');

        var footerWidth = $footer.width();
        var padding = 40;
        var elementsWidth = $userName.width() + $date.width() + $share.width() + $userImage.width() + padding;

        if (elementsWidth > footerWidth) {
            $userName.hide();
        }
    };

    return Post;
}(_core_bus__WEBPACK_IMPORTED_MODULE_0__["default"]));

/* harmony default export */ __webpack_exports__["default"] = (Post);

/***/ }),

/***/ "./js/app/widget/curator/utils/common.js":
/*!***********************************************!*\
  !*** ./js/app/widget/curator/utils/common.js ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);

var CommonUtils = {
    postUrl: function postUrl (post)
    {
        if (post.url && post.url !== "" && post.url !== "''")
        {
            // instagram
            return post.url;
        }

        if (post.network_id+"" === "1")
        {
            // twitter
            return 'https://twitter.com/'+post.user_screen_name+'/status/'+post.source_identifier;
        }

        return '';
    },

    center: function center (w, h, bound) {
        var s = window.screen,
            b = bound || {},
            bH = b.height || s.height,
            bW = b.width || s.height;

        return {
            top: (bH) ? (bH - h) / 2 : 0,
            left: (bW) ? (bW - w) / 2 : 0
        };
    },

    popup: function popup (mypage, myname, w, h, scroll) {

        var position = this.center(w, h),
            settings = 'height=' + h + ',width=' + w + ',top=' + position.top +
                ',left=' + position.left + ',scrollbars=' + scroll +
                ',resizable';

        window.open(mypage, myname, settings);
    },

    tinyparser: function tinyparser (string, obj) {
        return string.replace(/\{\{(.*?)\}\}/g, function (a, b) {
            return obj && typeof obj[b] !== "undefined" ? encodeURIComponent(obj[b]) : "";
        });
    },

    debounce: function debounce (func, wait, immediate) {
        var timeout;
        return function() {
            var context = this, args = arguments;
            var later = function() {
                timeout = null;
                if (!immediate) { func.apply(context, args); }
            };
            var callNow = immediate && !timeout;
            window.clearTimeout(timeout);
            timeout = window.setTimeout(later, wait);
            if (callNow) { func.apply(context, args); }
        };
    },

    uId: function uId () {
        // Math.random should be unique because of its seeding algorithm.
        // Convert it to base 36 (numbers + letters), and grab the first 9 characters
        // after the decimal.
        return '_' + Math.random().toString(36).substr(2, 9);
    }
};

/* harmony default export */ __webpack_exports__["default"] = (CommonUtils);

/***/ }),

/***/ "./js/app/widget/curator/utils/date.js":
/*!*********************************************!*\
  !*** ./js/app/widget/curator/utils/date.js ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _core_translate__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../core/translate */ "./js/app/widget/curator/core/translate.js");


var DateUtils = {
    /**
     * Parse a date string in form DD/MM/YYYY HH:MM::SS - returns as UTC
     */
    dateFromString: function dateFromString(time) {
        var dtstr = time.replace(/\D/g," ");
        var dtcomps = dtstr.split(" ");

        // modify month between 1 based ISO 8601 and zero based Date
        dtcomps[1]--;

        var date = new Date(Date.UTC(dtcomps[0],dtcomps[1],dtcomps[2],dtcomps[3],dtcomps[4],dtcomps[5]));

        return date;
    },

    /**
     * Format the date as DD/MM/YYYY
     */
    dateAsDayMonthYear: function dateAsDayMonthYear(strEpoch) {
        var myDate = new Date(parseInt(strEpoch, 10));
        // console.log(myDate.toGMTString()+"<br>"+myDate.toLocaleString());

        var day = myDate.getDate() + '';
        var month = (myDate.getMonth() + 1) + '';
        var year = myDate.getFullYear() + '';

        day = day.length === 1 ? '0' + day : day;
        month = month.length === 1 ? '0' + month : month;

        var created = day + '/' + month + '/' + year;

        return created;
    },

    /**
     * Convert the date into a time array
     */
    dateAsTimeArray: function dateAsTimeArray(strEpoch) {
        var myDate = new Date(parseInt(strEpoch, 10));

        var hours = myDate.getHours() + '';
        var mins = myDate.getMinutes() + '';
        var ampm;

        if (hours >= 12) {
            ampm = 'PM';
            if (hours > 12) {
                hours = (hours - 12) + '';
            }
        }
        else {
            ampm = 'AM';
        }

        hours = hours.length === 1 ? '0' + hours : hours; //console.log(hours.length);
        mins = mins.length === 1 ? '0' + mins : mins; //console.log(mins);

        var array = [
            parseInt(hours.charAt(0), 10),
            parseInt(hours.charAt(1), 10),
            parseInt(mins.charAt(0), 10),
            parseInt(mins.charAt(1), 10),
            ampm
        ];

        return array;
    },


    fuzzyDate: function fuzzyDate (dateString) {
        var date = Date.parse(dateString+' UTC');
        var delta = Math.round((new Date () - date) / 1000);

        var minute = 60,
            hour = minute * 60,
            day = hour * 24,
            week = day * 7,
            month = day * 30;

        var fuzzy;

        if (delta < 30) {
            fuzzy = 'Just now';
        } else if (delta < minute) {
            fuzzy = delta + ' seconds ago';
        } else if (delta < 2 * minute) {
            fuzzy = 'a minute ago.';
        } else if (delta < hour) {
            fuzzy = Math.floor(delta / minute) + ' minutes ago';
        } else if (Math.floor(delta / hour) === 1) {
            fuzzy = '1 hour ago.';
        } else if (delta < day) {
            fuzzy = Math.floor(delta / hour) + ' hours ago';
        } else if (delta < day * 2) {
            fuzzy = 'Yesterday';
        } else if (delta < week) {
            fuzzy = 'This week';
        } else if (delta < week * 2) {
            fuzzy = 'Last week';
        } else if (delta < month) {
            fuzzy = 'This month';
        } else {
            fuzzy = date;
        }

        return fuzzy;
    },

    prettyDate: function prettyDate (time) {
        var date = DateUtils.dateFromString(time);

        var diff = (((new Date()).getTime() - date.getTime()) / 1000);
        var day_diff = Math.floor(diff / 86400);
        var year = date.getFullYear().toString().substr(-2),
            month = date.getMonth()+1,
            day = date.getDate();

        if (isNaN(day_diff) || day_diff < 0 || day_diff >= 31) {
            return ((day < 10) ? '0' + day.toString() : day.toString()) + '/' + ((month < 10) ? '0' + month.toString() : month.toString()) + '/' + year;
        }

        var minute_diff = Math.floor(diff / 60);
        var hour_diff = Math.floor(diff / 3600);
        var week_diff = Math.ceil(day_diff / 7);

        var r =
            (
                (
                    day_diff === 0 &&
                    (
                        (diff < 60 && _core_translate__WEBPACK_IMPORTED_MODULE_0__["default"].t("Just now")) ||
                        (diff < 3600 && _core_translate__WEBPACK_IMPORTED_MODULE_0__["default"].t("minutes ago", minute_diff)) || //
                        (diff < 86400 && _core_translate__WEBPACK_IMPORTED_MODULE_0__["default"].t("hours ago", hour_diff)) // + " hours ago")
                    )
                ) ||
                (day_diff === 1 && _core_translate__WEBPACK_IMPORTED_MODULE_0__["default"].t("Yesterday")) ||
                (day_diff < 7 && _core_translate__WEBPACK_IMPORTED_MODULE_0__["default"].t("days ago",day_diff)) ||
                (day_diff < 31 && _core_translate__WEBPACK_IMPORTED_MODULE_0__["default"].t("weeks ago",week_diff))
            );
        return r;
    }
};

/* harmony default export */ __webpack_exports__["default"] = (DateUtils);

/***/ }),

/***/ "./js/app/widget/curator/utils/html.js":
/*!*********************************************!*\
  !*** ./js/app/widget/curator/utils/html.js ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _core_logger__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../core/logger */ "./js/app/widget/curator/core/logger.js");
/* harmony import */ var _core_lib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../core/lib */ "./js/app/widget/curator/core/lib.js");



var HtmlUtils = {
    checkContainer: function checkContainer (container) {
        _core_logger__WEBPACK_IMPORTED_MODULE_0__["default"].log("Curator->checkContainer: " + container);
        if (Object(_core_lib__WEBPACK_IMPORTED_MODULE_1__["default"])(container).length === 0) {
            _core_logger__WEBPACK_IMPORTED_MODULE_0__["default"].error('Curator could not find the element ' + container + '. Please ensure this element existings in your HTML code. Exiting.');
            return false;
        }
        return true;
    },

    checkPowered: function checkPowered (jQuerytag) {
        _core_logger__WEBPACK_IMPORTED_MODULE_0__["default"].log("Curator->checkPowered");
        var h = jQuerytag.html();
        // Logger.log (h);
        if (h.indexOf('Curator') > 0) {
            return true;
        } else {
            window.alert('Container is missing Powered by Curator');
            return false;
        }
    },

    addCSSRule: function addCSSRule (sheet, selector, rules, index) {
        index = index || 0;
        if ('insertRule' in sheet) {
            sheet.insertRule(selector + '{' + rules + '}', 0);
        }
        else if ('addRule' in sheet) {
            sheet.addRule(selector, rules);
        }
    },

    createSheet: function createSheet () {
        var style = document.createElement("style");
        // WebKit hack :(
        style.appendChild(document.createTextNode(""));
        document.head.appendChild(style);
        return style.sheet;
    },

    loadCSS: function loadCSS () {
        // not used!
    },

    isTouch: function isTouch () {
        var b = false;
        try {
            b = ("ontouchstart" in document.documentElement);
        } catch (e) {
        }

        return b;
    },

    isVisible: function isVisible (el) {
        if(el.css('display')!=='none' && el.css('visibility')!=='hidden' && el.width()>0) {
            return true;
        } else {
            return false;
        }
    }
};


/* harmony default export */ __webpack_exports__["default"] = (HtmlUtils);

/***/ }),

/***/ "./js/app/widget/curator/utils/string.js":
/*!***********************************************!*\
  !*** ./js/app/widget/curator/utils/string.js ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _libraries_twitter_text_regex__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../libraries/twitter-text-regex */ "./js/app/widget/curator/libraries/twitter-text-regex.js");



var StringUtils = {

    camelize: function camelize (s) {
        return s.replace (/(?:^|[-_])(\w)/g, function (_, c) {
            return c ? c.toUpperCase () : '';
        });
    },

    twitterLinks: function twitterLinks (s)
    {
        s = s.replace(/[@]+[A-Za-z0-9-_]+/g, function(u) {
            var username = u.replace("@","");
            return StringUtils.url("https://twitter.com/"+username,u);
        });
        s = s.replace(_libraries_twitter_text_regex__WEBPACK_IMPORTED_MODULE_0__["default"].txt.regexen.validHashtag, function(t) {
            var tag = t.replace("#","%23");
            return StringUtils.url("https://twitter.com/search?q="+tag,t);
        });

        return s;
    },

    instagramLinks: function instagramLinks (s)
    {
        s = s.replace(/[@]+[A-Za-z0-9-_\.]+/g, function(u) {
            var username = u.replace("@","");
            return StringUtils.url("https://www.instagram.com/"+username+'/',u);
        });
        s = s.replace(_libraries_twitter_text_regex__WEBPACK_IMPORTED_MODULE_0__["default"].txt.regexen.validHashtag, function(t) {
            var tag = t.replace("#","");
            return StringUtils.url("https://www.instagram.com/explore/tags/"+tag+'/',t);
        });

        return s;
    },

    facebookLinks: function facebookLinks (s)
    {
        s = s.replace(/[@]+[A-Za-z0-9-_]+/g, function(u) {
            var username = u.replace("@","");
            return StringUtils.url("https://www.facebook.com/"+username+'/',u);
        });
        s = s.replace(/[#]+[A-Za-z0-9-_]+/g, function(t) {
            var tag = t.replace("#","%23");
            return StringUtils.url("https://www.facebook.com/search/top/?q="+tag,t);
        });

        return s;
    },

    linksToHref: function linksToHref (s)
    {
        s = s.replace(/[A-Za-z]+:\/\/[A-Za-z0-9-_]+\.[A-Za-z0-9-_:%&~\?\/.=]+[A-Za-z0-9-_:%&~\?\/=]+/g, function(url) {
            return StringUtils.url(url);
        });

        return s;
    },

    url: function url (s,t) {
        t = t || s;
        return '<a href="'+s+'" target="_blank">'+t+'</a>';
    },

    youtubeVideoId: function youtubeVideoId (url){
        var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
        var match = url.match(regExp);

        if (match && match[7].length === 11) {
            return match[7];
        } else {
            // above doesn't work if video id starts with v
            // eg https://www.youtube.com/embed/vDbr_EamBK4?autoplay=1

            var regExp$1 = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/))([^#\&\?]*).*/;
            var match2 = url.match(regExp$1);
            if (match2 && match2[6].length === 11) {
                return match2[6];
            }
        }

        return false;
    },

    vimeoVideoId: function vimeoVideoId (url) {
        var regExp = /(?:www\.|player\.)?vimeo.com\/(?:channels\/(?:\w+\/)?|groups\/(?:[^\/]*)\/videos\/|album\/(?:\d+)\/video\/|video\/|)(\d+)(?:[a-zA-Z0-9_\-]+)?/;
        var match = url.match(regExp);
        
        if (match && match.length>=2) {
            return match[1];
        }

        return false;
    },

    filterHtml: function filterHtml (html) {
        try {
            var div = document.createElement("div");
            div.innerHTML = html;
            var text = div.textContent || div.innerText || "";
            return text;
        } catch (e) {
            return html;
        }
    },

    nl2br:function(s) {
        s = s.trim();
        s = s.replace(/(?:\r\n|\r|\n)/g, '<br />');

        return s;
    }
};

/* harmony default export */ __webpack_exports__["default"] = (StringUtils);

/***/ }),

/***/ "./js/app/widget/curator/widgets/base.js":
/*!***********************************************!*\
  !*** ./js/app/widget/curator/widgets/base.js ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _core_bus__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../core/bus */ "./js/app/widget/curator/core/bus.js");
/* harmony import */ var _utils_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../utils/common */ "./js/app/widget/curator/utils/common.js");
/* harmony import */ var _core_logger__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../core/logger */ "./js/app/widget/curator/core/logger.js");
/* harmony import */ var _utils_html__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../utils/html */ "./js/app/widget/curator/utils/html.js");
/* harmony import */ var _core_feed__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../core/feed */ "./js/app/widget/curator/core/feed.js");
/* harmony import */ var _core_ajax__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../core/ajax */ "./js/app/widget/curator/core/ajax.js");
/* harmony import */ var _core_events__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../core/events */ "./js/app/widget/curator/core/events.js");
/* harmony import */ var _ui_post__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../ui/post */ "./js/app/widget/curator/ui/post.js");
/* harmony import */ var _ui_filter__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../ui/filter */ "./js/app/widget/curator/ui/filter.js");
/* harmony import */ var _ui_popup_manager__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../ui/popup_manager */ "./js/app/widget/curator/ui/popup_manager.js");
/* harmony import */ var _core_lib__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../core/lib */ "./js/app/widget/curator/core/lib.js");
/* harmony import */ var _core_translate__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../core/translate */ "./js/app/widget/curator/core/translate.js");
/* harmony import */ var _core_globals__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../core/globals */ "./js/app/widget/curator/core/globals.js");














var Widget = (function (EventBus) {
    function Widget () {
        _core_logger__WEBPACK_IMPORTED_MODULE_2__["default"].log('Widget->construct');

        EventBus.call (this);

        this.id = _utils_common__WEBPACK_IMPORTED_MODULE_1__["default"].uId ();
    }

    if ( EventBus ) Widget.__proto__ = EventBus;
    Widget.prototype = Object.create( EventBus && EventBus.prototype );
    Widget.prototype.constructor = Widget;

    Widget.prototype.init = function init (options, defaults) {
        var this$1 = this;

        if (!options) {
            console.error('options missing');
            return false;
        }

        this.options = _core_lib__WEBPACK_IMPORTED_MODULE_10__["default"].extend(true,{}, defaults, options);

        if(!options.container) {
            console.error('options.container missing');
            return false;
        }

        if (!_utils_html__WEBPACK_IMPORTED_MODULE_3__["default"].checkContainer(this.options.container)) {
            return false;
        }
        this.$container = Object(_core_lib__WEBPACK_IMPORTED_MODULE_10__["default"])(this.options.container);

        if (!this.options.feedId) {
            console.error('options.feedId missing');
        }

        this.$container.addClass('crt-feed');
        this.$container.addClass('crt-feed-container');

        if (_utils_html__WEBPACK_IMPORTED_MODULE_3__["default"].isTouch()) {
            this.$container.addClass('crt-touch');
        } else {
            this.$container.addClass('crt-no-touch');
        }

        // get inline options
        var inlineOptions = [
            'lang',
            'debug'
        ];
        for (var i = 0, list = inlineOptions; i < list.length; i += 1) {
            var option = list[i];

            var val = this$1.$container.data('crt-'+option);
            if (val) {
                this$1.options[option] = val;
            }
        }

        if (this.options.debug) {
            _core_logger__WEBPACK_IMPORTED_MODULE_2__["default"].debug = true;
        }

        this.updateResponsiveOptions ();

        _core_logger__WEBPACK_IMPORTED_MODULE_2__["default"].log ('Setting language to: '+this.options.lang);
        _core_translate__WEBPACK_IMPORTED_MODULE_11__["default"].setLang(this.options.lang);

        this.checkPoweredBy ();
        this.createFeed();
        this.createFilter();
        this.createPopupManager();

        return true;
    };

    Widget.prototype.updateResponsiveOptions = function updateResponsiveOptions () {
        // console.log('updateResponsiveOptions');
        if (!this.options.responsive) {
            this.responsiveOptions = _core_lib__WEBPACK_IMPORTED_MODULE_10__["default"].extend(true, {}, this.options);
            return;
        }

        var width = Object(_core_lib__WEBPACK_IMPORTED_MODULE_10__["default"])(window).width();
        var keys = Object.keys(this.options.responsive);
        keys = keys.map(function (x) { return parseInt(x); });
        keys = keys.sort(function (a, b) {
            return a - b;
        });
        keys = keys.reverse();

        var foundKey = null;
        for (var i = 0, list = keys; i < list.length; i += 1) {
            var key = list[i];

            if (width <= key) {
                foundKey = key;
            }
        }
        if (!foundKey) {
            this.responsiveKey = null;
            this.responsiveOptions = _core_lib__WEBPACK_IMPORTED_MODULE_10__["default"].extend(true, {}, this.options);
        }

        if (this.responsiveKey !== foundKey) {
            // console.log('CHANGING RESPONSIVE SETTINGS '+foundKey);
            this.responsiveKey = foundKey;
            this.responsiveOptions = _core_lib__WEBPACK_IMPORTED_MODULE_10__["default"].extend(true, {}, this.options, this.options.responsive[foundKey]);
        }
    };

    Widget.prototype.createFeed = function createFeed () {
        this.feed = new _core_feed__WEBPACK_IMPORTED_MODULE_4__["default"] (this);
        this.feed.on(_core_events__WEBPACK_IMPORTED_MODULE_6__["default"].POSTS_LOADED, this.onPostsLoaded.bind(this));
        this.feed.on(_core_events__WEBPACK_IMPORTED_MODULE_6__["default"].POSTS_FAILED, this.onPostsFail.bind(this));
        this.feed.on(_core_events__WEBPACK_IMPORTED_MODULE_6__["default"].FEED_LOADED, this.onFeedLoaded.bind(this));
    };

    Widget.prototype.createPopupManager = function createPopupManager () {
        this.popupManager = new _ui_popup_manager__WEBPACK_IMPORTED_MODULE_9__["default"](this);
    };

    Widget.prototype.createFilter = function createFilter () {
        _core_logger__WEBPACK_IMPORTED_MODULE_2__["default"].log('Widget->createFilter');
        _core_logger__WEBPACK_IMPORTED_MODULE_2__["default"].log(this.options.filter);

        if (this.options.filter && (this.options.filter.showNetworks || this.options.filter.showSources)) {

            this.filter = new _ui_filter__WEBPACK_IMPORTED_MODULE_8__["default"](this);
        }
    };

    Widget.prototype.loadPosts = function loadPosts (page) {
        this.feed.loadPosts(page);
    };

    Widget.prototype.createPostElements = function createPostElements (posts)
    {
        var that = this;
        var postElements = [];
        Object(_core_lib__WEBPACK_IMPORTED_MODULE_10__["default"])(posts).each(function(){
            var p = that.createPostElement(this);
            postElements.push(p.$el);
        });
        return postElements;
    };

    Widget.prototype.createPostElement = function createPostElement (postJson) {
        var post = new _ui_post__WEBPACK_IMPORTED_MODULE_7__["default"](postJson, this.options, this);
        post.on(_core_events__WEBPACK_IMPORTED_MODULE_6__["default"].POST_CLICK,this.onPostClick.bind(this));
        post.on(_core_events__WEBPACK_IMPORTED_MODULE_6__["default"].POST_CLICK_READ_MORE,this.onPostClickReadMore.bind(this));
        post.on(_core_events__WEBPACK_IMPORTED_MODULE_6__["default"].POST_IMAGE_LOADED, this.onPostImageLoaded.bind(this));

        this.trigger(_core_events__WEBPACK_IMPORTED_MODULE_6__["default"].POST_CREATED, post);

        return post;
    };

    Widget.prototype.onPostsLoaded = function onPostsLoaded (event, posts) {
        _core_logger__WEBPACK_IMPORTED_MODULE_2__["default"].log('Widget->onPostsLoaded');
        _core_logger__WEBPACK_IMPORTED_MODULE_2__["default"].log(event);
        _core_logger__WEBPACK_IMPORTED_MODULE_2__["default"].log(posts);
    };

    Widget.prototype.onPostsFail = function onPostsFail (event, data) {
        _core_logger__WEBPACK_IMPORTED_MODULE_2__["default"].log('Widget->onPostsLoadedFail');
        _core_logger__WEBPACK_IMPORTED_MODULE_2__["default"].log(event);
        _core_logger__WEBPACK_IMPORTED_MODULE_2__["default"].log(data);
    };

    Widget.prototype.onPostClick = function onPostClick (ev, post) {
        _core_logger__WEBPACK_IMPORTED_MODULE_2__["default"].log('Widget->onPostClick');
        _core_logger__WEBPACK_IMPORTED_MODULE_2__["default"].log(ev);
        _core_logger__WEBPACK_IMPORTED_MODULE_2__["default"].log(post);

        this.trigger(_core_events__WEBPACK_IMPORTED_MODULE_6__["default"].POST_CLICK, post);

        if (this.options.postClickAction === _core_globals__WEBPACK_IMPORTED_MODULE_12__["default"].POST_CLICK_ACTION_OPEN_POPUP) {
            this.popupManager.showPopup(post.json);
        } else if (this.options.postClickAction === _core_globals__WEBPACK_IMPORTED_MODULE_12__["default"].POST_CLICK_ACTION_GOTO_SOURCE) {
            window.open(post.json.url);
        }
    };

    Widget.prototype.onPostClickReadMore = function onPostClickReadMore (ev, post) {
        _core_logger__WEBPACK_IMPORTED_MODULE_2__["default"].log('Widget->onPostClickReadMore');
        _core_logger__WEBPACK_IMPORTED_MODULE_2__["default"].log(ev);
        _core_logger__WEBPACK_IMPORTED_MODULE_2__["default"].log(post);

        this.trigger(_core_events__WEBPACK_IMPORTED_MODULE_6__["default"].POST_CLICK_READ_MORE, post);

        if (this.options.postClickReadMoreAction === _core_globals__WEBPACK_IMPORTED_MODULE_12__["default"].POST_CLICK_ACTION_OPEN_POPUP) {
            this.popupManager.showPopup(post.json);
        } else if (this.options.postClickAction === _core_globals__WEBPACK_IMPORTED_MODULE_12__["default"].POST_CLICK_ACTION_GOTO_SOURCE) {
            window.open(post.json.url);
        }
    };

    Widget.prototype.onPostImageLoaded = function onPostImageLoaded (ev, post) {
        // Logger.log('Widget->onPostImageLoaded');
        // Logger.log(event);
        // Logger.log(post);
    };

    Widget.prototype.onFeedLoaded = function onFeedLoaded (ev, response) {
        if (this.options.hidePoweredBy && response.account.plan.unbranded === 1) {
            //<a href="http://curator.io" target="_blank" class="crt-logo crt-tag">Powered by Curator.io</a>
            this.$container.addClass('crt-feed-unbranded');
        } else {
            this.$container.addClass('crt-feed-branded');
        }
    };

    Widget.prototype.track = function track (a) {
        _core_logger__WEBPACK_IMPORTED_MODULE_2__["default"].log('Feed->track '+a);

        _core_ajax__WEBPACK_IMPORTED_MODULE_5__["default"].get (
            this.getUrl('/track/'+this.options.feedId),
            {a:a},
            function (data) {
                _core_logger__WEBPACK_IMPORTED_MODULE_2__["default"].log('Feed->track success');
                _core_logger__WEBPACK_IMPORTED_MODULE_2__["default"].log(data);
            },
            function (jqXHR, textStatus, errorThrown) {
                _core_logger__WEBPACK_IMPORTED_MODULE_2__["default"].log('Feed->_loadPosts fail');
                _core_logger__WEBPACK_IMPORTED_MODULE_2__["default"].log(textStatus);
                _core_logger__WEBPACK_IMPORTED_MODULE_2__["default"].log(errorThrown);
            }
        );
    };

    Widget.prototype.getUrl = function getUrl (trail) {
        return this.options.apiEndpoint+trail;
    };

    Widget.prototype._t = function _t (s) {
        return _core_translate__WEBPACK_IMPORTED_MODULE_11__["default"].t (s);
    };

    Widget.prototype.checkPoweredBy = function checkPoweredBy () {
        var html = this.$container.text().trim();

        this.hasPoweredBy = html.indexOf('Powered by Curator.io') > -1;
    };

    Widget.prototype.destroy = function destroy () {
        _core_logger__WEBPACK_IMPORTED_MODULE_2__["default"].log('Widget->destroy');

        EventBus.prototype.destroy.call(this);

        if (this.feed) {
            this.feed.destroy();
        }
        if (this.filter) {
            this.filter.destroy();
        }
        if (this.popupManager) {
            this.popupManager.destroy();
        }
        this.$container.removeClass('crt-feed');
        this.$container.removeClass('crt-feed-unbranded');
        this.$container.removeClass('crt-feed-branded');
    };

    return Widget;
}(_core_bus__WEBPACK_IMPORTED_MODULE_0__["default"]));

/* harmony default export */ __webpack_exports__["default"] = (Widget);

/***/ }),

/***/ "./js/app/widget/list-feed-template.js":
/*!*********************************************!*\
  !*** ./js/app/widget/list-feed-template.js ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);


var template = "\n<div class=\"crt-feed-window\">\n    <div id=\"owl-feed\" class=\"owl-carousel owl-theme crt-feed\"></div>\n</div>\n<div class=\"crt-load-more\"><a href=\"#\"><%=this._t(\"load-more\")%></a></div>";

/* harmony default export */ __webpack_exports__["default"] = (template);

/***/ }),

/***/ "./js/app/widget/list-post-template.js":
/*!*********************************************!*\
  !*** ./js/app/widget/list-post-template.js ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);


var template = "\n<div class=\"item crt-list-post crt-post-<%=id%> <%=this.contentImageClasses()%> <%=this.contentTextClasses()%>\" data-post=\"<%=id%>\">     <div class=\"crt-post-c\"> \n        <div class=\"crt-post-content\"> \n            <div class=\"crt-list-post-image\">\n                <div>\n                <img class=\"crt-post-content-image\" src=\"<%=image%>\" alt=\"\" /> \n                <a href=\"javascript:;\" class=\"crt-play\"><i class=\"crt-play-icon\"></i></a> \n                <span class=\"crt-social-icon crt-social-icon-normal\"><i class=\"crt-icon-<%=this.networkIcon()%>\"></i></span> \n                <span class=\"crt-image-carousel\"><i class=\"crt-icon-image-carousel\"></i></span>\n                </div> \n            </div>\n            <div class=\"crt-list-post-text\">\n                <div class=\"crt-list-post-text-wrap\"> \n                    <div><%=this.parseText(text)%></div> \n                </div> \n                 <div class=\"crt-post-footer\">\n                    <span class=\"crt-date\"><%=this.prettyDate(source_created_at)%></span> \n                    <a href=\"#\" target=\"_blank\">\n                        View More\n                        <svg xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 47.58 47.58\" class=\"sprite arrow\"><defs><style>.cls-1{fill:none;}</style></defs><title>arrow-animate</title><g id=\"containter\"><rect class=\"cls-1\" width=\"47.58\" height=\"47.58\"></rect></g><g id=\"sideways\"><path id=\"sideways-2\" data-name=\"sideways\" class=\"cls-2\" d=\"M441,269l-12.64-12.64,2.08-2,16.18,16.18-16.18,16.17-2.13-2L441,272H409.11v-3Z\" transform=\"translate(-409.11 -246.72)\"></path></g><g id=\"sideways-under\"><rect id=\"downline-3\" data-name=\"downline\" class=\"cls-2\" y=\"22.28\" width=\"25.22\" height=\"3.02\"></rect></g></svg>\n                    </a>\n                </div>  \n            </div>\n        </div> \n    </div>\n</div>";

/* harmony default export */ __webpack_exports__["default"] = (template);

/***/ }),

/***/ "./js/app/widget/list.js":
/*!*******************************!*\
  !*** ./js/app/widget/list.js ***!
  \*******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _curator_widgets_base__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./curator/widgets/base */ "./js/app/widget/curator/widgets/base.js");
/* harmony import */ var _curator_core_logger__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./curator/core/logger */ "./js/app/widget/curator/core/logger.js");
/* harmony import */ var _curator_config_widget_list__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./curator/config/widget_list */ "./js/app/widget/curator/config/widget_list.js");
/* harmony import */ var _curator_core_templating__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./curator/core/templating */ "./js/app/widget/curator/core/templating.js");
/* harmony import */ var _curator_core_lib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./curator/core/lib */ "./js/app/widget/curator/core/lib.js");
/* harmony import */ var _curator_core_events__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./curator/core/events */ "./js/app/widget/curator/core/events.js");
/* harmony import */ var _list_feed_template__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./list-feed-template */ "./js/app/widget/list-feed-template.js");









var List = (function (Widget) {
    function List  (options) {
        Widget.call (this);

        this.loading=false;
        this.feed=null;
        this.$container=null;
        this.$feed=null;
        this.posts=[];

        if (this.init (options,  _curator_config_widget_list__WEBPACK_IMPORTED_MODULE_2__["default"])) {
            _curator_core_logger__WEBPACK_IMPORTED_MODULE_1__["default"].log("List->init with options:");
            _curator_core_logger__WEBPACK_IMPORTED_MODULE_1__["default"].log(this.options);

            var tmpl = _curator_core_templating__WEBPACK_IMPORTED_MODULE_3__["default"].renderDiv(_list_feed_template__WEBPACK_IMPORTED_MODULE_6__["default"], {});
            this.$container.append(tmpl);
            this.$feed = this.$container.find('.crt-feed');
            this.$feedWindow = this.$container.find('.crt-feed-window');
            this.$loadMore = this.$container.find('.crt-load-more a');
            this.$scroller = Object(_curator_core_lib__WEBPACK_IMPORTED_MODULE_4__["default"])(window);

            this.$container.addClass('crt-list');
            this.$container.addClass('crt-widget-list');

            // This triggers post loading
            this.feed.load();
        }
    }

    if ( Widget ) List.__proto__ = Widget;
    List.prototype = Object.create( Widget && Widget.prototype );
    List.prototype.constructor = List;


    List.prototype.loadPosts = function loadPosts () {
        // console.log ('LOAD POSTS CALLED!!!?!?!!?!?!');
    };


    List.prototype.onPostsLoaded = function onPostsLoaded (event, posts) {
        var this$1 = this;

        _curator_core_logger__WEBPACK_IMPORTED_MODULE_1__["default"].log("List->onPostsLoaded");

        this.loading = false;

        if (posts.length !== 0) {
            this.postElements = [];
            var i = 0;

            for (var i$1 = 0, list = posts; i$1 < list.length; i$1 += 1) {
                var postJson = list[i$1];

                var post = this$1.createPostElement(postJson);
                this$1.postElements.push(post);
                this$1.$feed.append(post.$el);
                post.layout();
            }
        }

        this.trigger(_curator_core_events__WEBPACK_IMPORTED_MODULE_5__["default"].POSTS_RENDERED, this.postElements);
    };

    List.prototype.destroy = function destroy () {
        Widget.prototype.destroy.call(this);

        this.feed.destroy();

        this.$container.empty()
            .removeClass('crt-list')
            .removeClass('crt-widget-list')
            .removeClass('crt-list-col'+this.columnCount)
            .css({'height':'','overflow':''});

        delete this.$feed;
        delete this.$container;
        delete this.options ;
        delete this.loading;

        // TODO add code to cascade destroy down to Posts
        // unregistering events etc
        delete this.feed;
    };

    return List;
}(_curator_widgets_base__WEBPACK_IMPORTED_MODULE_0__["default"]));

/* harmony default export */ __webpack_exports__["default"] = (List);


/***/ }),

/***/ "./js/vendor/jquery.fullPage.min.js":
/*!******************************************!*\
  !*** ./js/vendor/jquery.fullPage.min.js ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;/*!
 * fullPage 2.9.7
 * https://github.com/alvarotrigo/fullPage.js
 * @license MIT licensed
 *
 * Copyright (C) 2015 alvarotrigo.com - A project by Alvaro Trigo
 */
!function(e,o){"use strict"; true?!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")], __WEBPACK_AMD_DEFINE_RESULT__ = (function(n){return o(n,e,e.document,e.Math)}).apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__)):undefined}("undefined"!=typeof window?window:this,function(e,o,n,t,i){"use strict";var a="fullpage-wrapper",l="."+a,s="fp-responsive",r="fp-notransition",c="fp-destroyed",d="fp-enabled",f="fp-viewing",u="active",h="."+u,v="fp-completely",p="."+v,g="fp-section",m="."+g,w=m+h,S=m+":first",b=m+":last",x="fp-tableCell",y="."+x,C="fp-auto-height",T="fp-normal-scroll",k="fp-nav",L="#"+k,A="fp-tooltip",O="."+A,I="fp-show-active",E="fp-slide",M="."+E,B=M+h,R="fp-slides",z="."+R,H="fp-slidesContainer",D="."+H,P="fp-table",q="fp-slidesNav",F="."+q,V=F+" a",j="fp-controlArrow",Y="."+j,N="fp-prev",X=j+" "+N,U=Y+("."+N),W="fp-next",K=j+" "+W,_=Y+".fp-next",Q=e(o),G=e(n);e.fn.fullpage=function(j){if(e("html").hasClass(d)){ $o(); }else{var W=e("html, body"),J=e("body"),Z=e.fn.fullpage;j=e.extend({menu:!1,anchors:[],lockAnchors:!1,navigation:!1,navigationPosition:"right",navigationTooltips:[],showActiveTooltip:!1,slidesNavigation:!1,slidesNavPosition:"bottom",scrollBar:!1,hybrid:!1,css3:!0,scrollingSpeed:700,autoScrolling:!0,fitToSection:!0,fitToSectionDelay:1e3,easing:"easeInOutCubic",easingcss3:"ease",loopBottom:!1,loopTop:!1,loopHorizontal:!0,continuousVertical:!1,continuousHorizontal:!1,scrollHorizontally:!1,interlockedSlides:!1,dragAndMove:!1,offsetSections:!1,resetSliders:!1,fadingEffect:!1,normalScrollElements:null,scrollOverflow:!1,scrollOverflowReset:!1,scrollOverflowHandler:e.fn.fp_scrolloverflow?e.fn.fp_scrolloverflow.iscrollHandler:null,scrollOverflowOptions:null,touchSensitivity:5,normalScrollElementTouchThreshold:5,bigSectionsDestination:null,keyboardScrolling:!0,animateAnchor:!0,recordHistory:!0,controlArrows:!0,controlArrowColor:"#fff",verticalCentered:!0,sectionsColor:[],paddingTop:0,paddingBottom:0,fixedElements:null,responsive:0,responsiveWidth:0,responsiveHeight:0,responsiveSlides:!1,parallax:!1,parallaxOptions:{type:"reveal",percentage:62,property:"translate"},sectionSelector:".section",slideSelector:".slide",afterLoad:null,onLeave:null,afterRender:null,afterResize:null,afterReBuild:null,afterSlideLoad:null,onSlideLeave:null,afterResponsive:null,lazyLoading:!0},j);var $,ee,oe,ne,te=!1,ie=navigator.userAgent.match(/(iPhone|iPod|iPad|Android|playbook|silk|BlackBerry|BB10|Windows Phone|Tizen|Bada|webOS|IEMobile|Opera Mini)/),ae="ontouchstart"in o||navigator.msMaxTouchPoints>0||navigator.maxTouchPoints,le=e(this),se=Q.height(),re=!1,ce=!0,de=!0,fe=[],ue={m:{up:!0,down:!0,left:!0,right:!0}};ue.k=e.extend(!0,{},ue.m);var he,ve,pe,ge,me,we,Se,be=function(){var e;e=o.PointerEvent?{down:"pointerdown",move:"pointermove"}:{down:"MSPointerDown",move:"MSPointerMove"};return e}(),xe={touchmove:"ontouchmove"in o?"touchmove":be.move,touchstart:"ontouchstart"in o?"touchstart":be.down},ye='a[href], area[href], input:not([disabled]), select:not([disabled]), textarea:not([disabled]), button:not([disabled]), iframe, object, embed, [tabindex="0"], [contenteditable]',Ce=e.extend(!0,{},j);$o(),e.extend(e.easing,{easeInOutCubic:function(e,o,n,t,i){return(o/=i/2)<1?t/2*o*o*o+n:t/2*((o-=2)*o*o+2)+n}}),e(this).length&&(Z.version="2.9.6",Z.setAutoScrolling=ze,Z.setRecordHistory=He,Z.setScrollingSpeed=De,Z.setFitToSection=Pe,Z.setLockAnchors=function(e){j.lockAnchors=e},Z.setMouseWheelScrolling=qe,Z.setAllowScrolling=Fe,Z.setKeyboardScrolling=Ve,Z.moveSectionUp=je,Z.moveSectionDown=Ye,Z.silentMoveTo=Ne,Z.moveTo=Xe,Z.moveSlideRight=Ue,Z.moveSlideLeft=We,Z.fitToSection=Je,Z.reBuild=Ke,Z.setResponsive=_e,Z.destroy=function(o){ze(!1,"internal"),Fe(!1),Ve(!1),le.addClass(c),clearTimeout(ge),clearTimeout(pe),clearTimeout(ve),clearTimeout(me),clearTimeout(we),Q.off("scroll",Ge).off("hashchange",mo).off("resize",Mo),G.off("keydown",So).off("keyup",xo).off("click touchstart",L+" a").off("mouseenter",L+" li").off("mouseleave",L+" li").off("click touchstart",V).off("mouseover",j.normalScrollElements).off("mouseout",j.normalScrollElements),e(m).off("click touchstart",Y),clearTimeout(ge),clearTimeout(pe),o&&function(){Qo(0),le.find("img[data-src], source[data-src], audio[data-src], iframe[data-src]").each(function(){co(e(this),"src")}),le.find("img[data-srcset]").each(function(){co(e(this),"srcset")}),e(L+", "+F+", "+Y).remove(),e(m).css({height:"","background-color":"",padding:""}),e(M).css({width:""}),le.css({height:"",position:"","-ms-touch-action":"","touch-action":""}),W.css({overflow:"",height:""}),e("html").removeClass(d),J.removeClass(s),e.each(J.get(0).className.split(/\s+/),function(e,o){0===o.indexOf(f)&&J.removeClass(o)}),e(m+", "+M).each(function(){j.scrollOverflowHandler&&j.scrollOverflowHandler.remove(e(this)),e(this).removeClass(P+" "+u),e(this).attr("style",e(this).data("fp-styles"))}),zo(le),le.find(y+", "+D+", "+z).each(function(){e(this).replaceWith(this.childNodes)}),le.css({"-webkit-transition":"none",transition:"none"}),W.scrollTop(0);var o=[g,E,H];e.each(o,function(o,n){e("."+n).removeClass(n)})}()},Z.shared={afterRenderActions:Qe},function(){j.css3&&(j.css3=function(){var e,t=n.createElement("p"),a={webkitTransform:"-webkit-transform",OTransform:"-o-transform",msTransform:"-ms-transform",MozTransform:"-moz-transform",transform:"transform"};for(var l in n.body.insertBefore(t,null),a){ t.style[l]!==i&&(t.style[l]="translate3d(1px,1px,1px)",e=o.getComputedStyle(t).getPropertyValue(a[l])); }return n.body.removeChild(t),e!==i&&e.length>0&&"none"!==e}());j.scrollBar=j.scrollBar||j.hybrid,t=le.find(j.sectionSelector),j.anchors.length||(j.anchors=t.filter("[data-anchor]").map(function(){return e(this).data("anchor").toString()}).get()),j.navigationTooltips.length||(j.navigationTooltips=t.filter("[data-tooltip]").map(function(){return e(this).data("tooltip").toString()}).get()),le.css({height:"100%",position:"relative"}),le.addClass(a),e("html").addClass(d),se=Q.height(),le.removeClass(c),le.find(j.sectionSelector).addClass(g),le.find(j.slideSelector).addClass(E),e(m).each(function(o){var n,t,i,a,s=e(this),r=s.find(M),c=r.length;s.data("fp-styles",s.attr("style")),i=s,(a=o)||0!==e(w).length||i.addClass(u),ne=e(w),i.css("height",se+"px"),j.paddingTop&&i.css("padding-top",j.paddingTop),j.paddingBottom&&i.css("padding-bottom",j.paddingBottom),void 0!==j.sectionsColor[a]&&i.css("background-color",j.sectionsColor[a]),void 0!==j.anchors[a]&&i.attr("data-anchor",j.anchors[a]),n=s,t=o,void 0!==j.anchors[t]&&n.hasClass(u)&&Ho(j.anchors[t],t),j.menu&&j.css3&&e(j.menu).closest(l).length&&e(j.menu).appendTo(J),c>0?function(o,n,t){var i,a=100*t,l=100/t;n.wrapAll('<div class="'+H+'" />'),n.parent().wrap('<div class="'+R+'" />'),o.find(D).css("width",a+"%"),t>1&&(j.controlArrows&&((i=o).find(z).after('<div class="'+X+'"></div><div class="'+K+'"></div>'),"#fff"!=j.controlArrowColor&&(i.find(_).css("border-color","transparent transparent transparent "+j.controlArrowColor),i.find(U).css("border-color","transparent "+j.controlArrowColor+" transparent transparent")),j.loopHorizontal||i.find(U).hide()),j.slidesNavigation&&function(e,o){e.append('<div class="'+q+'"><ul></ul></div>');var n=e.find(F);n.addClass(j.slidesNavPosition);for(var t=0;t<o;t++){ n.find("ul").append('<li><a href="#"><span></span></a></li>'); }n.css("margin-left","-"+n.width()/2+"px"),n.find("li").first().find("a").addClass(u)}(o,t)),n.each(function(o){e(this).css("width",l+"%"),j.verticalCentered&&Po(e(this))});var s=o.find(B);s.length&&(0!==e(w).index(m)||0===e(w).index(m)&&0!==s.index())?_o(s,"internal"):n.eq(0).addClass(u)}(s,r,c):j.verticalCentered&&Po(s)}),j.fixedElements&&j.css3&&e(j.fixedElements).appendTo(J),j.navigation&&function(){J.append('<div id="'+k+'"><ul></ul></div>');var o=e(L);o.addClass(function(){return j.showActiveTooltip?I+" "+j.navigationPosition:j.navigationPosition});for(var n=0;n<e(m).length;n++){var t="";j.anchors.length&&(t=j.anchors[n]);var i='<li><a href="#'+t+'"><span></span></a>',a=j.navigationTooltips[n];void 0!==a&&""!==a&&(i+='<div class="'+A+" "+j.navigationPosition+'">'+a+"</div>"),i+="</li>",o.find("ul").append(i)}e(L).css("margin-top","-"+e(L).height()/2+"px"),e(L).find("li").eq(e(w).index(m)).find("a").addClass(u)}(),le.find('iframe[src*="youtube.com/embed/"]').each(function(){var o,n,t;o=e(this),n="enablejsapi=1",t=o.attr("src"),o.attr("src",t+(/\?/.test(t)?"&":"?")+n)}),j.scrollOverflow?he=j.scrollOverflowHandler.init(j):Qe(),Fe(!0),ze(j.autoScrolling,"internal"),Bo(),Wo(),"complete"===n.readyState&&go();var t;Q.on("load",go)}(),Q.on("scroll",Ge).on("hashchange",mo).blur(ko).resize(Mo),G.keydown(So).keyup(xo).on("click touchstart",L+" a",Lo).on("click touchstart",V,Ao).on("click",O,bo),e(m).on("click touchstart",Y,To),j.normalScrollElements&&(G.on("mouseenter touchstart",j.normalScrollElements,function(){Fe(!1)}),G.on("mouseleave touchend",j.normalScrollElements,function(){Fe(!0)})));var Te=!1,ke=0,Le=0,Ae=0,Oe=0,Ie=0,Ee=(new Date).getTime(),Me=0,Be=0,Re=se}function ze(o,n){o||Qo(0),Zo("autoScrolling",o,n);var t=e(w);j.autoScrolling&&!j.scrollBar?(W.css({overflow:"hidden",height:"100%"}),He(Ce.recordHistory,"internal"),le.css({"-ms-touch-action":"none","touch-action":"none"}),t.length&&Qo(t.position().top)):(W.css({overflow:"visible",height:"initial"}),He(!1,"internal"),le.css({"-ms-touch-action":"","touch-action":""}),t.length&&W.scrollTop(t.position().top))}function He(e,o){Zo("recordHistory",e,o)}function De(e,o){Zo("scrollingSpeed",e,o)}function Pe(e,o){Zo("fitToSection",e,o)}function qe(e){e?(!function(){var e,t="";o.addEventListener?e="addEventListener":(e="attachEvent",t="on");var a="onwheel"in n.createElement("div")?"wheel":n.onmousewheel!==i?"mousewheel":"DOMMouseScroll";"DOMMouseScroll"==a?n[e](t+"MozMousePixelScroll",io,!1):n[e](t+a,io,!1)}(),le.on("mousedown",yo).on("mouseup",Co)):(n.addEventListener?(n.removeEventListener("mousewheel",io,!1),n.removeEventListener("wheel",io,!1),n.removeEventListener("MozMousePixelScroll",io,!1)):n.detachEvent("onmousewheel",io),le.off("mousedown",yo).off("mouseup",Co))}function Fe(o,n){void 0!==n?(n=n.replace(/ /g,"").split(","),e.each(n,function(e,n){Jo(o,n,"m")})):(Jo(o,"all","m"),o?(qe(!0),(ie||ae)&&(j.autoScrolling&&J.off(xe.touchmove).on(xe.touchmove,$e),e(l).off(xe.touchstart).on(xe.touchstart,no).off(xe.touchmove).on(xe.touchmove,eo))):(qe(!1),(ie||ae)&&(j.autoScrolling&&J.off(xe.touchmove),e(l).off(xe.touchstart).off(xe.touchmove))))}function Ve(o,n){void 0!==n?(n=n.replace(/ /g,"").split(","),e.each(n,function(e,n){Jo(o,n,"k")})):(Jo(o,"all","k"),j.keyboardScrolling=o)}function je(){var o=e(w).prev(m);o.length||!j.loopTop&&!j.continuousVertical||(o=e(m).last()),o.length&&so(o,null,!0)}function Ye(){var o=e(w).next(m);o.length||!j.loopBottom&&!j.continuousVertical||(o=e(m).first()),o.length&&so(o,null,!1)}function Ne(e,o){De(0,"internal"),Xe(e,o),De(Ce.scrollingSpeed,"internal")}function Xe(e,o){var n=Vo(e);void 0!==o?jo(e,o):n.length>0&&so(n)}function Ue(e){ao("right",e)}function We(e){ao("left",e)}function Ke(o){if(!le.hasClass(c)){re=!0,se=Q.height(),e(m).each(function(){var o=e(this).find(z),n=e(this).find(M);j.verticalCentered&&e(this).find(y).css("height",qo(e(this))+"px"),e(this).css("height",se+"px"),n.length>1&&Io(o,o.find(B))}),j.scrollOverflow&&he.createScrollBarForAll();var n=e(w).index(m);n&&Ne(n+1),re=!1,e.isFunction(j.afterResize)&&o&&j.afterResize.call(le),e.isFunction(j.afterReBuild)&&!o&&j.afterReBuild.call(le)}}function _e(o){var n=J.hasClass(s);o?n||(ze(!1,"internal"),Pe(!1,"internal"),e(L).hide(),J.addClass(s),e.isFunction(j.afterResponsive)&&j.afterResponsive.call(le,o)):n&&(ze(Ce.autoScrolling,"internal"),Pe(Ce.autoScrolling,"internal"),e(L).show(),J.removeClass(s),e.isFunction(j.afterResponsive)&&j.afterResponsive.call(le,o))}function Qe(){var o,n=e(w);n.addClass(v),fo(n),uo(n),j.scrollOverflow&&j.scrollOverflowHandler.afterLoad(),(!(o=Vo(wo().section))||o.length&&o.index()===ne.index())&&e.isFunction(j.afterLoad)&&j.afterLoad.call(n,n.data("anchor"),n.index(m)+1),e.isFunction(j.afterRender)&&j.afterRender.call(le)}function Ge(){var o,t,i;if(!j.autoScrolling||j.scrollBar){var a=Q.scrollTop(),l=(i=(t=a)>ke?"down":"up",ke=t,Me=t,i),s=0,r=a+Q.height()/2,c=J.height()-Q.height()===a,d=n.querySelectorAll(m);if(c){ s=d.length-1; }else if(a){ for(var f=0;f<d.length;++f){d[f].offsetTop<=r&&(s=f)} }else { s=0; }if(function(o){var n=e(w).position().top,t=n+Q.height();if("up"==o){ return t>=Q.scrollTop()+Q.height(); }return n<=Q.scrollTop()}(l)&&(e(w).hasClass(v)||e(w).addClass(v).siblings().removeClass(v)),!(o=e(d).eq(s)).hasClass(u)){Te=!0;var h,p,g=e(w),S=g.index(m)+1,b=Do(o),x=o.data("anchor"),y=o.index(m)+1,C=o.find(B);C.length&&(p=C.data("anchor"),h=C.index()),de&&(o.addClass(u).siblings().removeClass(u),e.isFunction(j.onLeave)&&j.onLeave.call(g,S,y,b),e.isFunction(j.afterLoad)&&j.afterLoad.call(o,x,y),vo(g),fo(o),uo(o),Ho(x,y-1),j.anchors.length&&($=x),No(h,p,x,y)),clearTimeout(me),me=setTimeout(function(){Te=!1},100)}j.fitToSection&&(clearTimeout(we),we=setTimeout(function(){j.fitToSection&&e(w).outerHeight()<=se&&Je()},j.fitToSectionDelay))}}function Je(){de&&(re=!0,so(e(w)),re=!1)}function Ze(o){if(ue.m[o]){var n="down"===o?Ye:je;if(j.scrollOverflow){var t=j.scrollOverflowHandler.scrollable(e(w)),i="down"===o?"bottom":"top";if(t.length>0){if(!j.scrollOverflowHandler.isScrolled(i,t)){ return!0; }n()}else { n() }}else { n() }}}function $e(e){var o=e.originalEvent;j.autoScrolling&&oo(o)&&e.preventDefault()}function eo(o){var n=o.originalEvent,i=e(n.target).closest(m);if(oo(n)){j.autoScrolling&&o.preventDefault();var a=Ko(n);Oe=a.y,Ie=a.x,i.find(z).length&&t.abs(Ae-Ie)>t.abs(Le-Oe)?!te&&t.abs(Ae-Ie)>Q.outerWidth()/100*j.touchSensitivity&&(Ae>Ie?ue.m.right&&Ue(i):ue.m.left&&We(i)):j.autoScrolling&&de&&t.abs(Le-Oe)>Q.height()/100*j.touchSensitivity&&(Le>Oe?Ze("down"):Oe>Le&&Ze("up"))}}function oo(e){return void 0===e.pointerType||"mouse"!=e.pointerType}function no(e){var o=e.originalEvent;if(j.fitToSection&&W.stop(),oo(o)){var n=Ko(o);Le=n.y,Ae=n.x}}function to(e,o){for(var n=0,i=e.slice(t.max(e.length-o,1)),a=0;a<i.length;a++){ n+=i[a]; }return t.ceil(n/o)}function io(n){var i=(new Date).getTime(),a=e(p).hasClass(T);if(j.autoScrolling&&!oe&&!a){var l=(n=n||o.event).wheelDelta||-n.deltaY||-n.detail,s=t.max(-1,t.min(1,l)),r=void 0!==n.wheelDeltaX||void 0!==n.deltaX,c=t.abs(n.wheelDeltaX)<t.abs(n.wheelDelta)||t.abs(n.deltaX)<t.abs(n.deltaY)||!r;fe.length>149&&fe.shift(),fe.push(t.abs(l)),j.scrollBar&&(n.preventDefault?n.preventDefault():n.returnValue=!1);var d=i-Ee;if(Ee=i,d>200&&(fe=[]),de){ to(fe,10)>=to(fe,70)&&c&&Ze(s<0?"down":"up"); }return!1}j.fitToSection&&W.stop()}function ao(o,n){var t=(void 0===n?e(w):n).find(z),i=t.find(M).length;if(!(!t.length||te||i<2)){var a=t.find(B),l=null;if(!(l="left"===o?a.prev(M):a.next(M)).length){if(!j.loopHorizontal){ return; }l="left"===o?a.siblings(":last"):a.siblings(":first")}te=!0,Io(t,l,o)}}function lo(){e(B).each(function(){_o(e(this),"internal")})}function so(o,n,i){if(void 0!==o){var a,s,r,c,d,f,h,v,p={element:o,callback:n,isMovementUp:i,dtop:(s=(a=o).position(),r=s.top,c=s.top>Me,d=r-se+a.outerHeight(),f=j.bigSectionsDestination,a.outerHeight()>se?(c||f)&&"bottom"!==f||(r=d):(c||re&&a.is(":last-child"))&&(r=d),Me=r,r),yMovement:Do(o),anchorLink:o.data("anchor"),sectionIndex:o.index(m),activeSlide:o.find(B),activeSection:e(w),leavingSection:e(w).index(m)+1,localIsResizing:re};if(!(p.activeSection.is(o)&&!re||j.scrollBar&&Q.scrollTop()===p.dtop&&!o.hasClass(C))){if(p.activeSlide.length&&(h=p.activeSlide.data("anchor"),v=p.activeSlide.index()),e.isFunction(j.onLeave)&&!p.localIsResizing){var g=p.yMovement;if(void 0!==i&&(g=i?"up":"down"),!1===j.onLeave.call(p.activeSection,p.leavingSection,p.sectionIndex+1,g)){ return }}j.autoScrolling&&j.continuousVertical&&void 0!==p.isMovementUp&&(!p.isMovementUp&&"up"==p.yMovement||p.isMovementUp&&"down"==p.yMovement)&&(p=function(o){o.isMovementUp?e(w).before(o.activeSection.nextAll(m)):e(w).after(o.activeSection.prevAll(m).get().reverse());return Qo(e(w).position().top),lo(),o.wrapAroundElements=o.activeSection,o.dtop=o.element.position().top,o.yMovement=Do(o.element),o.leavingSection=o.activeSection.index(m)+1,o.sectionIndex=o.element.index(m),o}(p)),p.localIsResizing||vo(p.activeSection),j.scrollOverflow&&j.scrollOverflowHandler.beforeLeave(),o.addClass(u).siblings().removeClass(u),fo(o),j.scrollOverflow&&j.scrollOverflowHandler.onLeave(),de=!1,No(v,h,p.anchorLink,p.sectionIndex),function(o){if(j.css3&&j.autoScrolling&&!j.scrollBar){var n="translate3d(0px, -"+t.round(o.dtop)+"px, 0px)";Fo(n,!0),j.scrollingSpeed?(clearTimeout(pe),pe=setTimeout(function(){ro(o)},j.scrollingSpeed)):ro(o)}else{var i=function(e){var o={};j.autoScrolling&&!j.scrollBar?(o.options={top:-e.dtop},o.element=l):(o.options={scrollTop:e.dtop},o.element="html, body");return o}(o);e(i.element).animate(i.options,j.scrollingSpeed,j.easing).promise().done(function(){j.scrollBar?setTimeout(function(){ro(o)},30):ro(o)})}}(p),$=p.anchorLink,Ho(p.anchorLink,p.sectionIndex)}}}function ro(o){var n;(n=o).wrapAroundElements&&n.wrapAroundElements.length&&(n.isMovementUp?e(S).before(n.wrapAroundElements):e(b).after(n.wrapAroundElements),Qo(e(w).position().top),lo()),e.isFunction(j.afterLoad)&&!o.localIsResizing&&j.afterLoad.call(o.element,o.anchorLink,o.sectionIndex+1),j.scrollOverflow&&j.scrollOverflowHandler.afterLoad(),o.localIsResizing||uo(o.element),o.element.addClass(v).siblings().removeClass(v),de=!0,e.isFunction(o.callback)&&o.callback.call(this)}function co(e,o){e.attr(o,e.data(o)).removeAttr("data-"+o)}function fo(o){var n;j.lazyLoading&&po(o).find("img[data-src], img[data-srcset], source[data-src], source[data-srcset], video[data-src], audio[data-src], iframe[data-src]").each(function(){if(n=e(this),e.each(["src","srcset"],function(e,o){var t=n.attr("data-"+o);void 0!==t&&t&&co(n,o)}),n.is("source")){var o=n.closest("video").length?"video":"audio";n.closest(o).get(0).load()}})}function uo(o){var n=po(o);n.find("video, audio").each(function(){var o=e(this).get(0);o.hasAttribute("data-autoplay")&&"function"==typeof o.play&&o.play()}),n.find('iframe[src*="youtube.com/embed/"]').each(function(){var o=e(this).get(0);o.hasAttribute("data-autoplay")&&ho(o),o.onload=function(){o.hasAttribute("data-autoplay")&&ho(o)}})}function ho(e){e.contentWindow.postMessage('{"event":"command","func":"playVideo","args":""}',"*")}function vo(o){var n=po(o);n.find("video, audio").each(function(){var o=e(this).get(0);o.hasAttribute("data-keepplaying")||"function"!=typeof o.pause||o.pause()}),n.find('iframe[src*="youtube.com/embed/"]').each(function(){var o=e(this).get(0);/youtube\.com\/embed\//.test(e(this).attr("src"))&&!o.hasAttribute("data-keepplaying")&&e(this).get(0).contentWindow.postMessage('{"event":"command","func":"pauseVideo","args":""}',"*")})}function po(o){var n=o.find(B);return n.length&&(o=e(n)),o}function go(){var e=wo(),o=e.section,n=e.slide;o&&(j.animateAnchor?jo(o,n):Ne(o,n))}function mo(){if(!Te&&!j.lockAnchors){var e=wo(),o=e.section,n=e.slide,t=void 0===$,i=void 0===$&&void 0===n&&!te;o&&o.length&&(o&&o!==$&&!t||i||!te&&ee!=n)&&jo(o,n)}}function wo(){var e,n,t=o.location.hash;if(t.length){var i=t.replace("#","").split("/"),a=t.indexOf("#/")>-1;e=a?"/"+i[1]:decodeURIComponent(i[0]);var l=a?i[2]:i[1];l&&l.length&&(n=decodeURIComponent(l))}return{section:e,slide:n}}function So(o){clearTimeout(Se);var n=e(":focus"),t=o.which;if(9===t){ !function(o){var n=o.shiftKey,t=e(":focus"),i=e(w),a=i.find(B),l=(a.length?a:i).find(ye).not('[tabindex="-1"]');function s(e){return e.preventDefault(),l.first().focus()}t.length?t.closest(w,B).length||(t=s(o)):s(o);(!n&&t.is(l.last())||n&&t.is(l.first()))&&o.preventDefault()}(o); }else if(!n.is("textarea")&&!n.is("input")&&!n.is("select")&&"true"!==n.attr("contentEditable")&&""!==n.attr("contentEditable")&&j.keyboardScrolling&&j.autoScrolling){e.inArray(t,[40,38,32,33,34])>-1&&o.preventDefault(),oe=o.ctrlKey,Se=setTimeout(function(){!function(o){var n=o.shiftKey;if(!de&&[37,39].indexOf(o.which)<0){ return; }switch(o.which){case 38:case 33:ue.k.up&&je();break;case 32:if(n&&ue.k.up){je();break}case 40:case 34:ue.k.down&&Ye();break;case 36:ue.k.up&&Xe(1);break;case 35:ue.k.down&&Xe(e(m).length);break;case 37:ue.k.left&&We();break;case 39:ue.k.right&&Ue();break;default:;}}(o)},150)}}function bo(){e(this).prev().trigger("click")}function xo(e){ce&&(oe=e.ctrlKey)}function yo(e){2==e.which&&(Be=e.pageY,le.on("mousemove",Oo))}function Co(e){2==e.which&&le.off("mousemove")}function To(){var o=e(this).closest(m);e(this).hasClass(N)?ue.m.left&&We(o):ue.m.right&&Ue(o)}function ko(){ce=!1,oe=!1}function Lo(o){o.preventDefault();var n=e(this).parent().index();so(e(m).eq(n))}function Ao(o){o.preventDefault();var n=e(this).closest(m).find(z);Io(n,n.find(M).eq(e(this).closest("li").index()))}function Oo(e){de&&(e.pageY<Be&&ue.m.up?je():e.pageY>Be&&ue.m.down&&Ye()),Be=e.pageY}function Io(o,n,i){var a=o.closest(m),l={slides:o,destiny:n,direction:i,destinyPos:n.position(),slideIndex:n.index(),section:a,sectionIndex:a.index(m),anchorLink:a.data("anchor"),slidesNav:a.find(F),slideAnchor:Uo(n),prevSlide:a.find(B),prevSlideIndex:a.find(B).index(),localIsResizing:re};l.xMovement=function(e,o){if(e==o){ return"none"; }if(e>o){ return"left"; }return"right"}(l.prevSlideIndex,l.slideIndex),l.localIsResizing||(de=!1),j.onSlideLeave&&!l.localIsResizing&&"none"!==l.xMovement&&e.isFunction(j.onSlideLeave)&&!1===j.onSlideLeave.call(l.prevSlide,l.anchorLink,l.sectionIndex+1,l.prevSlideIndex,l.direction,l.slideIndex)?te=!1:(n.addClass(u).siblings().removeClass(u),l.localIsResizing||(vo(l.prevSlide),fo(n)),!j.loopHorizontal&&j.controlArrows&&(a.find(U).toggle(0!==l.slideIndex),a.find(_).toggle(!n.is(":last-child"))),a.hasClass(u)&&!l.localIsResizing&&No(l.slideIndex,l.slideAnchor,l.anchorLink,l.sectionIndex),function(e,o,n){var i=o.destinyPos;if(j.css3){var a="translate3d(-"+t.round(i.left)+"px, 0px, 0px)";Ro(e.find(D)).css(Go(a)),ge=setTimeout(function(){n&&Eo(o)},j.scrollingSpeed,j.easing)}else { e.animate({scrollLeft:t.round(i.left)},j.scrollingSpeed,j.easing,function(){n&&Eo(o)}) }}(o,l,!0))}function Eo(o){var n,t;n=o.slidesNav,t=o.slideIndex,n.find(h).removeClass(u),n.find("li").eq(t).find("a").addClass(u),o.localIsResizing||(e.isFunction(j.afterSlideLoad)&&j.afterSlideLoad.call(o.destiny,o.anchorLink,o.sectionIndex+1,o.slideAnchor,o.slideIndex),de=!0,uo(o.destiny)),te=!1}function Mo(){if(Bo(),ie){var o=e(n.activeElement);if(!o.is("textarea")&&!o.is("input")&&!o.is("select")){var i=Q.height();t.abs(i-Re)>20*t.max(Re,i)/100&&(Ke(!0),Re=i)}}else { clearTimeout(ve),ve=setTimeout(function(){Ke(!0)},350) }}function Bo(){var e=j.responsive||j.responsiveWidth,o=j.responsiveHeight,n=e&&Q.outerWidth()<e,t=o&&Q.height()<o;e&&o?_e(n||t):e?_e(n):o&&_e(t)}function Ro(e){var o="all "+j.scrollingSpeed+"ms "+j.easingcss3;return e.removeClass(r),e.css({"-webkit-transition":o,transition:o})}function zo(e){return e.addClass(r)}function Ho(o,n){var t,i,a;t=o,j.menu&&(e(j.menu).find(h).removeClass(u),e(j.menu).find('[data-menuanchor="'+t+'"]').addClass(u)),i=o,a=n,j.navigation&&(e(L).find(h).removeClass(u),i?e(L).find('a[href="#'+i+'"]').addClass(u):e(L).find("li").eq(a).find("a").addClass(u))}function Do(o){var n=e(w).index(m),t=o.index(m);return n==t?"none":n>t?"up":"down"}function Po(o){if(!o.hasClass(P)){var n=e('<div class="'+x+'" />').height(qo(o));o.addClass(P).wrapInner(n)}}function qo(e){var o=se;if(j.paddingTop||j.paddingBottom){var n=e;n.hasClass(g)||(n=e.closest(m));var t=parseInt(n.css("padding-top"))+parseInt(n.css("padding-bottom"));o=se-t}return o}function Fo(e,o){o?Ro(le):zo(le),le.css(Go(e)),setTimeout(function(){le.removeClass(r)},10)}function Vo(o){var n=le.find(m+'[data-anchor="'+o+'"]');if(!n.length){var t=void 0!==o?o-1:0;n=e(m).eq(t)}return n}function jo(e,o){var n=Vo(e);if(n.length){var t,i,a,l=(t=o,(a=(i=n).find(M+'[data-anchor="'+t+'"]')).length||(t=void 0!==t?t:0,a=i.find(M).eq(t)),a);e===$||n.hasClass(u)?Yo(l):so(n,function(){Yo(l)})}}function Yo(e){e.length&&Io(e.closest(z),e)}function No(e,o,n,t){var i="";j.anchors.length&&!j.lockAnchors&&(e?(void 0!==n&&(i=n),void 0===o&&(o=e),ee=o,Xo(i+"/"+o)):void 0!==e?(ee=o,Xo(n)):Xo(n)),Wo()}function Xo(e){if(j.recordHistory){ location.hash=e; }else if(ie||ae){ o.history.replaceState(i,i,"#"+e); }else{var n=o.location.href.split("#")[0];o.location.replace(n+"#"+e)}}function Uo(e){var o=e.data("anchor"),n=e.index();return void 0===o&&(o=n),o}function Wo(){var o=e(w),n=o.find(B),t=Uo(o),i=Uo(n),a=String(t);n.length&&(a=a+"-"+i),a=a.replace("/","-").replace("#","");var l=new RegExp("\\b\\s?"+f+"-[^\\s]+\\b","g");J[0].className=J[0].className.replace(l,""),J.addClass(f+"-"+a)}function Ko(e){var o=[];return o.y=void 0!==e.pageY&&(e.pageY||e.pageX)?e.pageY:e.touches[0].pageY,o.x=void 0!==e.pageX&&(e.pageY||e.pageX)?e.pageX:e.touches[0].pageX,ae&&oo(e)&&(j.scrollBar||!j.autoScrolling)&&(o.y=e.touches[0].pageY,o.x=e.touches[0].pageX),o}function _o(e,o){De(0,"internal"),void 0!==o&&(re=!0),Io(e.closest(z),e),void 0!==o&&(re=!1),De(Ce.scrollingSpeed,"internal")}function Qo(e){var o=t.round(e);j.css3&&j.autoScrolling&&!j.scrollBar?Fo("translate3d(0px, -"+o+"px, 0px)",!1):j.autoScrolling&&!j.scrollBar?le.css("top",-o):W.scrollTop(o)}function Go(e){return{"-webkit-transform":e,"-moz-transform":e,"-ms-transform":e,transform:e}}function Jo(o,n,t){"all"!==n?ue[t][n]=o:e.each(Object.keys(ue[t]),function(e,n){ue[t][n]=o})}function Zo(e,o,n){j[e]=o,"internal"!==n&&(Ce[e]=o)}function $o(){e("html").hasClass(d)?en("error","Fullpage.js can only be initialized once and you are doing it multiple times!"):(j.continuousVertical&&(j.loopTop||j.loopBottom)&&(j.continuousVertical=!1,en("warn","Option `loopTop/loopBottom` is mutually exclusive with `continuousVertical`; `continuousVertical` disabled")),j.scrollBar&&j.scrollOverflow&&en("warn","Option `scrollBar` is mutually exclusive with `scrollOverflow`. Sections with scrollOverflow might not work well in Firefox"),!j.continuousVertical||!j.scrollBar&&j.autoScrolling||(j.continuousVertical=!1,en("warn","Scroll bars (`scrollBar:true` or `autoScrolling:false`) are mutually exclusive with `continuousVertical`; `continuousVertical` disabled")),j.scrollOverflow&&!j.scrollOverflowHandler&&(j.scrollOverflow=!1,en("error","The option `scrollOverflow:true` requires the file `scrolloverflow.min.js`. Please include it before fullPage.js.")),e.each(["fadingEffect","continuousHorizontal","scrollHorizontally","interlockedSlides","resetSliders","responsiveSlides","offsetSections","dragAndMove","scrollOverflowReset","parallax"],function(e,o){j[o]&&en("warn","fullpage.js extensions require jquery.fullpage.extensions.min.js file instead of the usual jquery.fullpage.js. Requested: "+o)}),e.each(j.anchors,function(o,n){var t=G.find("[name]").filter(function(){return e(this).attr("name")&&e(this).attr("name").toLowerCase()==n.toLowerCase()}),i=G.find("[id]").filter(function(){return e(this).attr("id")&&e(this).attr("id").toLowerCase()==n.toLowerCase()});(i.length||t.length)&&(en("error","data-anchor tags can not have the same value as any `id` element on the site (or `name` element for IE)."),i.length&&en("error",'"'+n+'" is is being used by another element `id` property'),t.length&&en("error",'"'+n+'" is is being used by another element `name` property'))}))}function en(e,o){console&&console[e]&&console[e]("fullPage: "+o)}}});
//# sourceMappingURL=jquery.fullpage.min.js.map

/***/ }),

/***/ "./js/vendor/scrolloverflow.min.js":
/*!*****************************************!*\
  !*** ./js/vendor/scrolloverflow.min.js ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(jQuery, IScroll) {var __WEBPACK_AMD_DEFINE_RESULT__;/**
* Customized version of iScroll.js 0.0.8
* It fixes bugs affecting its integration with fullpage.js
* @license
*/
!function(t,i,s){var e=t.requestAnimationFrame||t.webkitRequestAnimationFrame||t.mozRequestAnimationFrame||t.oRequestAnimationFrame||t.msRequestAnimationFrame||function(i){t.setTimeout(i,1e3/60)},o=function(){var e={},o=i.createElement("div").style,n=function(){for(var t=["t","webkitT","MozT","msT","OT"],i=0,s=t.length;i<s;i++){ if(t[i]+"ransform"in o){ return t[i].substr(0,t[i].length-1); } }return!1}();function r(t){return!1!==n&&(""===n?t:n+t.charAt(0).toUpperCase()+t.substr(1))}e.getTime=Date.now||function(){return(new Date).getTime()},e.extend=function(t,i){for(var s in i){ t[s]=i[s] }},e.addEvent=function(t,i,s,e){t.addEventListener(i,s,!!e)},e.removeEvent=function(t,i,s,e){t.removeEventListener(i,s,!!e)},e.prefixPointerEvent=function(i){return t.MSPointerEvent?"MSPointer"+i.charAt(7).toUpperCase()+i.substr(8):i},e.momentum=function(t,i,e,o,n,r){var h,a,l=t-i,c=s.abs(l)/e;return a=c/(r=void 0===r?6e-4:r),(h=t+c*c/(2*r)*(l<0?-1:1))<o?(h=n?o-n/2.5*(c/8):o,a=(l=s.abs(h-t))/c):h>0&&(h=n?n/2.5*(c/8):0,a=(l=s.abs(t)+h)/c),{destination:s.round(h),duration:a}};var h=r("transform");return e.extend(e,{hasTransform:!1!==h,hasPerspective:r("perspective")in o,hasTouch:"ontouchstart"in t,hasPointer:!(!t.PointerEvent&&!t.MSPointerEvent),hasTransition:r("transition")in o}),e.isBadAndroid=function(){var i=t.navigator.appVersion;if(/Android/.test(i)&&!/Chrome\/\d/.test(i)){var s=i.match(/Safari\/(\d+.\d)/);return!(s&&"object"==typeof s&&s.length>=2)||parseFloat(s[1])<535.19}return!1}(),e.extend(e.style={},{transform:h,transitionTimingFunction:r("transitionTimingFunction"),transitionDuration:r("transitionDuration"),transitionDelay:r("transitionDelay"),transformOrigin:r("transformOrigin")}),e.hasClass=function(t,i){return new RegExp("(^|\\s)"+i+"(\\s|$)").test(t.className)},e.addClass=function(t,i){if(!e.hasClass(t,i)){var s=t.className.split(" ");s.push(i),t.className=s.join(" ")}},e.removeClass=function(t,i){if(e.hasClass(t,i)){var s=new RegExp("(^|\\s)"+i+"(\\s|$)","g");t.className=t.className.replace(s," ")}},e.offset=function(t){for(var i=-t.offsetLeft,s=-t.offsetTop;t=t.offsetParent;){ i-=t.offsetLeft,s-=t.offsetTop; }return{left:i,top:s}},e.preventDefaultException=function(t,i){for(var s in i){ if(i[s].test(t[s])){ return!0; } }return!1},e.extend(e.eventType={},{touchstart:1,touchmove:1,touchend:1,mousedown:2,mousemove:2,mouseup:2,pointerdown:3,pointermove:3,pointerup:3,MSPointerDown:3,MSPointerMove:3,MSPointerUp:3}),e.extend(e.ease={},{quadratic:{style:"cubic-bezier(0.25, 0.46, 0.45, 0.94)",fn:function(t){return t*(2-t)}},circular:{style:"cubic-bezier(0.1, 0.57, 0.1, 1)",fn:function(t){return s.sqrt(1- --t*t)}},back:{style:"cubic-bezier(0.175, 0.885, 0.32, 1.275)",fn:function(t){return(t-=1)*t*(5*t+4)+1}},bounce:{style:"",fn:function(t){return(t/=1)<1/2.75?7.5625*t*t:t<2/2.75?7.5625*(t-=1.5/2.75)*t+.75:t<2.5/2.75?7.5625*(t-=2.25/2.75)*t+.9375:7.5625*(t-=2.625/2.75)*t+.984375}},elastic:{style:"",fn:function(t){return 0===t?0:1==t?1:.4*s.pow(2,-10*t)*s.sin((t-.055)*(2*s.PI)/.22)+1}}}),e.tap=function(t,s){var e=i.createEvent("Event");e.initEvent(s,!0,!0),e.pageX=t.pageX,e.pageY=t.pageY,t.target.dispatchEvent(e)},e.click=function(s){var e,o=s.target;/(SELECT|INPUT|TEXTAREA)/i.test(o.tagName)||((e=i.createEvent(t.MouseEvent?"MouseEvents":"Event")).initEvent("click",!0,!0),e.view=s.view||t,e.detail=1,e.screenX=o.screenX||0,e.screenY=o.screenY||0,e.clientX=o.clientX||0,e.clientY=o.clientY||0,e.ctrlKey=!!s.ctrlKey,e.altKey=!!s.altKey,e.shiftKey=!!s.shiftKey,e.metaKey=!!s.metaKey,e.button=0,e.relatedTarget=null,e._constructed=!0,o.dispatchEvent(e))},e}();function n(s,e){
var this$1 = this;
for(var n in this$1.wrapper="string"==typeof s?i.querySelector(s):s,this$1.scroller=this$1.wrapper.children[0],this$1.scrollerStyle=this$1.scroller.style,this$1.options={resizeScrollbars:!0,mouseWheelSpeed:20,snapThreshold:.334,disablePointer:!o.hasPointer,disableTouch:o.hasPointer||!o.hasTouch,disableMouse:o.hasPointer||o.hasTouch,startX:0,startY:0,scrollY:!0,directionLockThreshold:5,momentum:!0,bounce:!0,bounceTime:600,bounceEasing:"",preventDefault:!0,preventDefaultException:{tagName:/^(INPUT|TEXTAREA|BUTTON|SELECT|LABEL)$/},HWCompositing:!0,useTransition:!0,useTransform:!0,bindToWrapper:void 0===t.onmousedown},e){ this$1.options[n]=e[n]; }this.translateZ=this.options.HWCompositing&&o.hasPerspective?" translateZ(0)":"",this.options.useTransition=o.hasTransition&&this.options.useTransition,this.options.useTransform=o.hasTransform&&this.options.useTransform,this.options.eventPassthrough=!0===this.options.eventPassthrough?"vertical":this.options.eventPassthrough,this.options.preventDefault=!this.options.eventPassthrough&&this.options.preventDefault,this.options.scrollY="vertical"!=this.options.eventPassthrough&&this.options.scrollY,this.options.scrollX="horizontal"!=this.options.eventPassthrough&&this.options.scrollX,this.options.freeScroll=this.options.freeScroll&&!this.options.eventPassthrough,this.options.directionLockThreshold=this.options.eventPassthrough?0:this.options.directionLockThreshold,this.options.bounceEasing="string"==typeof this.options.bounceEasing?o.ease[this.options.bounceEasing]||o.ease.circular:this.options.bounceEasing,this.options.resizePolling=void 0===this.options.resizePolling?60:this.options.resizePolling,!0===this.options.tap&&(this.options.tap="tap"),this.options.useTransition||this.options.useTransform||/relative|absolute/i.test(this.scrollerStyle.position)||(this.scrollerStyle.position="relative"),"scale"==this.options.shrinkScrollbars&&(this.options.useTransition=!1),this.options.invertWheelDirection=this.options.invertWheelDirection?-1:1,this.x=0,this.y=0,this.directionX=0,this.directionY=0,this._events={},this._init(),this.refresh(),this.scrollTo(this.options.startX,this.options.startY),this.enable()}function r(t,s,e){var o=i.createElement("div"),n=i.createElement("div");return!0===e&&(o.style.cssText="position:absolute;z-index:9999",n.style.cssText="-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;position:absolute;background:rgba(0,0,0,0.5);border:1px solid rgba(255,255,255,0.9);border-radius:3px"),n.className="iScrollIndicator","h"==t?(!0===e&&(o.style.cssText+=";height:7px;left:2px;right:2px;bottom:0",n.style.height="100%"),o.className="iScrollHorizontalScrollbar"):(!0===e&&(o.style.cssText+=";width:7px;bottom:2px;top:2px;right:1px",n.style.width="100%"),o.className="iScrollVerticalScrollbar"),o.style.cssText+=";overflow:hidden",s||(o.style.pointerEvents="none"),o.appendChild(n),o}function h(s,n){
var this$1 = this;
for(var r in this$1.wrapper="string"==typeof n.el?i.querySelector(n.el):n.el,this$1.wrapperStyle=this$1.wrapper.style,this$1.indicator=this$1.wrapper.children[0],this$1.indicatorStyle=this$1.indicator.style,this$1.scroller=s,this$1.options={listenX:!0,listenY:!0,interactive:!1,resize:!0,defaultScrollbars:!1,shrink:!1,fade:!1,speedRatioX:0,speedRatioY:0},n){ this$1.options[r]=n[r]; }if(this.sizeRatioX=1,this.sizeRatioY=1,this.maxPosX=0,this.maxPosY=0,this.options.interactive&&(this.options.disableTouch||(o.addEvent(this.indicator,"touchstart",this),o.addEvent(t,"touchend",this)),this.options.disablePointer||(o.addEvent(this.indicator,o.prefixPointerEvent("pointerdown"),this),o.addEvent(t,o.prefixPointerEvent("pointerup"),this)),this.options.disableMouse||(o.addEvent(this.indicator,"mousedown",this),o.addEvent(t,"mouseup",this))),this.options.fade){this.wrapperStyle[o.style.transform]=this.scroller.translateZ;var h=o.style.transitionDuration;if(!h){ return; }this.wrapperStyle[h]=o.isBadAndroid?"0.0001ms":"0ms";var a=this;o.isBadAndroid&&e(function(){"0.0001ms"===a.wrapperStyle[h]&&(a.wrapperStyle[h]="0s")}),this.wrapperStyle.opacity="0"}}n.prototype={version:"5.2.0",_init:function(){this._initEvents(),(this.options.scrollbars||this.options.indicators)&&this._initIndicators(),this.options.mouseWheel&&this._initWheel(),this.options.snap&&this._initSnap(),this.options.keyBindings&&this._initKeys()},destroy:function(){this._initEvents(!0),clearTimeout(this.resizeTimeout),this.resizeTimeout=null,this._execEvent("destroy")},_transitionEnd:function(t){t.target==this.scroller&&this.isInTransition&&(this._transitionTime(),this.resetPosition(this.options.bounceTime)||(this.isInTransition=!1,this._execEvent("scrollEnd")))},_start:function(t){if(1!=o.eventType[t.type]&&0!==(t.which?t.button:t.button<2?0:4==t.button?1:2)){ return; }if(this.enabled&&(!this.initiated||o.eventType[t.type]===this.initiated)){!this.options.preventDefault||o.isBadAndroid||o.preventDefaultException(t.target,this.options.preventDefaultException)||t.preventDefault();var i,e=t.touches?t.touches[0]:t;this.initiated=o.eventType[t.type],this.moved=!1,this.distX=0,this.distY=0,this.directionX=0,this.directionY=0,this.directionLocked=0,this.startTime=o.getTime(),this.options.useTransition&&this.isInTransition?(this._transitionTime(),this.isInTransition=!1,i=this.getComputedPosition(),this._translate(s.round(i.x),s.round(i.y)),this._execEvent("scrollEnd")):!this.options.useTransition&&this.isAnimating&&(this.isAnimating=!1,this._execEvent("scrollEnd")),this.startX=this.x,this.startY=this.y,this.absStartX=this.x,this.absStartY=this.y,this.pointX=e.pageX,this.pointY=e.pageY,this._execEvent("beforeScrollStart")}},_move:function(t){if(this.enabled&&o.eventType[t.type]===this.initiated){this.options.preventDefault&&t.preventDefault();var i,e,n,r,h=t.touches?t.touches[0]:t,a=h.pageX-this.pointX,l=h.pageY-this.pointY,c=o.getTime();if(this.pointX=h.pageX,this.pointY=h.pageY,this.distX+=a,this.distY+=l,n=s.abs(this.distX),r=s.abs(this.distY),!(c-this.endTime>300&&n<10&&r<10)){if(this.directionLocked||this.options.freeScroll||(n>r+this.options.directionLockThreshold?this.directionLocked="h":r>=n+this.options.directionLockThreshold?this.directionLocked="v":this.directionLocked="n"),"h"==this.directionLocked){if("vertical"==this.options.eventPassthrough){ t.preventDefault(); }else if("horizontal"==this.options.eventPassthrough){ return void(this.initiated=!1); }l=0}else if("v"==this.directionLocked){if("horizontal"==this.options.eventPassthrough){ t.preventDefault(); }else if("vertical"==this.options.eventPassthrough){ return void(this.initiated=!1); }a=0}a=this.hasHorizontalScroll?a:0,l=this.hasVerticalScroll?l:0,i=this.x+a,e=this.y+l,(i>0||i<this.maxScrollX)&&(i=this.options.bounce?this.x+a/3:i>0?0:this.maxScrollX),(e>0||e<this.maxScrollY)&&(e=this.options.bounce?this.y+l/3:e>0?0:this.maxScrollY),this.directionX=a>0?-1:a<0?1:0,this.directionY=l>0?-1:l<0?1:0,this.moved||this._execEvent("scrollStart"),this.moved=!0,this._translate(i,e),c-this.startTime>300&&(this.startTime=c,this.startX=this.x,this.startY=this.y)}}},_end:function(t){if(this.enabled&&o.eventType[t.type]===this.initiated){this.options.preventDefault&&!o.preventDefaultException(t.target,this.options.preventDefaultException)&&t.preventDefault();t.changedTouches&&t.changedTouches[0];var i,e,n=o.getTime()-this.startTime,r=s.round(this.x),h=s.round(this.y),a=s.abs(r-this.startX),l=s.abs(h-this.startY),c=0,p="";if(this.isInTransition=0,this.initiated=0,this.endTime=o.getTime(),!this.resetPosition(this.options.bounceTime)){if(this.scrollTo(r,h),!this.moved){ return this.options.tap&&o.tap(t,this.options.tap),this.options.click&&o.click(t),void this._execEvent("scrollCancel"); }if(this._events.flick&&n<200&&a<100&&l<100){ this._execEvent("flick"); }else{if(this.options.momentum&&n<300&&(i=this.hasHorizontalScroll?o.momentum(this.x,this.startX,n,this.maxScrollX,this.options.bounce?this.wrapperWidth:0,this.options.deceleration):{destination:r,duration:0},e=this.hasVerticalScroll?o.momentum(this.y,this.startY,n,this.maxScrollY,this.options.bounce?this.wrapperHeight:0,this.options.deceleration):{destination:h,duration:0},r=i.destination,h=e.destination,c=s.max(i.duration,e.duration),this.isInTransition=1),this.options.snap){var d=this._nearestSnap(r,h);this.currentPage=d,c=this.options.snapSpeed||s.max(s.max(s.min(s.abs(r-d.x),1e3),s.min(s.abs(h-d.y),1e3)),300),r=d.x,h=d.y,this.directionX=0,this.directionY=0,p=this.options.bounceEasing}if(r!=this.x||h!=this.y){ return(r>0||r<this.maxScrollX||h>0||h<this.maxScrollY)&&(p=o.ease.quadratic),void this.scrollTo(r,h,c,p); }this._execEvent("scrollEnd")}}}},_resize:function(){var t=this;clearTimeout(this.resizeTimeout),this.resizeTimeout=setTimeout(function(){t.refresh()},this.options.resizePolling)},resetPosition:function(t){var i=this.x,s=this.y;return t=t||0,!this.hasHorizontalScroll||this.x>0?i=0:this.x<this.maxScrollX&&(i=this.maxScrollX),!this.hasVerticalScroll||this.y>0?s=0:this.y<this.maxScrollY&&(s=this.maxScrollY),(i!=this.x||s!=this.y)&&(this.scrollTo(i,s,t,this.options.bounceEasing),!0)},disable:function(){this.enabled=!1},enable:function(){this.enabled=!0},refresh:function(){this.wrapper.offsetHeight;this.wrapperWidth=this.wrapper.clientWidth,this.wrapperHeight=this.wrapper.clientHeight,this.scrollerWidth=this.scroller.offsetWidth,this.scrollerHeight=this.scroller.offsetHeight,this.maxScrollX=this.wrapperWidth-this.scrollerWidth,this.maxScrollY=this.wrapperHeight-this.scrollerHeight,this.hasHorizontalScroll=this.options.scrollX&&this.maxScrollX<0,this.hasVerticalScroll=this.options.scrollY&&this.maxScrollY<0,this.hasHorizontalScroll||(this.maxScrollX=0,this.scrollerWidth=this.wrapperWidth),this.hasVerticalScroll||(this.maxScrollY=0,this.scrollerHeight=this.wrapperHeight),this.endTime=0,this.directionX=0,this.directionY=0,this.wrapperOffset=o.offset(this.wrapper),this._execEvent("refresh"),this.resetPosition()},on:function(t,i){this._events[t]||(this._events[t]=[]),this._events[t].push(i)},off:function(t,i){if(this._events[t]){var s=this._events[t].indexOf(i);s>-1&&this._events[t].splice(s,1)}},_execEvent:function(t){
var arguments$1 = arguments;
var this$1 = this;
if(this._events[t]){var i=0,s=this._events[t].length;if(s){ for(;i<s;i++){ this$1._events[t][i].apply(this$1,[].slice.call(arguments$1,1)) } }}},scrollBy:function(t,i,s,e){t=this.x+t,i=this.y+i,s=s||0,this.scrollTo(t,i,s,e)},scrollTo:function(t,i,s,e){e=e||o.ease.circular,this.isInTransition=this.options.useTransition&&s>0;var n=this.options.useTransition&&e.style;!s||n?(n&&(this._transitionTimingFunction(e.style),this._transitionTime(s)),this._translate(t,i)):this._animate(t,i,s,e.fn)},scrollToElement:function(t,i,e,n,r){if(t=t.nodeType?t:this.scroller.querySelector(t)){var h=o.offset(t);h.left-=this.wrapperOffset.left,h.top-=this.wrapperOffset.top,!0===e&&(e=s.round(t.offsetWidth/2-this.wrapper.offsetWidth/2)),!0===n&&(n=s.round(t.offsetHeight/2-this.wrapper.offsetHeight/2)),h.left-=e||0,h.top-=n||0,h.left=h.left>0?0:h.left<this.maxScrollX?this.maxScrollX:h.left,h.top=h.top>0?0:h.top<this.maxScrollY?this.maxScrollY:h.top,i=null==i||"auto"===i?s.max(s.abs(this.x-h.left),s.abs(this.y-h.top)):i,this.scrollTo(h.left,h.top,i,r)}},_transitionTime:function(t){
var this$1 = this;
if(this.options.useTransition){t=t||0;var i=o.style.transitionDuration;if(i){if(this.scrollerStyle[i]=t+"ms",!t&&o.isBadAndroid){this.scrollerStyle[i]="0.0001ms";var s=this;e(function(){"0.0001ms"===s.scrollerStyle[i]&&(s.scrollerStyle[i]="0s")})}if(this.indicators){ for(var n=this.indicators.length;n--;){ this$1.indicators[n].transitionTime(t) } }}}},_transitionTimingFunction:function(t){
var this$1 = this;
if(this.scrollerStyle[o.style.transitionTimingFunction]=t,this.indicators){ for(var i=this.indicators.length;i--;){ this$1.indicators[i].transitionTimingFunction(t) } }},_translate:function(t,i){
var this$1 = this;
if(this.options.useTransform?this.scrollerStyle[o.style.transform]="translate("+t+"px,"+i+"px)"+this.translateZ:(t=s.round(t),i=s.round(i),this.scrollerStyle.left=t+"px",this.scrollerStyle.top=i+"px"),this.x=t,this.y=i,this.indicators){ for(var e=this.indicators.length;e--;){ this$1.indicators[e].updatePosition() } }},_initEvents:function(i){var s=i?o.removeEvent:o.addEvent,e=this.options.bindToWrapper?this.wrapper:t;s(t,"orientationchange",this),s(t,"resize",this),this.options.click&&s(this.wrapper,"click",this,!0),this.options.disableMouse||(s(this.wrapper,"mousedown",this),s(e,"mousemove",this),s(e,"mousecancel",this),s(e,"mouseup",this)),o.hasPointer&&!this.options.disablePointer&&(s(this.wrapper,o.prefixPointerEvent("pointerdown"),this),s(e,o.prefixPointerEvent("pointermove"),this),s(e,o.prefixPointerEvent("pointercancel"),this),s(e,o.prefixPointerEvent("pointerup"),this)),o.hasTouch&&!this.options.disableTouch&&(s(this.wrapper,"touchstart",this),s(e,"touchmove",this),s(e,"touchcancel",this),s(e,"touchend",this)),s(this.scroller,"transitionend",this),s(this.scroller,"webkitTransitionEnd",this),s(this.scroller,"oTransitionEnd",this),s(this.scroller,"MSTransitionEnd",this)},getComputedPosition:function(){var i,s,e=t.getComputedStyle(this.scroller,null);return this.options.useTransform?(i=+((e=e[o.style.transform].split(")")[0].split(", "))[12]||e[4]),s=+(e[13]||e[5])):(i=+e.left.replace(/[^-\d.]/g,""),s=+e.top.replace(/[^-\d.]/g,"")),{x:i,y:s}},_initIndicators:function(){
var this$1 = this;
var t,i=this.options.interactiveScrollbars,s="string"!=typeof this.options.scrollbars,e=[],o=this;this.indicators=[],this.options.scrollbars&&(this.options.scrollY&&(t={el:r("v",i,this.options.scrollbars),interactive:i,defaultScrollbars:!0,customStyle:s,resize:this.options.resizeScrollbars,shrink:this.options.shrinkScrollbars,fade:this.options.fadeScrollbars,listenX:!1},this.wrapper.appendChild(t.el),e.push(t)),this.options.scrollX&&(t={el:r("h",i,this.options.scrollbars),interactive:i,defaultScrollbars:!0,customStyle:s,resize:this.options.resizeScrollbars,shrink:this.options.shrinkScrollbars,fade:this.options.fadeScrollbars,listenY:!1},this.wrapper.appendChild(t.el),e.push(t))),this.options.indicators&&(e=e.concat(this.options.indicators));for(var n=e.length;n--;){ this$1.indicators.push(new h(this$1,e[n])); }function a(t){if(o.indicators){ for(var i=o.indicators.length;i--;){ t.call(o.indicators[i]) } }}this.options.fadeScrollbars&&(this.on("scrollEnd",function(){a(function(){this.fade()})}),this.on("scrollCancel",function(){a(function(){this.fade()})}),this.on("scrollStart",function(){a(function(){this.fade(1)})}),this.on("beforeScrollStart",function(){a(function(){this.fade(1,!0)})})),this.on("refresh",function(){a(function(){this.refresh()})}),this.on("destroy",function(){a(function(){this.destroy()}),delete this.indicators})},_initWheel:function(){o.addEvent(this.wrapper,"wheel",this),o.addEvent(this.wrapper,"mousewheel",this),o.addEvent(this.wrapper,"DOMMouseScroll",this),this.on("destroy",function(){clearTimeout(this.wheelTimeout),this.wheelTimeout=null,o.removeEvent(this.wrapper,"wheel",this),o.removeEvent(this.wrapper,"mousewheel",this),o.removeEvent(this.wrapper,"DOMMouseScroll",this)})},_wheel:function(t){if(this.enabled){var i,e,o,n,r=this;if(void 0===this.wheelTimeout&&r._execEvent("scrollStart"),clearTimeout(this.wheelTimeout),this.wheelTimeout=setTimeout(function(){r.options.snap||r._execEvent("scrollEnd"),r.wheelTimeout=void 0},400),"deltaX"in t){ 1===t.deltaMode?(i=-t.deltaX*this.options.mouseWheelSpeed,e=-t.deltaY*this.options.mouseWheelSpeed):(i=-t.deltaX,e=-t.deltaY); }else if("wheelDeltaX"in t){ i=t.wheelDeltaX/120*this.options.mouseWheelSpeed,e=t.wheelDeltaY/120*this.options.mouseWheelSpeed; }else if("wheelDelta"in t){ i=e=t.wheelDelta/120*this.options.mouseWheelSpeed; }else{if(!("detail"in t)){ return; }i=e=-t.detail/3*this.options.mouseWheelSpeed}if(i*=this.options.invertWheelDirection,e*=this.options.invertWheelDirection,this.hasVerticalScroll||(i=e,e=0),this.options.snap){ return o=this.currentPage.pageX,n=this.currentPage.pageY,i>0?o--:i<0&&o++,e>0?n--:e<0&&n++,void this.goToPage(o,n); }o=this.x+s.round(this.hasHorizontalScroll?i:0),n=this.y+s.round(this.hasVerticalScroll?e:0),this.directionX=i>0?-1:i<0?1:0,this.directionY=e>0?-1:e<0?1:0,o>0?o=0:o<this.maxScrollX&&(o=this.maxScrollX),n>0?n=0:n<this.maxScrollY&&(n=this.maxScrollY),this.scrollTo(o,n,0)}},_initSnap:function(){this.currentPage={},"string"==typeof this.options.snap&&(this.options.snap=this.scroller.querySelectorAll(this.options.snap)),this.on("refresh",function(){
var this$1 = this;
var t,i,e,o,n,r,h=0,a=0,l=0,c=this.options.snapStepX||this.wrapperWidth,p=this.options.snapStepY||this.wrapperHeight;if(this.pages=[],this.wrapperWidth&&this.wrapperHeight&&this.scrollerWidth&&this.scrollerHeight){if(!0===this.options.snap){ for(e=s.round(c/2),o=s.round(p/2);l>-this.scrollerWidth;){for(this.pages[h]=[],t=0,n=0;n>-this.scrollerHeight;){ this$1.pages[h][t]={x:s.max(l,this$1.maxScrollX),y:s.max(n,this$1.maxScrollY),width:c,height:p,cx:l-e,cy:n-o},n-=p,t++; }l-=c,h++} }else { for(t=(r=this.options.snap).length,i=-1;h<t;h++){ (0===h||r[h].offsetLeft<=r[h-1].offsetLeft)&&(a=0,i++),this$1.pages[a]||(this$1.pages[a]=[]),l=s.max(-r[h].offsetLeft,this$1.maxScrollX),n=s.max(-r[h].offsetTop,this$1.maxScrollY),e=l-s.round(r[h].offsetWidth/2),o=n-s.round(r[h].offsetHeight/2),this$1.pages[a][i]={x:l,y:n,width:r[h].offsetWidth,height:r[h].offsetHeight,cx:e,cy:o},l>this$1.maxScrollX&&a++; } }this.goToPage(this.currentPage.pageX||0,this.currentPage.pageY||0,0),this.options.snapThreshold%1==0?(this.snapThresholdX=this.options.snapThreshold,this.snapThresholdY=this.options.snapThreshold):(this.snapThresholdX=s.round(this.pages[this.currentPage.pageX][this.currentPage.pageY].width*this.options.snapThreshold),this.snapThresholdY=s.round(this.pages[this.currentPage.pageX][this.currentPage.pageY].height*this.options.snapThreshold))}}),this.on("flick",function(){var t=this.options.snapSpeed||s.max(s.max(s.min(s.abs(this.x-this.startX),1e3),s.min(s.abs(this.y-this.startY),1e3)),300);this.goToPage(this.currentPage.pageX+this.directionX,this.currentPage.pageY+this.directionY,t)})},_nearestSnap:function(t,i){
var this$1 = this;
if(!this.pages.length){ return{x:0,y:0,pageX:0,pageY:0}; }var e=0,o=this.pages.length,n=0;if(s.abs(t-this.absStartX)<this.snapThresholdX&&s.abs(i-this.absStartY)<this.snapThresholdY){ return this.currentPage; }for(t>0?t=0:t<this.maxScrollX&&(t=this.maxScrollX),i>0?i=0:i<this.maxScrollY&&(i=this.maxScrollY);e<o;e++){ if(t>=this$1.pages[e][0].cx){t=this$1.pages[e][0].x;break} }for(o=this.pages[e].length;n<o;n++){ if(i>=this$1.pages[0][n].cy){i=this$1.pages[0][n].y;break} }return e==this.currentPage.pageX&&((e+=this.directionX)<0?e=0:e>=this.pages.length&&(e=this.pages.length-1),t=this.pages[e][0].x),n==this.currentPage.pageY&&((n+=this.directionY)<0?n=0:n>=this.pages[0].length&&(n=this.pages[0].length-1),i=this.pages[0][n].y),{x:t,y:i,pageX:e,pageY:n}},goToPage:function(t,i,e,o){o=o||this.options.bounceEasing,t>=this.pages.length?t=this.pages.length-1:t<0&&(t=0),i>=this.pages[t].length?i=this.pages[t].length-1:i<0&&(i=0);var n=this.pages[t][i].x,r=this.pages[t][i].y;e=void 0===e?this.options.snapSpeed||s.max(s.max(s.min(s.abs(n-this.x),1e3),s.min(s.abs(r-this.y),1e3)),300):e,this.currentPage={x:n,y:r,pageX:t,pageY:i},this.scrollTo(n,r,e,o)},next:function(t,i){var s=this.currentPage.pageX,e=this.currentPage.pageY;++s>=this.pages.length&&this.hasVerticalScroll&&(s=0,e++),this.goToPage(s,e,t,i)},prev:function(t,i){var s=this.currentPage.pageX,e=this.currentPage.pageY;--s<0&&this.hasVerticalScroll&&(s=0,e--),this.goToPage(s,e,t,i)},_initKeys:function(i){
var this$1 = this;
var s,e={pageUp:33,pageDown:34,end:35,home:36,left:37,up:38,right:39,down:40};if("object"==typeof this.options.keyBindings){ for(s in this$1.options.keyBindings){ "string"==typeof this$1.options.keyBindings[s]&&(this$1.options.keyBindings[s]=this$1.options.keyBindings[s].toUpperCase().charCodeAt(0)); } }else { this.options.keyBindings={}; }for(s in e){ this$1.options.keyBindings[s]=this$1.options.keyBindings[s]||e[s]; }o.addEvent(t,"keydown",this),this.on("destroy",function(){o.removeEvent(t,"keydown",this)})},_key:function(t){if(this.enabled){var i,e=this.options.snap,n=e?this.currentPage.pageX:this.x,r=e?this.currentPage.pageY:this.y,h=o.getTime(),a=this.keyTime||0;switch(this.options.useTransition&&this.isInTransition&&(i=this.getComputedPosition(),this._translate(s.round(i.x),s.round(i.y)),this.isInTransition=!1),this.keyAcceleration=h-a<200?s.min(this.keyAcceleration+.25,50):0,t.keyCode){case this.options.keyBindings.pageUp:this.hasHorizontalScroll&&!this.hasVerticalScroll?n+=e?1:this.wrapperWidth:r+=e?1:this.wrapperHeight;break;case this.options.keyBindings.pageDown:this.hasHorizontalScroll&&!this.hasVerticalScroll?n-=e?1:this.wrapperWidth:r-=e?1:this.wrapperHeight;break;case this.options.keyBindings.end:n=e?this.pages.length-1:this.maxScrollX,r=e?this.pages[0].length-1:this.maxScrollY;break;case this.options.keyBindings.home:n=0,r=0;break;case this.options.keyBindings.left:n+=e?-1:5+this.keyAcceleration>>0;break;case this.options.keyBindings.up:r+=e?1:5+this.keyAcceleration>>0;break;case this.options.keyBindings.right:n-=e?-1:5+this.keyAcceleration>>0;break;case this.options.keyBindings.down:r-=e?1:5+this.keyAcceleration>>0;break;default:return}e?this.goToPage(n,r):(n>0?(n=0,this.keyAcceleration=0):n<this.maxScrollX&&(n=this.maxScrollX,this.keyAcceleration=0),r>0?(r=0,this.keyAcceleration=0):r<this.maxScrollY&&(r=this.maxScrollY,this.keyAcceleration=0),this.scrollTo(n,r,0),this.keyTime=h)}},_animate:function(t,i,s,n){var r=this,h=this.x,a=this.y,l=o.getTime(),c=l+s;this.isAnimating=!0,function p(){var d,u,f,m=o.getTime();if(m>=c){ return r.isAnimating=!1,r._translate(t,i),void(r.resetPosition(r.options.bounceTime)||r._execEvent("scrollEnd")); }f=n(m=(m-l)/s),d=(t-h)*f+h,u=(i-a)*f+a,r._translate(d,u),r.isAnimating&&e(p)}()},handleEvent:function(t){switch(t.type){case"touchstart":case"pointerdown":case"MSPointerDown":case"mousedown":this._start(t);break;case"touchmove":case"pointermove":case"MSPointerMove":case"mousemove":this._move(t);break;case"touchend":case"pointerup":case"MSPointerUp":case"mouseup":case"touchcancel":case"pointercancel":case"MSPointerCancel":case"mousecancel":this._end(t);break;case"orientationchange":case"resize":this._resize();break;case"transitionend":case"webkitTransitionEnd":case"oTransitionEnd":case"MSTransitionEnd":this._transitionEnd(t);break;case"wheel":case"DOMMouseScroll":case"mousewheel":this._wheel(t);break;case"keydown":this._key(t);break;case"click":this.enabled&&!t._constructed&&(t.preventDefault(),t.stopPropagation())}}},h.prototype={handleEvent:function(t){switch(t.type){case"touchstart":case"pointerdown":case"MSPointerDown":case"mousedown":this._start(t);break;case"touchmove":case"pointermove":case"MSPointerMove":case"mousemove":this._move(t);break;case"touchend":case"pointerup":case"MSPointerUp":case"mouseup":case"touchcancel":case"pointercancel":case"MSPointerCancel":case"mousecancel":this._end(t)}},destroy:function(){this.options.fadeScrollbars&&(clearTimeout(this.fadeTimeout),this.fadeTimeout=null),this.options.interactive&&(o.removeEvent(this.indicator,"touchstart",this),o.removeEvent(this.indicator,o.prefixPointerEvent("pointerdown"),this),o.removeEvent(this.indicator,"mousedown",this),o.removeEvent(t,"touchmove",this),o.removeEvent(t,o.prefixPointerEvent("pointermove"),this),o.removeEvent(t,"mousemove",this),o.removeEvent(t,"touchend",this),o.removeEvent(t,o.prefixPointerEvent("pointerup"),this),o.removeEvent(t,"mouseup",this)),this.options.defaultScrollbars&&this.wrapper.parentNode.removeChild(this.wrapper)},_start:function(i){var s=i.touches?i.touches[0]:i;i.preventDefault(),i.stopPropagation(),this.transitionTime(),this.initiated=!0,this.moved=!1,this.lastPointX=s.pageX,this.lastPointY=s.pageY,this.startTime=o.getTime(),this.options.disableTouch||o.addEvent(t,"touchmove",this),this.options.disablePointer||o.addEvent(t,o.prefixPointerEvent("pointermove"),this),this.options.disableMouse||o.addEvent(t,"mousemove",this),this.scroller._execEvent("beforeScrollStart")},_move:function(t){var i,s,e,n,r=t.touches?t.touches[0]:t;o.getTime();this.moved||this.scroller._execEvent("scrollStart"),this.moved=!0,i=r.pageX-this.lastPointX,this.lastPointX=r.pageX,s=r.pageY-this.lastPointY,this.lastPointY=r.pageY,e=this.x+i,n=this.y+s,this._pos(e,n),t.preventDefault(),t.stopPropagation()},_end:function(i){if(this.initiated){if(this.initiated=!1,i.preventDefault(),i.stopPropagation(),o.removeEvent(t,"touchmove",this),o.removeEvent(t,o.prefixPointerEvent("pointermove"),this),o.removeEvent(t,"mousemove",this),this.scroller.options.snap){var e=this.scroller._nearestSnap(this.scroller.x,this.scroller.y),n=this.options.snapSpeed||s.max(s.max(s.min(s.abs(this.scroller.x-e.x),1e3),s.min(s.abs(this.scroller.y-e.y),1e3)),300);this.scroller.x==e.x&&this.scroller.y==e.y||(this.scroller.directionX=0,this.scroller.directionY=0,this.scroller.currentPage=e,this.scroller.scrollTo(e.x,e.y,n,this.scroller.options.bounceEasing))}this.moved&&this.scroller._execEvent("scrollEnd")}},transitionTime:function(t){t=t||0;var i=o.style.transitionDuration;if(i&&(this.indicatorStyle[i]=t+"ms",!t&&o.isBadAndroid)){this.indicatorStyle[i]="0.0001ms";var s=this;e(function(){"0.0001ms"===s.indicatorStyle[i]&&(s.indicatorStyle[i]="0s")})}},transitionTimingFunction:function(t){this.indicatorStyle[o.style.transitionTimingFunction]=t},refresh:function(){this.transitionTime(),this.options.listenX&&!this.options.listenY?this.indicatorStyle.display=this.scroller.hasHorizontalScroll?"block":"none":this.options.listenY&&!this.options.listenX?this.indicatorStyle.display=this.scroller.hasVerticalScroll?"block":"none":this.indicatorStyle.display=this.scroller.hasHorizontalScroll||this.scroller.hasVerticalScroll?"block":"none",this.scroller.hasHorizontalScroll&&this.scroller.hasVerticalScroll?(o.addClass(this.wrapper,"iScrollBothScrollbars"),o.removeClass(this.wrapper,"iScrollLoneScrollbar"),this.options.defaultScrollbars&&this.options.customStyle&&(this.options.listenX?this.wrapper.style.right="8px":this.wrapper.style.bottom="8px")):(o.removeClass(this.wrapper,"iScrollBothScrollbars"),o.addClass(this.wrapper,"iScrollLoneScrollbar"),this.options.defaultScrollbars&&this.options.customStyle&&(this.options.listenX?this.wrapper.style.right="2px":this.wrapper.style.bottom="2px"));this.wrapper.offsetHeight;this.options.listenX&&(this.wrapperWidth=this.wrapper.clientWidth,this.options.resize?(this.indicatorWidth=s.max(s.round(this.wrapperWidth*this.wrapperWidth/(this.scroller.scrollerWidth||this.wrapperWidth||1)),8),this.indicatorStyle.width=this.indicatorWidth+"px"):this.indicatorWidth=this.indicator.clientWidth,this.maxPosX=this.wrapperWidth-this.indicatorWidth,"clip"==this.options.shrink?(this.minBoundaryX=8-this.indicatorWidth,this.maxBoundaryX=this.wrapperWidth-8):(this.minBoundaryX=0,this.maxBoundaryX=this.maxPosX),this.sizeRatioX=this.options.speedRatioX||this.scroller.maxScrollX&&this.maxPosX/this.scroller.maxScrollX),this.options.listenY&&(this.wrapperHeight=this.wrapper.clientHeight,this.options.resize?(this.indicatorHeight=s.max(s.round(this.wrapperHeight*this.wrapperHeight/(this.scroller.scrollerHeight||this.wrapperHeight||1)),8),this.indicatorStyle.height=this.indicatorHeight+"px"):this.indicatorHeight=this.indicator.clientHeight,this.maxPosY=this.wrapperHeight-this.indicatorHeight,"clip"==this.options.shrink?(this.minBoundaryY=8-this.indicatorHeight,this.maxBoundaryY=this.wrapperHeight-8):(this.minBoundaryY=0,this.maxBoundaryY=this.maxPosY),this.maxPosY=this.wrapperHeight-this.indicatorHeight,this.sizeRatioY=this.options.speedRatioY||this.scroller.maxScrollY&&this.maxPosY/this.scroller.maxScrollY),this.updatePosition()},updatePosition:function(){var t=this.options.listenX&&s.round(this.sizeRatioX*this.scroller.x)||0,i=this.options.listenY&&s.round(this.sizeRatioY*this.scroller.y)||0;this.options.ignoreBoundaries||(t<this.minBoundaryX?("scale"==this.options.shrink&&(this.width=s.max(this.indicatorWidth+t,8),this.indicatorStyle.width=this.width+"px"),t=this.minBoundaryX):t>this.maxBoundaryX?"scale"==this.options.shrink?(this.width=s.max(this.indicatorWidth-(t-this.maxPosX),8),this.indicatorStyle.width=this.width+"px",t=this.maxPosX+this.indicatorWidth-this.width):t=this.maxBoundaryX:"scale"==this.options.shrink&&this.width!=this.indicatorWidth&&(this.width=this.indicatorWidth,this.indicatorStyle.width=this.width+"px"),i<this.minBoundaryY?("scale"==this.options.shrink&&(this.height=s.max(this.indicatorHeight+3*i,8),this.indicatorStyle.height=this.height+"px"),i=this.minBoundaryY):i>this.maxBoundaryY?"scale"==this.options.shrink?(this.height=s.max(this.indicatorHeight-3*(i-this.maxPosY),8),this.indicatorStyle.height=this.height+"px",i=this.maxPosY+this.indicatorHeight-this.height):i=this.maxBoundaryY:"scale"==this.options.shrink&&this.height!=this.indicatorHeight&&(this.height=this.indicatorHeight,this.indicatorStyle.height=this.height+"px")),this.x=t,this.y=i,this.scroller.options.useTransform?this.indicatorStyle[o.style.transform]="translate("+t+"px,"+i+"px)"+this.scroller.translateZ:(this.indicatorStyle.left=t+"px",this.indicatorStyle.top=i+"px")},_pos:function(t,i){t<0?t=0:t>this.maxPosX&&(t=this.maxPosX),i<0?i=0:i>this.maxPosY&&(i=this.maxPosY),t=this.options.listenX?s.round(t/this.sizeRatioX):this.scroller.x,i=this.options.listenY?s.round(i/this.sizeRatioY):this.scroller.y,this.scroller.scrollTo(t,i)},fade:function(t,i){if(!i||this.visible){clearTimeout(this.fadeTimeout),this.fadeTimeout=null;var s=t?250:500,e=t?0:300;t=t?"1":"0",this.wrapperStyle[o.style.transitionDuration]=s+"ms",this.fadeTimeout=setTimeout(function(t){this.wrapperStyle.opacity=t,this.visible=+t}.bind(this,t),e)}}},n.utils=o,"undefined"!=typeof module&&module.exports?module.exports=n: true?!(__WEBPACK_AMD_DEFINE_RESULT__ = (function(){return n}).call(exports, __webpack_require__, exports, module),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__)):undefined}(window,document,Math),
/*!
* Scrolloverflow module for fullPage.js
* https://github.com/alvarotrigo/fullPage.js
* @license MIT licensed
*
* Copyright (C) 2015 alvarotrigo.com - A project by Alvaro Trigo
*/
function(t,i,s){s.fn.fp_scrolloverflow=function(){var e="fp-scrollable",o="."+e,n=".active",r=".fp-section",h=r+n,a=".fp-slide",l=a+n,c=".fp-tableCell",p="fp-responsive",d="fp-auto-height-responsive";function u(t){var i=t.closest(r);return i.length?parseInt(i.css("padding-bottom"))+parseInt(i.css("padding-top")):0}function f(){var e=this;function o(){var t;s("body").hasClass(p)?(t=e.options.scrollOverflowHandler,h(function(i){i.closest(r).hasClass(d)&&t.remove(i)})):h(n)}function n(i){if(!i.hasClass("fp-noscroll")){i.css("overflow","hidden");var o,n=e.options.scrollOverflowHandler,h=n.wrapContent(),a=i.closest(r),l=n.scrollable(i),p=u(a);l.length?o=n.scrollHeight(i):(o=i.get(0).scrollHeight-p,e.options.verticalCentered&&(o=i.find(c).get(0).scrollHeight-p));var d=s(t).height()-p;o>d?l.length?n.update(i,d):(e.options.verticalCentered?i.find(c).wrapInner(h):i.wrapInner(h),n.create(i,d,e.iscrollOptions)):n.remove(i),i.css("overflow","")}}function h(t){s(r).each(function(){var i=s(this).find(a);i.length?i.each(function(){t(s(this))}):t(s(this))})}e.options=null,e.init=function(n,r){return e.options=n,e.iscrollOptions=r,"complete"===i.readyState&&(o(),s.fn.fullpage.shared.afterRenderActions()),s(t).on("load",function(){o(),s.fn.fullpage.shared.afterRenderActions()}),e},e.createScrollBarForAll=o}IScroll.prototype.wheelOn=function(){this.wrapper.addEventListener("wheel",this),this.wrapper.addEventListener("mousewheel",this),this.wrapper.addEventListener("DOMMouseScroll",this)},IScroll.prototype.wheelOff=function(){this.wrapper.removeEventListener("wheel",this),this.wrapper.removeEventListener("mousewheel",this),this.wrapper.removeEventListener("DOMMouseScroll",this)};var m={refreshId:null,iScrollInstances:[],iscrollOptions:{scrollbars:!0,mouseWheel:!0,hideScrollbars:!1,fadeScrollbars:!1,disableMouse:!0,interactiveScrollbars:!0},init:function(i){var e="ontouchstart"in t||navigator.msMaxTouchPoints>0||navigator.maxTouchPoints;return m.iscrollOptions.click=e,m.iscrollOptions=s.extend(m.iscrollOptions,i.scrollOverflowOptions),(new f).init(i,m.iscrollOptions)},toggleWheel:function(t){s(h).find(o).each(function(){var i=s(this).data("iscrollInstance");void 0!==i&&i&&(t?i.wheelOn():i.wheelOff())})},onLeave:function(){m.toggleWheel(!1)},beforeLeave:function(){m.onLeave()},afterLoad:function(){m.toggleWheel(!0)},create:function(t,i,e){var n=t.find(o);n.height(i),n.each(function(){var i=s(this),o=i.data("iscrollInstance");o&&s.each(m.iScrollInstances,function(){s(this).destroy()}),o=new IScroll(i.get(0),e),m.iScrollInstances.push(o),t.closest(r).hasClass("active")||o.wheelOff(),i.data("iscrollInstance",o)})},isScrolled:function(t,i){var s=i.data("iscrollInstance");return!s||("top"===t?s.y>=0&&!i.scrollTop():"bottom"===t?0-s.y+i.scrollTop()+1+i.innerHeight()>=i[0].scrollHeight:void 0)},scrollable:function(t){return t.find(".fp-slides").length?t.find(l).find(o):t.find(o)},scrollHeight:function(t){return t.find(o).children().first().get(0).scrollHeight},remove:function(t){var i=t.find(o);if(i.length){var s=i.data("iscrollInstance");s&&s.destroy(),i.data("iscrollInstance",null)}t.find(o).children().first().children().first().unwrap().unwrap()},update:function(t,i){clearTimeout(m.refreshId),m.refreshId=setTimeout(function(){s.each(m.iScrollInstances,function(){s(this).get(0).refresh(),s.fn.fullpage.silentMoveTo(s(h).index()+1)})},150),t.find(o).css("height",i+"px").parent().css("height",i+u(t)+"px")},wrapContent:function(){return'<div class="'+e+'"><div class="fp-scroller"></div></div>'}};return{iscrollHandler:m}}()}(window,document,jQuery);
//# sourceMappingURL=scrolloverflow.min.js.map
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js"), __webpack_require__(/*! iscroll */ "./node_modules/iscroll/build/iscroll.js")))

/***/ }),

/***/ "./scss/app.scss":
/*!***********************!*\
  !*** ./scss/app.scss ***!
  \***********************/
/*! no static exports found */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ 0:
/*!*********************************************!*\
  !*** multi ./js/app/app.js ./scss/app.scss ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! ./js/app/app.js */"./js/app/app.js");
module.exports = __webpack_require__(/*! ./scss/app.scss */"./scss/app.scss");


/***/ })

/******/ });
//# sourceMappingURL=app.js.map