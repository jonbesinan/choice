import $ from 'jquery';
import Player from '@vimeo/player';
import Logger from '../widget/curator/core/logger';

const video = {
    vimeo: null,
    init () {
        $(document.body).on('click', '.video', function () {
            let self = $(this);

            if(self.get(0).paused){
                self.get(0).play();
                self.parent().children(".playpause").fadeOut();
            }else{
                self.get(0).pause();
                self.parent().children(".playpause").fadeIn();
            }
        });

        $('#loop').hover(function() {
            $(this).parents('section').addClass('hovered');
        },
        function() {
            $(this).parents('section').removeClass('hovered');
        });

        let flag = true,
        self = this,
        vidLoaded = false;
        $('#loop').click(e => {
            let $self = $(e.currentTarget);

            $self.parents('.block-video').parent().addClass('active transform-transition');
            $('.bg-video .info').hide();
            $('.bg-video .mobile-view').addClass('d-none');
            $('.main').addClass('clear-left');
            $('body').addClass('loading');
            $('#section-1').addClass('playing').removeClass('hovered');
            
            if(flag){
                setTimeout(function () {
                    $('#loop').hide();

                    if($(window).width() > 768 && vidLoaded == false) {
                        this.video = document.createElement("video");
                        this.video.controls = false;
                        this.video.id = "vid";
                        this.video.style.cssText = "display: none;";
                        // this.video.autoplay = true;
                        // this.video.loop = true;
                        // this.video.muted = true;

                        this.source1 = document.createElement("source");
                        this.source1.setAttribute("src","https://s3.amazonaws.com/maud-choice/the-choice-foundation-main-video-final-html5.mp4");
                        this.source1.setAttribute("type", "video/mp4");
                        this.video.appendChild(this.source1);

                        this.source2 = document.createElement("source");
                        this.source2.setAttribute("src","https://s3.amazonaws.com/maud-choice/the-choice-foundation-main-video-final-html5.webm");
                        this.source2.setAttribute("type", "video/webm");
                        this.video.appendChild(this.source2);

                        this.source3 = document.createElement("source");
                        this.source3.setAttribute("src","https://s3.amazonaws.com/maud-choice/the-choice-foundation-main-video-final-html5.3gp");
                        this.source3.setAttribute("type", "video/3gp");
                        this.video.appendChild(this.source3);

                        document.getElementById('vid-container').appendChild(this.video);

                        let vid = this.video,
                        progressBar = $('#progress-bar');

                        vid.ontimeupdate = e => {
                            let percentage = Math.floor((100 /  vid.duration) * vid.currentTime);
                            progressBar.val(percentage);
                            progressBar.html( percentage + '% played' );

                            video.seektimeupdate( vid );
                        };

                        vid.onended = () => {
                            $('.ctrl-play').addClass('paused');
                        };

                        $(document.body).on('click', '.ctrl-play, #vid', function() {
                            console.log('triggered video');
                            if (vid.paused || vid.ended){
                                vid.play();
                                $('.ctrl-play').removeClass('paused');
                            }
                            else {
                                vid.pause();
                                $('.ctrl-play').addClass('paused');
                            }
                        });

                        progressBar.on('click', video.seektime); 

                        video.volumeController(vid);
                        video.screenController(vid);

                        vidLoaded = true;
                    }

                    if($(window).width() > 768) {
                        $('#vid').fadeIn(function(){
                            $(this)[0].play();
                            $('.vid-controls').stop(true, true)
                            .animate({
                                height:"toggle",
                                opacity:"toggle"
                            },700); 
                        });
                    }
                    else{
                        let iframe = $('#vid-mobile iframe');
                        self.vimeo = new Player(iframe);

                        $('#vid-mobile').fadeIn(() =>{
                            self.vimeo.play();
                        });
                    }
                   
                }, 500);
                
                flag = false;
            }

            if($(window).width() > 768) {
                $.fn.fullpage.setAutoScrolling(false);
            }
        });

        $(document.body).on('click', '.btn-back', function(){
            flag = true;

            setTimeout(function () {
                $('.block-video').removeClass('active').find('#loop').removeAttr("controls","controls").attr('muted', 'muted');
                $('.main').removeClass('clear-left');
                $('body').removeClass('loading');
                $('#vid, #vid-mobile').hide();
                $('#loop').show();
                $('#loop').parents('.block-video').parent().removeClass('active');
                $('.bg-video .info').show();
                $('.bg-video .mobile-view').removeClass('d-none');
                $('.ctrl-play').removeClass('paused');
                $('#vid')[0].pause();
                self.vimeo.pause();
                
                $('#section-1').removeClass('playing');
            }, 200);

            setTimeout(function () {
                $('#loop').parents('.block-video').parent().removeClass('transform-transition');
            }, 1000);
            
            if($(window).width() > 768) {
                $('.vid-controls').stop(true, true)
                .animate({
                    height:"toggle",
                    opacity:"toggle"
                },300);

                $.fn.fullpage.setAutoScrolling(true);
            }
        });
    },
    loadLoop(){
        Logger.log("LOOP->loaded");
        this.loop = document.createElement("video");
        this.loop.controls = false;
        this.loop.id = "loop-video";
        this.loop.autoplay = true;
        this.loop.attributeName = "data-autoplay";
        this.loop.preload = "auto";
        this.loop.loop = true;
        this.loop.muted = true;

        this.loopSource1 = document.createElement("source");
        this.loopSource1.setAttribute("src","https://s3.amazonaws.com/maud-choice/the-choice-foundation-looping-video-blue.mp4");
        this.loopSource1.setAttribute("type", "video/mp4");
        this.loop.appendChild(this.loopSource1);

        this.loopSource2 = document.createElement("source");
        this.loopSource2.setAttribute("src","https://s3.amazonaws.com/maud-choice/the-choice-foundation-looping-video-blue.webm");
        this.loopSource2.setAttribute("type", "video/webm");
        this.loop.appendChild(this.loopSource2);

        this.loopSource3 = document.createElement("source");
        this.loopSource3.setAttribute("src","https://s3.amazonaws.com/maud-choice/the-choice-foundation-looping-video-blue.3gp");
        this.loopSource3.setAttribute("type", "video/3gp");
        this.loop.appendChild(this.loopSource3);

        document.getElementById('loop').appendChild(this.loop);

        this.loop.load();
        this.loop.play();
    },
    volumeController(vid){
        $('.ctrl-vol').on('click', e => {
            let self = $(e.currentTarget);

            if (vid.muted) {
                vid.muted = false;
                self.removeClass('muted');
            }
            else {
                vid.muted = true;
                self.addClass('muted'); 
            }
        });
    },
    screenController(vid){
        $('.ctrl-screen').on('click', e => {
            if (vid.requestFullscreen)
                if (document.fullScreenElement) {
                    document.cancelFullScreen();
                } else {
                    vid.requestFullscreen();
                }
                else if (vid.msRequestFullscreen)
                if (document.msFullscreenElement) {
                    document.msExitFullscreen();
                } else {
                    vid.msRequestFullscreen();
                }
                else if (vid.mozRequestFullScreen)
                if (document.mozFullScreenElement) {
                    document.mozCancelFullScreen();
                } else {
                    vid.mozRequestFullScreen();
                }
                else if (vid.webkitRequestFullscreen)
                if (document.webkitFullscreenElement) {
                    document.webkitCancelFullScreen();
                } else {
                    vid.webkitRequestFullscreen();
                }
            else {
                console.log("Fullscreen API is not supported");
            }
        });
    },
    seektime(e){
        let vid = $('#vid')[0], 
        progressBar = $('#progress-bar');

        let percent = e.offsetX / this.offsetWidth;
        vid.currentTime = percent * vid.duration;
        e.target.value = Math.floor(percent / 100);
        e.target.innerHTML = progressBar[0].value + '% played';
    },
    seektimeupdate(vid){
        let nt = vid.currentTime * (100 / vid.duration),
        curmins = Math.floor(vid.currentTime / 60),
        cursecs = Math.floor(vid.currentTime - curmins * 60),
        durmins = Math.floor(vid.duration / 60),
        dursecs = Math.floor(vid.duration - durmins * 60);
        
        if(cursecs < 10){ cursecs = "0"+cursecs; }
        if(dursecs < 10){ dursecs = "0"+dursecs; }
        if(curmins < 10){ curmins = curmins; }
        if(durmins < 10){ durmins = durmins; }
        
        $('.vid-controls .current').html(curmins+":"+cursecs);
        $('.vid-controls .duration').html(durmins+":"+dursecs);
    }
};

// call init here
video.init();

export default video;