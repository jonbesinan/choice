import $ from 'jquery';

const collapse = {
    init () {
        let el = $('.btn-collapse'),
        self = this;

        id = setTimeout( () => {
            self.setHeight();
        }, 600);


        let id;
        $(window).resize(() => {
            $('ul.accordion .body').removeAttr('style');
            $('ul.accordion li').removeClass('active');

            clearTimeout(id);
            id = setTimeout( () => {
                self.setHeight();
            }, 600);
        });

        $(document.body).on('click', '.btn-collapse', function(e){
            e.preventDefault();

            let $this = $(this),
                $container = $this.parents('li');
                
            //$container.toggleClass('active').find('.body').toggleClass('active');
            $container.toggleClass('active').find('.body').slideToggle(500, $.bez([0.1, 0.01, 0, 1]));

            if($(window).width() > 768){
                setTimeout(function(){ 
                    $.fn.fullpage.reBuild();
                }, 500);
            }

            return false;
        });
    },
    setHeight () {
        // $('ul.accordion .body').map(function() {
        //     let self = $(this),
        //     h = self.actual( 'height' );
        //     self.height( h );
        // });

        // let pageHeight = $(window).height();
        // $('ul.accordion .body').css('height', function() {
        //     let h = $(this).innerHeight();
        //     console.log($(this).outerHeight(true));
        //     return h;
        // });
    }
};

// call init here
collapse.init();

export default collapse;