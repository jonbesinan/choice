import $ from 'jquery';
import lottie from 'lottie-web';
import video from '../partials/video';
import Logger from '../widget/curator/core/logger';

const loader = {
    init () {
        let json = $('#bm').data('json'),
            elLoader = document.getElementById('bm');
        
        lottie.loadAnimation({
            container: elLoader,
            renderer: 'svg',
            loop: true,
            autoplay: true,
            path: json
        });

        loader.run();
    },
    run () {
        let el = $('#loader'),
        elLoader = document.getElementById('bm');
        
        window.onload = () => {
            video.loadLoop();

            setTimeout(() => {
                Logger.log("LOADER->run");
                elLoader.style.display = 'none';
                $('section, .footer-1, header, .sticky').not('#section-feed').addClass('loaded');
                $('.block-heading').addClass('loaded');
                $('body').removeClass('loading');
                el.addClass('active');    
            }, 2500);

            setTimeout(() => {
                $('.no-gutters').removeClass('transform-transition');
            }, 2700);

            setTimeout(() => {
                $('.block-heading').addClass('animate-top');
            }, 3500);
        };
    }
};

// call init here
loader.init();

export default loader;