import $ from 'jquery';
//import List from '../widget/list';
// import Events from '../widget/curator/core/events';

const slider = {
    init () {
        slider.gallery();
        slider.invest();
        //slider.feed();
    },
    gallery () {
        let $el = $(".gallery");

        $el.each( (index, element) => {
            let $this = $(element),
                $container = $this.parents('.gallery-wrapper');
            
            let owl = $this.owlCarousel({
                center: true,
                items: 1,
                margin: 20,
                video: true,
                onInitialized: slider.loaded,
                onChanged: slider.change,
                onDrag: slider.drag
            });

            owl.on('initialize.owl.carousel', function(event){ 
                // Do something
                console.log( 'Owl is loaded!' );
            });
            
            owl.on('changed.owl.carousel', function(event) {
                $el.find('iframe').remove();
                console.log('changed');
            });

            $container.find(".gallery-prev").click(() => {
                owl.trigger('prev.owl.carousel');
                return false;
            });

            $container.find(".gallery-next").click(() => {
                owl.trigger('next.owl.carousel');
                return false;
            });

            $container.find(".nav-tabs").children('li').children('a').click( e => {
                let self = $(e.currentTarget), 
                    indexSlider = self.attr('slider'),
                    indexItem = self.parent().index();
                
                if($('#who-gallery-'+indexSlider).hasClass('disable-slider') == false){
                    owl.trigger('to.owl.carousel', indexItem);
                }
            });
        });

        let maxHeight=0;
        $(".tab-content .tab-pane").each(e =>{
            let $this = $(e.currentTarget), 
            height = $this.height();
            maxHeight = height>maxHeight?height:maxHeight;
            if ( height > maxHeight ) {
                $this.css('min-height', height);
            }
        });

        $(".gallery-wrapper").each(function() {
            let $self = $(this),
            tabs = $self.find('.tab-pane'),
            maxHeight = 0;
            tabs.each(function() {
                let $child = $(this);
                if(maxHeight < $child.height()) {
                    maxHeight = $child.height();
                }
            });
            
            tabs.css('min-height', maxHeight+'px');
        });
    },
    invest (){
        let $el = $("#invest");

        let owl = $el.owlCarousel({
            center: true,
            items: 1, 
            rtl: true,
            touchDrag  : false,
            mouseDrag  : false,
            autoHeight: true,
        });

        $('.btn-panel').click(e => {
            let $self = $(e.currentTarget);
            let panel = $self.data('panel');

            $('.panel-in').hide();
            $('#'+panel).show();
            owl.trigger('to.owl.carousel', 1);


        });

        $('.btn-panel-back').click(e => {
            owl.trigger('to.owl.carousel', 0);

            return false;
        });
    },
    feed () {
        let list = new List ({
            debug:true, // While you're testing
            container:'#curator-feed',
            feedId:'8558f0f9-043f-4bd9-bad1-037cf10a'
        });

        list.on(Events.POSTS_RENDERED, ()=>{
            let owl = $('#owl-feed').owlCarousel({
                autoplay: 3000,
                loop: true,
                responsive:{
                    0 : {
                        items: 1
                    },
                    767 : {
                        items: 2
                    },
                    992 : {
                        items: 4
                    }
                }
            });

            $(".crt-list-post-text-wrap").text(function(index, currentText) {
                return currentText.substr(0, 175);
            });
        });
        
    },
    loaded () {
        setTimeout(function(){ 
            $('.owl-video-play-icon').html('<span></span>');
        }, 1500);
    },
    change (event) {
        let el = event.target,
            index = event.page.index == -1 ? 0 : event.page.index,
            container = $(el).parents('.gallery-wrapper');
            
        container.find('.nav-tabs').children('li').eq(index).children('a').tab('show');   
        container.find('.current').text(index + 1);

        $('.gallery').removeClass('hold');
    },
    drag () {
        $('.gallery').addClass('hold');
        console.log('dragging');
    }
};

// call init here
slider.init();

export default slider;