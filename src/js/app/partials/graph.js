import $ from 'jquery';

const graph = {
    init () {
        graph.current();
    },
    current () {
        let el = $('#current-graph'),
            now = el.data('current'),
            target = el.data('target');

        let total = (now / target) * 100;
        el.find('.progress').height(total + '%');
    }
};

// call init here
graph.init();

export default graph;