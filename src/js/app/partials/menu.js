import jquery from 'jquery';
global.$ = jquery;
global.jQuery = jquery;

import Headroom from 'headroom.js/dist/headroom.js';
window.Headroom = Headroom;

const menu = {
    moved: false,
    movedTo: '',
    init () {
        menu.header();
        menu.section();
    },
    header () {
        let menu = $('.header-menu'),
            menuH = $('#menu-header-nav').actual('height'),
            menuLink = $('.header-menu a'),
            headerH = $('header').height();

        $('.menu-toggle').click(e => {
            let self = $(e.currentTarget);
            console.log('added delay toggle');
            
            self.parent().find('.header-menu').toggleClass('active');
            self.parents('.sticky-el').find('.block-heading').toggleClass('active');
            self.parents('section').find('h1').toggleClass('active');
            return false;
        });

        let flag = true;
        $('.sticky-header .menu-toggle').click(e => {
            let self = $(e.currentTarget);

            if(flag){
                self.removeClass('collapsed');
                flag = false;

                $('html').addClass('mobile-menu');
            }else{
                self.addClass('collapsed');
                flag = true;

                setTimeout(function(){ 
                    $('.sticky-header').removeClass('headroom--unpinned').addClass('headroom--pinned');
                }, 500);
                $('html').removeClass('mobile-menu');
                $('body').removeClass('loading');
            }

            return false;
        });



        $(document.body).on('click','.header-menu a', e => {
            let url = $(e.currentTarget).attr('href'), 
            id = url.replace('#section-',''),
            q = this.urlParameter(url, 'block');
              
            this.goTo(id.split('?')[0], q);

            if($(window).width() < 769) {
                $('.menu-toggle').addClass('collapsed');
                $('.header-menu').removeClass('active');
                flag = true;
            }
            
            return false;
        });

        $(document.body).on('click','.btn-move-who', e => {
            let id = $(e.currentTarget).attr('href').replace('#section-','');
            this.goTo(id, 'who-gallery-1');
            
            return false;
        });

        $('.sticky-header').headroom({
            onUnpin : function() {
                $('.sticky-header').find('.header-menu').removeClass('active');
            },
            onTop : function() {
                $('.sticky-header').find('.header-menu').removeClass('active');
            }
        });
    },
    section () {
        $('.btn-next, .block-image .bg-img').click(e => {
            let $this = $(e.currentTarget);

            if($(window).width() > 768){
                if($this.parents('.fp-section').attr('id') == 'section-1'){
                    $.fn.fullpage.moveSectionDown();
                }else{
                    let iscroll = $('.fp-section.active').find('.fp-scrollable').data('iscrollInstance');
                    if(iscroll && typeof iscroll !== undefined){
                        iscroll.scrollToElement('.fp-section.active .row-down');
                    };
                }
            }else{
                if($this.parents('.section').attr('id') == 'section-1'){
                    $('html, body').animate({ 
                        scrollTop: $this.parents('.section').next().offset().top 
                    }, 800, $.bez("easeOutCubic")); 
                }else{
                    $('html, body').animate({ 
                        scrollTop: $this.parents('.section').find('.row-down').offset().top 
                    }, 800, $.bez("easeOutCubic"));
                }
            }

            return false;
        });
    },
    urlParameter(url, name) {
        let newUrl = (RegExp(name + '=' + '(.+?)(&|$)').exec(url)||[,null])[1];
        return newUrl != null ? newUrl : '';
    },
    goTo (id, _move = '') {
        //console.log(id);
        menu.moved = true;
        menu.movedTo = _move;

        if($(window).width() > 768){
            $.fn.fullpage.moveTo(id);
        }
        else{
            $('html, body').animate({
                scrollTop: $('#section-'+id).offset().top
            }, 800, $.bez("easeOutCubic"));
        }
    }
};

// call init here
menu.init();

export default menu;