import $ from 'jquery';
import menu from '../partials/menu';

let fullpageActive = false;

const scroll = {
    init () {
        this.header();

        let id;
        $(window).resize(() => {
            clearTimeout(id);
            id = setTimeout( () => {
                if($(window).width() > 768) {
                    if (!fullpageActive) {
                        this.initFullpage();
                    } else {
                        $('#section-feed').removeClass('loaded');
                        $('.footer-bottom').insertBefore('#section-feed .fp-scroller').addClass('loaded');

                        setTimeout(function () {
                            console.log('rebuild fullpage 2');
                            $.fn.fullpage.reBuild();
                        }, 1000);

                        // setTimeout(function () {
                        //     $('#section-5').addClass('loaded');
                        // }, 1500);
                        
                    }
                }
                else {
                    if (fullpageActive) {
                        console.log('trigger resize');
                        scroll.destroyFullpage();
                    }
                }
            }, 100);
        });

        if ($(window).width() > 768) {
            this.initFullpage();
        }
    },

    initFullpage () {
        fullpageActive = true;

        $('#fullpage').fullpage({
            licenseKey: 'OPEN-SOURCE-GPLV3-LICENSE',
            scrollOverflowOptions: {
                disablePointer: true
            },
            responsiveWidth: 768,
            scrollOverflow: true,
            loopBottom: false,
	        loopTop: false,
            css3: true,
            easing: 'easeInOutCubic', //this use if css3 is false
            easingcss3: 'cubic-bezier(0.215, 0.610, 0.355, 1.000)',
            afterRender: function () {
                //$('video[loop]').get(0).play();
            },
            afterLoad: function(){
                let title = $(this).data('title'),
                id = this[0].id.replace('section-','');
                $('.block-heading').find('h1').text(title);

                if(this[0].id == 'section-1' && $(window).width() > 768) {
                    $('.block-video').removeClass('overflow-hidden');
                }else{
                    $('.block-video').addClass('overflow-hidden');
                }

                if(this[0].id == 'section-4' && $(window).width() > 768) {
                    $('.footer-bottom').removeClass('loaded');
                }

                if(this[0].id == 'section-5' && $(window).width() > 768) {
                    $('.sticky-el').insertBefore('.block-invest');
                }

                if(this[0].id == 'section-feed' && $(window).width() > 768) {
                    //$('#section-feed').addClass('loaded');
                    $('.footer-bottom').insertBefore('#section-feed .fp-scroller').addClass('loaded');
                    $.fn.fullpage.reBuild();
                }

                //this fixed for IE scroll issue (it force to move to next section)
                let iscroll = $('.fp-section.active').find('.fp-scrollable').data('iscrollInstance');
                if(iscroll && typeof iscroll !== undefined){
                    iscroll.on('scrollEnd', function(){

                        if(this.y == this.maxScrollY){
                            $.fn.fullpage.moveTo(parseInt(id) + 1);
                        }

                        if(this.y == 0){
                            $.fn.fullpage.moveTo(parseInt(id) - 1);
                        }
                    });
                };

                menu.moved = false;

                // console.log(prevIsScroll);
                // console.log(this);
            },
            onLeave: function(index, nextIndex, direction){
                let id = this[0].id.replace('section-','');

                if(id == '4' && $(window).width() > 768) {
                    $('.scrollable-el').scrollTop(2);
                }
                if(id == '5' && $(window).width() > 768 && direction == 'up') {
                    $('.sticky-el').insertBefore('#fullpage');
                }

                if(id == 'feed' && $(window).width() > 768) {
                    $('#section-feed').removeClass('loaded');
                    $('.footer-bottom').insertAfter('.main').removeClass('loaded');
                }

                //this fixed for card https://trello.com/c/IZntBDgz
                let prevEl = $('#section-' + (nextIndex - 1) ),
                prevIsScroll = prevEl.find('.fp-scrollable').data('iscrollInstance');

                if(prevIsScroll && typeof prevIsScroll !== undefined){
                    prevIsScroll.scrollTo(0, prevIsScroll.maxScrollY);
                }

                let currentEl = $('#section-' + nextIndex),
                currentIsScroll = currentEl.find('.fp-scrollable').data('iscrollInstance');

                //console.log(menu.movedTo);
                if(currentIsScroll && typeof currentIsScroll !== undefined && menu.moved && menu.movedTo == ''){
                    currentIsScroll.scrollTo(0, 0);
                }else if(currentIsScroll && typeof currentIsScroll !== undefined && menu.moved && menu.movedTo != ''){
                    currentIsScroll.scrollToElement('#'+menu.movedTo);
                }

                //console.log(nextIndex);
                //console.log(currentEl);
            }
        });
    },

    destroyFullpage () {
        console.log('destroy fullpage');
        $('.footer-bottom').insertAfter('.main').removeClass('loaded');
        $.fn.fullpage.destroy('all');
        fullpageActive = false;
    },

    isScrolledIntoView(elem) {
        if ($(elem).length == 0) {
            return false;
        }
        let docViewTop = $(window).scrollTop();
        let docViewBottom = docViewTop + $(window).height();

        let elemTop = $(elem).offset().top;
        let elemBottom = elemTop + $(elem).height();

        return (docViewBottom >= elemTop && docViewTop);
    },
    header() {
        let blockImage = $('.block-image, .block-video');

        blockImage.waypoint({
            handler: function(direction) {
                if(direction == 'down'){
                    $('.sticky-header').addClass('disable-header');
                }else{
                    $('.sticky-header').removeClass('disable-header');
                }
                //console.log(direction);
            }
        });

        $('.block-who-we-are, .block-what-we-do, .block-get-involved').waypoint({
            handler: function(direction) {
                if(direction == 'up'){
                    $('.sticky-header').addClass('disable-header');
                }else{
                    $('.sticky-header').removeClass('disable-header');
                }

            }
        });

        // $(scroll).on(function(){
        //     let $header = $(".sticky-header");
        //     let $self = $(".block-image");
        //     let fixed_position = $header.offset().top,
        //     fixed_height = $header.height(),
        //     toCross_position = $self.offset().top,
        //     toCross_height = $self.height();

        //     if (fixed_position + fixed_height  < toCross_position) {
        //         $header.removeClass('disable-header');
        //     } else if (fixed_position > toCross_position + toCross_height) {
        //         $header.removeClass('disable-header');
        //     } else {
        //         $header.addClass('disable-header');
        //     }
        // });
    }
};

// call init here
scroll.init();

export default scroll;