import svg4everybody from 'svg4everybody';

const general = {
    init () {
        general.svg();
    },
    svg () {
        svg4everybody();
    }
};

// call init here
general.init();

export default general;