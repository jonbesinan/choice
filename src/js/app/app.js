import jquery from 'jquery';
global.$ = jquery;
global.jQuery = jquery;

import Tether from 'tether';
window.Tether = Tether;

import boot from 'bootstrap';
import owl from 'owl.carousel';
import actual from 'jquery.actual';
import svg4everybody from 'svg4everybody';
import lottie from 'lottie-web';
import bez from 'jquery-bez';
import easing from 'jquery.easing';
import waypoints from 'waypoints/lib/jquery.waypoints';
import Player from '@vimeo/player';

import Headroom from 'headroom.js/dist/headroom.js';
window.Headroom = Headroom;
import 'headroom.js/dist/jQuery.headroom.js';

import scrolloverflow from '../vendor/scrolloverflow.min';
import jqueryFullpage from '../vendor/jquery.fullPage.min';

import loader from './partials/loader';
import general from './partials/general';
import menu from './partials/menu';
import scroll from './partials/scroll';
import graph from './partials/graph';
import collapse from './partials/collapse';
import './partials/video';
import './partials/slider';

import List from './widget/list';
import Events from "./widget/curator/core/events";

let list = new List ({
    debug:true, // While you're testing
    container:'#curator-feed',
    feedId:'8558f0f9-043f-4bd9-bad1-037cf10a'
});

list.on(Events.POSTS_RENDERED, ()=>{
    let owl = $('#owl-feed').owlCarousel({
        autoplay: 3000,
        loop: true,
        responsive:{
            0 : {
                items: 1
            },
            767 : {
                items: 2
            },
            992 : {
                items: 4
            }
        }
    });

    $(".crt-list-post-text-wrap").text(function(index, currentText) {
        return currentText.substr(0, 175);
    });
});