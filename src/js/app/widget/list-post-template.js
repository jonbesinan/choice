

const template = `
<div class="item crt-list-post crt-post-<%=id%> <%=this.contentImageClasses()%> <%=this.contentTextClasses()%>" data-post="<%=id%>"> \
    <div class="crt-post-c"> 
        <div class="crt-post-content"> 
            <div class="crt-list-post-image">
                <div>
                <img class="crt-post-content-image" src="<%=image%>" alt="" /> 
                <a href="javascript:;" class="crt-play"><i class="crt-play-icon"></i></a> 
                <span class="crt-social-icon crt-social-icon-normal"><i class="crt-icon-<%=this.networkIcon()%>"></i></span> 
                <span class="crt-image-carousel"><i class="crt-icon-image-carousel"></i></span>
                </div> 
            </div>
            <div class="crt-list-post-text">
                <div class="crt-list-post-text-wrap"> 
                    <div><%=this.parseText(text)%></div> 
                </div> 
                 <div class="crt-post-footer">
                    <span class="crt-date"><%=this.prettyDate(source_created_at)%></span> 
                    <a href="#" target="_blank">
                        View More
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 47.58 47.58" class="sprite arrow"><defs><style>.cls-1{fill:none;}</style></defs><title>arrow-animate</title><g id="containter"><rect class="cls-1" width="47.58" height="47.58"></rect></g><g id="sideways"><path id="sideways-2" data-name="sideways" class="cls-2" d="M441,269l-12.64-12.64,2.08-2,16.18,16.18-16.18,16.17-2.13-2L441,272H409.11v-3Z" transform="translate(-409.11 -246.72)"></path></g><g id="sideways-under"><rect id="downline-3" data-name="downline" class="cls-2" y="22.28" width="25.22" height="3.02"></rect></g></svg>
                    </a>
                </div>  
            </div>
        </div> 
    </div>
</div>`;

export default template;