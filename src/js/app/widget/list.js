
import Widget from './curator/widgets/base';
import Logger from './curator/core/logger';
import Config from './curator/config/widget_list';
import TemplatingUtils from './curator/core/templating';
import z from './curator/core/lib';
import Events from "./curator/core/events";
import ListFeedTemplate from './list-feed-template';

class List extends Widget {

    constructor  (options) {
        super ();

        this.loading=false;
        this.feed=null;
        this.$container=null;
        this.$feed=null;
        this.posts=[];

        if (this.init (options,  Config)) {
            Logger.log("List->init with options:");
            Logger.log(this.options);

            let tmpl = TemplatingUtils.renderDiv(ListFeedTemplate, {});
            this.$container.append(tmpl);
            this.$feed = this.$container.find('.crt-feed');
            this.$feedWindow = this.$container.find('.crt-feed-window');
            this.$loadMore = this.$container.find('.crt-load-more a');
            this.$scroller = z(window);

            this.$container.addClass('crt-list');
            this.$container.addClass('crt-widget-list');

            // This triggers post loading
            this.feed.load();
        }
    }


    loadPosts () {
        // console.log ('LOAD POSTS CALLED!!!?!?!!?!?!');
    }


    onPostsLoaded (event, posts) {
        Logger.log("List->onPostsLoaded");

        this.loading = false;

        if (posts.length !== 0) {
            this.postElements = [];
            let i = 0;

            for (let postJson of posts) {
                let post = this.createPostElement(postJson);
                this.postElements.push(post);
                this.$feed.append(post.$el);
                post.layout();
            }
        }

        this.trigger(Events.POSTS_RENDERED, this.postElements);
    }

    destroy () {
        super.destroy();

        this.feed.destroy();

        this.$container.empty()
            .removeClass('crt-list')
            .removeClass('crt-widget-list')
            .removeClass('crt-list-col'+this.columnCount)
            .css({'height':'','overflow':''});

        delete this.$feed;
        delete this.$container;
        delete this.options ;
        delete this.loading;

        // TODO add code to cascade destroy down to Posts
        // unregistering events etc
        delete this.feed;
    }
}

export default List;
