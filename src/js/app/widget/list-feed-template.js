

const template = `
<div class="crt-feed-window">
    <div id="owl-feed" class="owl-carousel owl-theme crt-feed"></div>
</div>
<div class="crt-load-more"><a href="#"><%=this._t("load-more")%></a></div>`;

export default template;