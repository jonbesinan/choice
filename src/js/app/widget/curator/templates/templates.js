

import popupUnderlayTemplate from './general/popup_underlay';
import popupWrapperTemplate from './general/popup_wrapper';
import popupTemplate from './general/popup';
import filterTemplate from './general/filter';

let Templates = {
    'filter'                : filterTemplate,
    'popup'                 : popupTemplate,
    'popup-underlay'        : popupUnderlayTemplate,
    'popup-wrapper'         : popupWrapperTemplate,
};

export default Templates;




