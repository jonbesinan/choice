let networks = {
    1 : {
        id: 1,
        name:'Twitter',
        slug:'twitter',
        icon:'crt-icon-twitter'
    },
    2 : {
        id: 2,
        name:'Instagram',
        slug:'instagram',
        icon:'crt-icon-instagram'
    },
    3 : {
        id: 3,
        name:'Facebook',
        slug:'facebook',
        icon:'crt-icon-facebook'
    },
    4 : {
        id: 4,
        name:'Pinterest',
        slug:'pinterest',
        icon:'crt-icon-pinterest'
    },
    5 : {
        id: 5,
        name:'Google',
        slug:'google',
        icon:'crt-icon-google'
    },
    6 : {
        id: 6,
        name:'Vine',
        slug:'vine',
        icon:'crt-icon-vine'
    },
    7 : {
        id: 7,
        name:'Flickr',
        slug:'flickr',
        icon:'crt-icon-flickr'
    },
    8 : {
        id: 8,
        name:'Youtube',
        slug:'youtube',
        icon:'crt-icon-youtube'
    },
    9 : {
        id: 9,
        name:'Tumblr',
        slug:'tumblr',
        icon:'crt-icon-tumblr'
    },
    10 : {
        id: 10,
        name:'RSS',
        slug:'rss',
        icon:'crt-icon-rss'
    },
    11 : {
        id: 11,
        name:'LinkedIn',
        slug:'linkedin',
        icon:'crt-icon-linkedin'
    },
};

export default networks;